//
//  AlertCrudViewController.swift
//  SoyEducadoraSereyd
//
//  Created by Isaias Carrera Ventura on 03/06/17.
//  Copyright © 2017 FullStack. All rights reserved.
//

import UIKit
import SwiftValidator
import UserNotifications

class AlertCrudViewController: UIViewController , ValidationDelegate, UITextFieldDelegate {
	
    @IBOutlet weak var scrollView: UIScrollView!
	@IBOutlet weak var titleAlertTextField: UITextField!
	@IBOutlet weak var descriptionAlertTextField: UITextField!
	@IBOutlet weak var dateAlertTextField: UITextField!
	
	let validator = Validator()
	let datePicker = UIDatePicker()
    var group: GroupModel?
	
	override func viewDidLoad() {
		super.viewDidLoad()
		setupView()
	}
	
	func setupView() -> Void{
		
		CustomBar.setNavBarProfile((self.navigationController?.navigationBar)!, navItem: navigationItem,nav: self)
		
		titleAlertTextField.roundBorder()
		descriptionAlertTextField.roundBorder()
		dateAlertTextField.roundBorder()
		
		createDatePicker()
		
		//REGISTER VALIDATION
		validator.registerField(titleAlertTextField, rules: [RequiredRule(message: "Ingresa un título")])
		validator.registerField(descriptionAlertTextField, rules: [RequiredRule(message: "Ingresa una descripción")])
		validator.registerField(dateAlertTextField, rules: [RequiredRule(message: "Ingresa la fecha")])
		
		//VIEW IN EDITING MODE
		/*NotificationCenter.default.addObserver(self, selector: #selector(GroupCrudViewController.keyboardWillShow), name: NSNotification.Name.UIKeyboardWillShow, object: nil)
		NotificationCenter.default.addObserver(self, selector: #selector(GroupCrudViewController.keyboardWillHide), name: NSNotification.Name.UIKeyboardWillHide, object: nil)*/
		
		
	}
	
	override func didReceiveMemoryWarning() {
		super.didReceiveMemoryWarning()
		// Dispose of any resources that can be recreated.
	}
	
	@IBAction func saveAlertMethod(_ sender: Any) {
		
		validator.validate(self)
		
	}
	
    var identifierForGroupReminder : String?
    
	func validationSuccessful(){
        
        identifierForGroupReminder = (self.group?.value(forKey: "idGroup") as! String) + "_" + dateAlertTextField.text!
        
        UNUserNotificationCenter.current().getNotificationSettings { (notificationSettings) in
            
            switch notificationSettings.authorizationStatus{
                case .notDetermined:
                    self.requestAuthorization(completionHandler: { (success) in
                        guard success else { return }
                        
                        // Schedule Local Notification
                        self.scheduleLocalNotification()
                        
                    })

                    break
                case .authorized:
                    
                    self.scheduleLocalNotification()
                    
                    break
                case .denied:
                    
                    DispatchQueue.main.async {
                        _ = SweetAlert().showAlert("Oops!", subTitle: "¡Permite acceder a tus notificaciones!", style: AlertStyle.error)
                    }
                    
                    break
            }
            
        }
		
		
	}
    
    private func requestAuthorization(completionHandler: @escaping(_ success: Bool) -> ()){
        // Request Authorization
        UNUserNotificationCenter.current().requestAuthorization(options: [.alert, .sound, .badge]) { (success, error) in
            if error != nil {
                DispatchQueue.main.async {
                    _ = SweetAlert().showAlert("Oops!", subTitle: "¡Permite acceder a tus notificaciones!", style: AlertStyle.error)
                }
            }
            
            completionHandler(success)
        }

    }
	
    private func scheduleLocalNotification() {
        
        // Create Notification Content
        let notificationContent = UNMutableNotificationContent()
        
        // Configure Notification Content
        notificationContent.title = titleAlertTextField.text!
        notificationContent.subtitle = dateAlertTextField.text!
        notificationContent.body = descriptionAlertTextField.text!
        notificationContent.sound = UNNotificationSound.default()
        
        let calendar = Calendar(identifier: .gregorian)
        let components = calendar.dateComponents(in: .current, from: self.datePicker.date)
        let newComponents = DateComponents(calendar: calendar, timeZone: .current, month: components.month, day: components.day, hour: components.hour, minute: components.minute)
        
        let notificationTrigger = UNCalendarNotificationTrigger(dateMatching: newComponents, repeats: false)
        
        // Add Trigger
        //let notificationTrigger = UNTimeIntervalNotificationTrigger(timeInterval: 60.0, repeats: false)
        
        // Create Notification Request
        let notificationRequest = UNNotificationRequest(identifier: self.identifierForGroupReminder!, content: notificationContent, trigger: notificationTrigger)
        
        // Add Request to User Notification Center
        UNUserNotificationCenter.current().add(notificationRequest) { (error) in
            if error != nil {
                
               
                
                DispatchQueue.main.async {
                    
                    self.view.endEditing(true)
                    _ = SweetAlert().showAlert("Oops!", subTitle: "¡Algo salió mal!", style: AlertStyle.error)
                }

            }
            
            DispatchQueue.main.async {
                
                self.view.endEditing(true)
                _ = SweetAlert().showAlert("Alerta agregada", subTitle: "¡Agregaste una alerta correctamente!", style: AlertStyle.success)
            }
           
        }
    }
    
	func validationFailed(_ errors: [(Validatable, ValidationError)])
	{
		var stringError = ""
		
		for (field, error) in errors {
			if let field = field as? UITextField {
				field.layer.borderColor = UIColor.red.cgColor
				field.layer.borderWidth = 1.0
			}
			
			stringError = stringError + "\n\(error.errorMessage)"
			error.errorLabel?.text = error.errorMessage // works if you added labels
			error.errorLabel?.isHidden = false
			
		}
        
        self.view.endEditing(true)
		_ = SweetAlert().showAlert("Error", subTitle: "¡Ingresa los datos requeridos! \(stringError)", style: AlertStyle.error, buttonTitle:"Ok", buttonColor:ColorHelper.getGreenColor(1.0))
		
		
	}
	
	func createDatePicker(){
		
		datePicker.minimumDate = Date()
		datePicker.locale = Locale.current
		
		let toolbar  = UIToolbar()
		
		toolbar.sizeToFit()
		
		let doneButton = UIBarButtonItem(barButtonSystemItem: .done, target: nil, action: #selector(donePressedForDatePicker))
		toolbar.setItems([doneButton], animated: false)
		
		dateAlertTextField.inputAccessoryView = toolbar
		
		dateAlertTextField.inputView = datePicker
		
	}
	
	func donePressedForDatePicker(){
		
		let dateFormatter = DateFormatter()
		dateFormatter.dateStyle = .short
		dateFormatter.dateFormat = "dd-MM-YYYY HH:MM:SS"
		dateFormatter.timeStyle = .medium
		
		dateAlertTextField.text = "\(dateFormatter.string(from: datePicker.date))"
		self.view.endEditing(true)
		
	}
	
	//KEYBOARD METHODS
	override func touchesBegan(_ touches: Set<UITouch>, with event: UIEvent?) {
		
		self.view.endEditing(true)
		
	}
	
	func textFieldShouldReturn(_ textField: UITextField) -> Bool {
		
		textField.resignFirstResponder()
		return true
		
	}
	
	
	/*func keyboardWillShow(notification: NSNotification) {
		
		//textfield for school is not always visible for user
		
		if !titleAlertTextField.isEditing{
			
			if let keyboardSize = (notification.userInfo?[UIKeyboardFrameBeginUserInfoKey] as? NSValue)?.cgRectValue {
				if self.view.frame.origin.y == 0{
					self.view.frame.origin.y -= keyboardSize.height
				}
			}
			
		}
		
	}
	
	func keyboardWillHide(notification: NSNotification) {
		if let keyboardSize = (notification.userInfo?[UIKeyboardFrameBeginUserInfoKey] as? NSValue)?.cgRectValue {
			if self.view.frame.origin.y != 0{
				self.view.frame.origin.y += keyboardSize.height
			}
		}
	}*/
    
    func textFieldDidEndEditing(_ textField: UITextField) {
        
        scrollView.setContentOffset(CGPoint(x: 0, y: 0), animated: true)
        
    }
    
    func textFieldDidBeginEditing(_ textField: UITextField) {
        
        if descriptionAlertTextField == textField || dateAlertTextField == textField{
        
            scrollView.setContentOffset(CGPoint(x: 0, y: 250), animated: true)
            
        }
        
    }
	
	
}
