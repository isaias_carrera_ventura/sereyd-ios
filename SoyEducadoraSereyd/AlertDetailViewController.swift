//
//  AlertDetailViewController.swift
//  SoyEducadoraSereyd
//
//  Created by Isaias Carrera Ventura on 03/06/17.
//  Copyright © 2017 FullStack. All rights reserved.
//

import UIKit
import ActionButton
import UserNotifications

class AlertDetailViewController: UIViewController,UITableViewDelegate,UITableViewDataSource {
	
	var group: GroupModel?
	var actionButton: ActionButton!
	@IBOutlet weak var tableView: UITableView!
	
	// Properties
    var notificationsArray = [UNNotificationRequest]()
	let dateFormatter = DateFormatter()
	let locale = Locale.current
	
	override func viewDidLoad() {
		
		super.viewDidLoad()
		setupView()
		
	}
	
	func setupView() -> Void{
		
		actionButton = ActionButton(attachedToView: self.view, items: [])
		actionButton.action = { button in
			self.performSegue(withIdentifier: "crudAlertSegue", sender: self)
		}
		
		actionButton.setTitle("+", forState: UIControlState())
		actionButton.backgroundColor = ColorHelper.getSecondaryColor(1.0)
		
		//TABLE VIEW
		dateFormatter.locale = Locale.current
		dateFormatter.dateStyle = .medium
		dateFormatter.timeStyle = .short
		
	}
    
    
    override func prepare(for segue: UIStoryboardSegue, sender: Any?) {
        
        if segue.identifier == "crudAlertSegue" {
            
            let destinatioViewController = segue.destination as! AlertCrudViewController
            destinatioViewController.group = self.group
            
        }
        
    }
	
	override func viewDidAppear(_ animated: Bool) {
		
		super.viewDidAppear(true)
        fetchReminders()
		
	}
    
    public func fetchReminders() -> Void{
        
        //FETCH REMINDERS
        
        let center = UNUserNotificationCenter.current()
        notificationsArray = [UNNotificationRequest]()
        let idGroupInDatabase = group!.value(forKey: "idGroup") as! String
        
        center.getPendingNotificationRequests { (notifications) in
            
            for item in notifications{
                
                let reminder = item.identifier.components(separatedBy: "_")
                let idGroup = reminder[0]
                
                if idGroup == idGroupInDatabase {
                    self.notificationsArray.append(item)
                }
                
            }
            
            DispatchQueue.main.async {
                self.tableView.reloadData()
            }
            
        }

    }
	
	public func tableView(_ tableView: UITableView, numberOfRowsInSection section: Int) -> Int{
		
		return notificationsArray.count
		
	}
    
    
    override func viewWillAppear(_ animated: Bool) {
        tableView.estimatedRowHeight = 100
        tableView.rowHeight = UITableViewAutomaticDimension
    }

    
	
	public func tableView(_ tableView: UITableView, cellForRowAt indexPath: IndexPath) -> UITableViewCell{
		
		let cell = tableView.dequeueReusableCell(withIdentifier: "Cell", for: indexPath) as! AlertTableViewCell
        let notification = notificationsArray[indexPath.row]
        
        cell.titleAlertLabel.text = notification.content.title
        cell.descriptionAlert.text = notification.content.body
         
        let trigger = notification.trigger as! UNCalendarNotificationTrigger
        
        let dateFormatter = DateFormatter()
        dateFormatter.dateStyle = .short
        dateFormatter.dateFormat = "dd-MM-YYYY HH:MM:SS"
        dateFormatter.timeStyle = .medium
        
        var dateAux = trigger.dateComponents
        dateAux.year = -16
        cell.dateAlertLabel.text = "\(dateFormatter.string(from: dateAux.date!))"
        
        
		cell.tapDeleteAction = { (cell) in
			
			_ = SweetAlert().showAlert("¿Eliminar esta alerta?", subTitle: "", style: AlertStyle.warning, buttonTitle:"Cancelar", buttonColor:ColorHelper.getGreenColor(1.0), otherButtonTitle:  "Eliminar",otherButtonColor: ColorHelper.getRedColor(1.0)) { (isOtherButton) -> Void in
				if isOtherButton != true {
					
                    
                    let identifier = self.notificationsArray[indexPath.row].identifier
                    UNUserNotificationCenter.current().removePendingNotificationRequests(withIdentifiers: [identifier])
                    self.fetchReminders()
					_ = SweetAlert().showAlert("Alerta eliminada!", subTitle: "Has eliminado correctamente una tarea", style: AlertStyle.success)
					
				}
			}
			
			
		}
        
		return cell
		
	}
	
		
	// When returning from AddReminderViewController
	@IBAction func unwindToReminderList(_ sender: UIStoryboardSegue) {
		/*if let sourceViewController = sender.source as? AlertCrudViewController, let reminder = sourceViewController.reminder {
			
		}*/
	}
	
	
}
