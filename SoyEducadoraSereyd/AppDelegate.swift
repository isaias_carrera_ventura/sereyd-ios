//
//  AppDelegate.swift
//  SoyEducadoraSereyd
//
//  Created by Isaias Carrera Ventura on 18/05/17.
//  Copyright © 2017 FullStack. All rights reserved.
//

import UIKit
import Firebase
import RealmSwift
import Realm

@UIApplicationMain
class AppDelegate: UIResponder, UIApplicationDelegate {

	var window: UIWindow?

	func application(_ application: UIApplication, didFinishLaunchingWithOptions launchOptions: [UIApplicationLaunchOptionsKey: Any]?) -> Bool {
		
		//FACEBOOK
		FBSDKApplicationDelegate.sharedInstance().application(application, didFinishLaunchingWithOptions: launchOptions)
		
		//FIREBASE
		FIRApp.configure()
        
        //SDK PAYPAL
        // Override point for customization after application launch.
        
        //Sandbox
        
        /*PayPalMobile.initializeWithClientIds(forEnvironments: [PayPalEnvironmentSandbox: "AV-Ko_obbP8zFHmrsJPqEBYPQ65HgQn7AvfZQky-U5c8bCMDnaKDT7xmpNwovkB5EAnmbqkbaVKnFe08"])*/
        
        PayPalMobile.initializeWithClientIds(forEnvironments: [PayPalEnvironmentProduction: "AQDhZDKIlUru6ry5D3xCWXesF2ruqilOAiHTBJPjdHzFEiUThTNpRXVdjXV3ZWBwkMdJxCZsfnChxM7U"])
        
		//NOTIFICATIONS
		let notificationType : UIUserNotificationType = [UIUserNotificationType.alert,UIUserNotificationType.badge,UIUserNotificationType.sound]
		let notificationSettings = UIUserNotificationSettings(types: notificationType, categories: nil)
		application.registerForRemoteNotifications()
		application.registerUserNotificationSettings(notificationSettings)
		
		//WINDOW TINT
		window?.tintColor = ColorHelper.getPrimaryColor(1.0)
		UISearchBar.appearance().barTintColor = UIColor.lightGray
		UISearchBar.appearance().tintColor = UIColor.white
		setNavigationBarStyles()
		
        
        /*REALM UPDATE*/
        let config = Realm.Configuration(
            // Set the new schema version. This must be greater than the previously used
            // version (if you've never set a schema version before, the version is 0).
            schemaVersion: 1,
            
            // Set the block which will be called automatically when opening a Realm with
            // a schema version lower than the one set above
            migrationBlock: { migration, oldSchemaVersion in
                // We haven’t migrated anything yet, so oldSchemaVersion == 0
                if (oldSchemaVersion < 1) {
                    // Nothing to do!
                    // Realm will automatically detect new properties and removed properties
                    // And will update the schema on disk automatically
                    migration.enumerateObjects(ofType: Planning.className(), { (oldObject, newObject) in
                        
                        newObject!["premium"] = Constants.FREE
                        
                    })
                    
                }
        })
        
        // Tell Realm to use this new configuration object for the default Realm
        Realm.Configuration.defaultConfiguration = config
        
        // Now that we've told Realm how to handle the schema change, opening the file
        // will automatically perform the migration
        //let realm = try! Realm()
        
		return true
	}
	
	func setNavigationBarStyles() -> Void{
		
		let navigationBarAppearace = UINavigationBar.appearance()
		
		navigationBarAppearace.tintColor = ColorHelper.getWhiteColor(1.0)
		navigationBarAppearace.barTintColor = ColorHelper.getPrimaryColor(1.0)
		
		// change navigation item title color
		navigationBarAppearace.titleTextAttributes = [NSForegroundColorAttributeName:UIColor.white]
		
		UIApplication.shared.statusBarStyle = UIStatusBarStyle.lightContent
		
	}
	
	//FACEBOOK
	func application(_ app: UIApplication, open url: URL, options: [UIApplicationOpenURLOptionsKey : Any] = [:]) -> Bool {
		
		return FBSDKApplicationDelegate.sharedInstance().application(app, open: url, options: options)
		
	}
	
	func application(_ application: UIApplication, didReceiveRemoteNotification userInfo: [AnyHashable : Any], fetchCompletionHandler completionHandler: @escaping (UIBackgroundFetchResult) -> Void) {
		
		
	}

	func applicationWillResignActive(_ application: UIApplication) {
		// Sent when the application is about to move from active to inactive state. This can occur for certain types of temporary interruptions (such as an incoming phone call or SMS message) or when the user quits the application and it begins the transition to the background state.
		// Use this method to pause ongoing tasks, disable timers, and invalidate graphics rendering callbacks. Games should use this method to pause the game.
	}

	func applicationDidEnterBackground(_ application: UIApplication) {
		// Use this method to release shared resources, save user data, invalidate timers, and store enough application state information to restore your application to its current state in case it is terminated later.
		// If your application supports background execution, this method is called instead of applicationWillTerminate: when the user quits.
	}

	func applicationWillEnterForeground(_ application: UIApplication) {
		// Called as part of the transition from the background to the active state; here you can undo many of the changes made on entering the background.
	}

	func applicationDidBecomeActive(_ application: UIApplication) {
		// Restart any tasks that were paused (or not yet started) while the application was inactive. If the application was previously in the background, optionally refresh the user interface.
	}

	func applicationWillTerminate(_ application: UIApplication) {
		// Called when the application is about to terminate. Save data if appropriate. See also applicationDidEnterBackground:.
	}


}

