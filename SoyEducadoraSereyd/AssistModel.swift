//
//  AssistModel.swift
//  SoyEducadoraSereyd
//
//  Created by Isaias Carrera Ventura on 03/08/17.
//  Copyright © 2017 FullStack. All rights reserved.
//

import Foundation

class AssistModel {
    
    var idGroup: String!
    var assistObject: AssistObject
    
    init(idGroup: String,assistObject: AssistObject) {
        
        self.assistObject = assistObject
        self.idGroup = idGroup
        
    }
    
    class AssistObject {
    
        var idStudent: String!
        var assist: Int = 1
        var codunct: Int = -1
        
        init(idStudentAux: String) {
            idStudent = idStudentAux
        }
    
    }
    
}
