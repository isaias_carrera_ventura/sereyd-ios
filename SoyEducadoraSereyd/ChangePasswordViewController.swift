//
//  ChangePasswordViewController.swift
//  SoyEducadoraSereyd
//
//  Created by Isaias Carrera Ventura on 16/09/17.
//  Copyright © 2017 FullStack. All rights reserved.
//

import UIKit
import SwiftValidator
import Alamofire
import NVActivityIndicatorView

class ChangePasswordViewController: UIViewController , UITextFieldDelegate, ValidationDelegate, NVActivityIndicatorViewable{

    let validator = Validator()
    
    @IBOutlet weak var scrollView: UIScrollView!
    @IBOutlet weak var textFieldNewPassword: UITextField!
    @IBOutlet weak var textFieldConfirmPassword: UITextField!
    
    override func viewDidLoad() {
        super.viewDidLoad()
        textFieldNewPassword.roundBorder()
        textFieldConfirmPassword.roundBorder()
        validator.registerField(textFieldNewPassword, rules: [MinLengthRule(length: 6, message: "Contraseña con mínimo 6 caractéres")])
        validator.registerField(textFieldConfirmPassword, rules: [ConfirmationRule(confirmField: textFieldNewPassword, message: "Las contraseñas no coinciden")])

        // Do any additional setup after loading the view.
    }

    override func didReceiveMemoryWarning() {
        super.didReceiveMemoryWarning()
        // Dispose of any resources that can be recreated.
    }
    
    @IBAction func changePassword(_ sender: Any) {
        
        validator.validate(self)
    }
    
    
    func validationSuccessful() {
        
        let params = ["password":textFieldNewPassword.text!]
        //GET IMAGE AND SAVE OBJECT
        self.startAnimating(Constants.getSizeForLoadingView(view: self.view), type: NVActivityIndicatorType.ballBeat, color: ColorHelper.getSecondaryColor(1.0), padding: NVActivityIndicatorView.DEFAULT_PADDING)
        let url = "\(Constants.PASSWORD_RESTORE_ENDPOINT_CHANGE)\(UserController.getUserInDatabase()!.value(forKey: "idUser") as! String)"
        Alamofire.request(url, method: .put, parameters: params)
            .validate()
            .responseString { (dataResponse) in
                
                self.stopAnimating()
                
                guard dataResponse.error == nil else{
                    
                    let stringError = AlamofireHandlerError.getErrorMessage(dataResponse)
                    _ = SweetAlert().showAlert("Error", subTitle: "\(stringError)", style: AlertStyle.error, buttonTitle:"Ok", buttonColor:ColorHelper.getGreenColor(1.0))
                    
                    return
                    
                }
                
                guard let dataResponseSign = dataResponse.data else{
                    
                    return
                    
                }
                
                let jsonResponseUser = try? JSONSerialization.jsonObject(with: dataResponseSign, options: .allowFragments)
                
                guard (jsonResponseUser as? [String:Any]) != nil else{
                    
                    return
                    
                }
                
                
                let dictionaryResponse = jsonResponseUser as! [String:Any]
                
                if dictionaryResponse["error"] as! Int == 0 {
                    
                    _ = SweetAlert().showAlert("Aviso", subTitle: "¡Tu contraseña se ha reestablecida, verifica tu correo e ingresa a la app!", style: AlertStyle.success, buttonTitle:"Ok", buttonColor:ColorHelper.getGreenColor(1.0)){(isOtherButton) -> Void in
                        
                        self.navigationController?.popViewController(animated: true)
                        
                    }
                    
                    
                }else{
                    _ = SweetAlert().showAlert("Error", subTitle: "¡Algo salió mal!", style: AlertStyle.error, buttonTitle:"Ok", buttonColor:ColorHelper.getGreenColor(1.0))
                }
                
        }

        
        
    }

    
    func validationFailed(_ errors:[(Validatable ,ValidationError)]) {
        
        var stringError = ""
        
        for (field, error) in errors {
            if let field = field as? UITextField {
                field.layer.borderColor = UIColor.red.cgColor
                field.layer.borderWidth = 1.0
            }
            stringError = stringError + "\n\(error.errorMessage)"
            error.errorLabel?.text = error.errorMessage // works if you added labels
            error.errorLabel?.isHidden = false
            
        }
        
        
        self.view.endEditing(true) 
        _ = SweetAlert().showAlert("Error", subTitle: "¡Ingresa los datos requeridos! \(stringError)", style: AlertStyle.error, buttonTitle:"Ok", buttonColor:ColorHelper.getGreenColor(1.0))
        
        
    }
    
    //KEYBOARD METHODS
    override func touchesBegan(_ touches: Set<UITouch>, with event: UIEvent?) {
        
        self.view.endEditing(true)
        
    }
    
    func textFieldShouldReturn(_ textField: UITextField) -> Bool {
        
        textField.resignFirstResponder()
        return true
        
    }
    
    
    func textFieldDidEndEditing(_ textField: UITextField) {
        
        scrollView.setContentOffset(CGPoint(x: 0, y: 0), animated: true)
        
    }
    
    func textFieldDidBeginEditing(_ textField: UITextField) {
        
        scrollView.setContentOffset(CGPoint(x: 0, y: 150), animated: true)
        
    }


}
