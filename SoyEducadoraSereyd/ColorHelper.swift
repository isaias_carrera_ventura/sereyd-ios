//
//  ColorHelper.swift
//  SoyEducadoraSereyd
//
//  Created by Isaias Carrera Ventura on 19/05/17.
//  Copyright © 2017 FullStack. All rights reserved.
//

import Foundation
import UIKit

class ColorHelper{
    
    static let primaryColor: UInt = 0x8263ab
    static let secondaryColor : UInt = 0x59bf9e
    static let thirdColor : UInt = 0xf7a341
    static let whiteColor: UInt = 0xFFFFFF
    
    static let grayColor: UInt = 0x6E6E6E
    
    static let blueColor: UInt = 0x00b9e5
    static let redColor : UInt = 0xff5b5c
    static let aquaColor: UInt = 0x00cad4
    static let greenColor: UInt = 0x5ca700
    static let yellowColor: UInt = 0xf7a341
    
    static func UIColorFromRGB(_ rgbValue: UInt,alphaValue : Double) -> UIColor {
        
        return UIColor(
            red: CGFloat((rgbValue & 0xFF0000) >> 16) / 255.0,
            green: CGFloat((rgbValue & 0x00FF00) >> 8) / 255.0,
            blue: CGFloat(rgbValue & 0x0000FF) / 255.0,
            alpha: CGFloat(alphaValue)
        )
        
    }
    
    static func UIColorFromHex(_ rgbValue:UInt32)->UIColor {
        
        let red = CGFloat((rgbValue & 0xFF0000) >> 16)/256.0
        let green = CGFloat((rgbValue & 0xFF00) >> 8)/256.0
        let blue = CGFloat(rgbValue & 0xFF)/256.0
        
        return UIColor(red:red, green:green, blue:blue, alpha:1.0)
    }
    
    static func getGreenColor(_ alphaValue: Double) -> UIColor{
        return self.UIColorFromRGB(greenColor, alphaValue: alphaValue)
    }
    
    static func getPrimaryColor(_ alphaValue: Double) -> UIColor{
        return self.UIColorFromRGB(primaryColor, alphaValue: alphaValue)
    }
    
    static func getSecondaryColor(_ alphaValue: Double) -> UIColor{
        return self.UIColorFromRGB(secondaryColor, alphaValue: alphaValue)
    }
    
    static func getThirdColor(_ alphaValue: Double) -> UIColor{
        return self.UIColorFromRGB(thirdColor, alphaValue: alphaValue)
    }
    
    static func getWhiteColor(_ alphaValue: Double) -> UIColor{
        return self.UIColorFromRGB(whiteColor, alphaValue: alphaValue)
    }
    
    static func getRedColor(_ alphaValue: Double) -> UIColor{
        return self.UIColorFromRGB(redColor, alphaValue: alphaValue)
    }
    
    static func getGrayColor(_ alphaValue: Double) -> UIColor{
        return self.UIColorFromRGB(grayColor, alphaValue: alphaValue)
    }
    
    static func getAquaColor(_ alphaValue: Double) -> UIColor{
        return self.UIColorFromRGB(aquaColor, alphaValue: alphaValue)
    }
    
    
    static func getRandomColorForResource() -> UIColor{
        
        let random = Int(arc4random_uniform(UInt32(5)))
        switch random{
            
        case 0:
            return UIColorFromRGB(blueColor,alphaValue: 1.0)
        case 1:
            return UIColorFromRGB(redColor,alphaValue: 1.0)
        case 2:
            return UIColorFromRGB(aquaColor,alphaValue: 1.0)
        case 3:
            return UIColorFromRGB(greenColor,alphaValue: 1.0)
        case 4:
            return UIColorFromRGB(yellowColor,alphaValue: 1.0)
        default:
            return UIColorFromRGB(blueColor,alphaValue: 1.0)
        }
        
        
    }
    
}
