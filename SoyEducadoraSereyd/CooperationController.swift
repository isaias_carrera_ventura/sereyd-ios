//
//  CooperationController.swift
//  SoyEducadoraSereyd
//
//  Created by Isaias Carrera Ventura on 31/05/17.
//  Copyright © 2017 FullStack. All rights reserved.
//

import Foundation
import RealmSwift
import Alamofire

class CooperationController{
	
	class func addCooperationToDatabase(_ cooperation: CooperationModel) -> Void{
		
		let realm = try! Realm()
		
		try! realm.write {
			
			
			realm.add(cooperation)
			
		}
		
	}
	
	class func deleteCooperationInDatabase(cooperation: CooperationModel) -> Void{
		
		let realm = try! Realm()
		
		// Delete an object with a transaction
		try! realm.write {
			realm.delete(cooperation)
		}
		
	}
	
	class func editCooperationInDatabase(cooperation: CooperationModel,concept: String,subject: String,account: Double) -> Void{
		
		let realm = try! Realm()
		
		// Delete an object with a transaction
		try! realm.write {
		
			cooperation.setValue(concept, forKey: "concept")
			cooperation.setValue(subject, forKey: "subject")
			cooperation.setValue(account, forKey: "account")
			
		}
		
	}
	
	class func cooperationFromResponse(_ dataResponse: DefaultDataResponse) -> CooperationModel?{
	
		guard dataResponse.error == nil else{
			
			
			return nil
			
		}
		
		guard let dataResponse = dataResponse.data else{
			
			
            return nil
			
		}
		
		let jsonResponse = try? JSONSerialization.jsonObject(with: dataResponse, options: .allowFragments) as! [String:Any]
		
		guard (jsonResponse) != nil else{
			
			return nil
			
		}
		 
		if (jsonResponse!["error"] as? Int) == 1 {
			
			return nil
			
		}
	
		let cooperationObject = jsonResponse!["cooperation"] as! [String:Any]
		
		let cooperation = CooperationModel()
		cooperation.concept = cooperationObject["concept"] as? String
		cooperation.subject = cooperationObject["description"] as? String
		cooperation.account = (cooperationObject["amount"] as! NSString).doubleValue
		cooperation.id_cooperation = cooperationObject["id_cooperation"] as? String
	
		return cooperation		
	}
	
	class func getCooperationInDatabase(_ group: GroupModel) -> [CooperationModel]{
		
		var items: LinkingObjects<CooperationModel>!
		items = group.value(forKey: "cooperations") as! LinkingObjects<CooperationModel>
		
		var cooperation = [CooperationModel]()
		
		for cooperationDb in items{
			
			cooperation.append(cooperationDb)
			
		}
		
		return cooperation
		
	}
	
	class  func getCooperationListFromResponse(_ dataResponse: DefaultDataResponse) -> [CooperationModel]?{
		
		
		guard dataResponse.error == nil else{
			
			return nil
			
		}
		
		guard let dataResponse = dataResponse.data else{
			
			return nil
			
		}
		
		let jsonResponse = try? JSONSerialization.jsonObject(with: dataResponse, options: .allowFragments) as! [String:Any]
		
		guard (jsonResponse) != nil else{
			
			return nil
			
		}
		
		if (jsonResponse!["error"] as? Int) == 1 {
			
			return nil
			
		}
		
		guard let groupObject = jsonResponse?["data"] as? [String:Any] else{
			
			return nil
		}
		
		guard let cooperationArrayRaw = groupObject["cooperations"] as? NSArray else{
			
			return nil
			
		}
		
		var cooperationArray = [CooperationModel]()
		
		if cooperationArrayRaw.count == 0{
			return cooperationArray
		}
		
		for cooperationRaw in cooperationArrayRaw{
			
			let coopObject = cooperationRaw as! [String:Any]
			
			let cooperationAppend = CooperationModel()
			cooperationAppend.account = (coopObject["amount"] as! NSString).doubleValue
			cooperationAppend.concept = coopObject["concept"] as? String
			cooperationAppend.subject = coopObject["description"] as? String
			cooperationAppend.account = (coopObject["amount"] as! NSString).doubleValue
			cooperationAppend.id_cooperation = coopObject["id_cooperation"] as? String
			
			cooperationArray.append(cooperationAppend)
			
		}
		 
		return cooperationArray
		
	}
	
	class func saveListUnrepeatedCooperationInList(_ cooperationArray: [CooperationModel],_ group: GroupModel){
		
		let listCooperationInDatabase = getCooperationInDatabase(group)
		
		if  cooperationArray.count > listCooperationInDatabase.count {
			
			for cooperation in cooperationArray{
				
				var isCooperationInDatabase = false
				
				for cooperationInDatabase in listCooperationInDatabase{
					
					if(cooperationInDatabase.value(forKey: "id_cooperation") as! String == cooperation.id_cooperation!){
						isCooperationInDatabase = true
					}
					
				}
				
				if !isCooperationInDatabase{
					cooperation.groups.append(group)
					addCooperationToDatabase(cooperation)
				}
				
			}
			
		}else if  cooperationArray.count < listCooperationInDatabase.count{
			
			for cooperation in listCooperationInDatabase{
				
				var isCooperationInServer = false
				
				for cooperationInServer in cooperationArray {
					
					if cooperation.value(forKey: "id_cooperation") as? String == cooperationInServer.id_cooperation{
						isCooperationInServer = true
					}
				}
				
				if !isCooperationInServer {
					
					deleteCooperationInDatabase(cooperation: cooperation)
					
					
				}
				
			}
			
			
		}
		
		saveChangesInDatabaseUpdate(getCooperationInDatabase(group),cooperationArray)
		

	}
	
	class func saveChangesInDatabaseUpdate(_ cooperationInDB:[CooperationModel],_ cooperationInServer:[CooperationModel]) -> Void{
		
		for cooperation in cooperationInDB{
			
			if let cooperationAux = getCooperationInServerById(cooperation.value(forKey: "id_cooperation") as! String,cooperationInServer)
			{
				editCooperationInDatabase(cooperation: cooperation, concept: cooperationAux.concept!, subject: cooperationAux.subject!, account: cooperationAux.account)
			}
		}
	}
	
	class func getCooperationInServerById(_ idCooperation: String,_ elements: [CooperationModel] ) -> CooperationModel?{
		
		for cooperation in elements{
			
			if cooperation.id_cooperation == idCooperation{
				return cooperation
			}
			
		}
		
		return nil
		
	}
	
	class func deleteResponseFromServer(_ dataResponse: DefaultDataResponse) -> Bool{
		
		guard dataResponse.error == nil else{
			
			return false
			
		}
		
		guard let dataResponse = dataResponse.data else{
			
			return false
			
		}
		
		let jsonResponse = try? JSONSerialization.jsonObject(with: dataResponse, options: .allowFragments) as! [String:Any]
		
		guard (jsonResponse) != nil else{
			
			return false
			
		}
		
		if (jsonResponse!["error"] as? Int) == 1 {
			
			return false
			
		}
		
		return true
		
	}
	
	class func updateResponseFromServer(_ dataResponse: DefaultDataResponse) -> Bool{
		
		guard dataResponse.error == nil else{
			
			return false
			
		}
		
		guard let dataResponse = dataResponse.data else{
			
			return false
			
		}
		
		let jsonResponse = try? JSONSerialization.jsonObject(with: dataResponse, options: .allowFragments) as! [String:Any]
		
		guard (jsonResponse) != nil else{
			
			return false
			
		}
		
		if (jsonResponse!["error"] as? Int) == 1 {
			
			return false
			
		}
		
		return true
		
	}
	
	
}
