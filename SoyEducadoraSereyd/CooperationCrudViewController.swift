//
//  CooperationCrudViewController.swift
//  SoyEducadoraSereyd
//
//  Created by Isaias Carrera Ventura on 01/06/17.
//  Copyright © 2017 FullStack. All rights reserved.
//

import UIKit
import SwiftValidator
import Alamofire
import NVActivityIndicatorView

class CooperationCrudViewController: UIViewController, ValidationDelegate, UITextFieldDelegate,NVActivityIndicatorViewable {
	
	
    @IBOutlet weak var scrollView: UIScrollView!
	@IBOutlet weak var subjectTextField: UITextField!
	@IBOutlet weak var conceptTextField: UITextField!
	@IBOutlet weak var amountTextField: UITextField!
	
	var group: GroupModel?
	var cooperation: CooperationModel?
	var bandEditCooperation : Bool = false
	
	let validator = Validator()
	
	override func viewDidLoad() {
		
		super.viewDidLoad()
		setupView()
		
		if cooperation != nil{
			setupViewWithCoooperation(cooperation!)
			self.bandEditCooperation = true
		}
		
	}
	
	func setupViewWithCoooperation(_ cooperation: CooperationModel) -> Void{
		
		subjectTextField.text = cooperation.value(forKey: "subject") as? String
		conceptTextField.text = cooperation.value(forKey: "concept") as? String
		amountTextField.text = "\(String(describing: cooperation.value(forKey: "account")!))"
		
	}
	
	func setupView(){
		
		CustomBar.setNavBarProfile((self.navigationController?.navigationBar)!, navItem: navigationItem,nav: self)
		subjectTextField.roundBorder()
		conceptTextField.roundBorder()
		amountTextField.roundBorder()
		
		//VIEW IN EDITING MODE
		/*NotificationCenter.default.addObserver(self, selector: #selector(GroupCrudViewController.keyboardWillShow), name: NSNotification.Name.UIKeyboardWillShow, object: nil)
		NotificationCenter.default.addObserver(self, selector: #selector(GroupCrudViewController.keyboardWillHide), name: NSNotification.Name.UIKeyboardWillHide, object: nil)*/
		
		//REGISTER VALIDATION
		
		validator.registerField(subjectTextField, rules: [RequiredRule(message: "Ingresa el concepto")])
		validator.registerField(conceptTextField, rules: [RequiredRule(message: "Ingresa el asunto")])
		validator.registerField(amountTextField, rules: [RegexRule(regex: "[+-]?([0-9]*[.])?[0-9]+", message: "Cantidad incorrecta")])
		
	}
	
	@IBAction func saveCooperation(_ sender: Any) {
		
		validator.validate(self)
		
	}
	
	func validationSuccessful(){
		
		if self.bandEditCooperation {
			
			requestForUpdateCooperation()
			
		}else{
			
			let cooperation = CooperationModel()
			cooperation.account = Double(amountTextField.text!)!
			cooperation.concept = conceptTextField.text!
			cooperation.subject = subjectTextField.text!
			cooperation.groups.append(group!)
			
			requestForNewCooperation(cooperation)
			
		}
		
		
	}
	
	func requestForUpdateCooperation() -> Void{
		
		let url = "\(Constants.COOPERATION_ENDPOINT)\(cooperation!.value(forKey: "id_cooperation") as! String)"
		
		self.startAnimating(Constants.getSizeForLoadingView(view: self.view), type: NVActivityIndicatorType.ballBeat, color: ColorHelper.getSecondaryColor(1.0), padding: NVActivityIndicatorView.DEFAULT_PADDING)
		
		let params = ["concept":conceptTextField.text!,
		              "description":subjectTextField.text!,
		              "amount":amountTextField.text!]
		
		Alamofire.request(url, method: .put, parameters: params)
			.validate()
			.response { (dataResponse) in
				
				self.bandEditCooperation = false
				
				if CooperationController.updateResponseFromServer(dataResponse){
					CooperationController.editCooperationInDatabase(cooperation: self.cooperation!, concept: self.conceptTextField.text!, subject: self.subjectTextField.text!, account: Double(self.amountTextField.text!)!)
					
                    
                    self.view.endEditing(true)
					_ = SweetAlert().showAlert("Cooperación editada", subTitle: "¡Editaste una cooperación correctamente!", style: AlertStyle.success, buttonTitle:"Ok", buttonColor: ColorHelper.getGreenColor(1.0)) { (button) -> Void in
						
						self.navigationController?.popViewController(animated: true)
						
					}

				}else{
					
                    
                    self.view.endEditing(true)
					_ = SweetAlert().showAlert("Error", subTitle: "¡Algo salió mal!", style: AlertStyle.error, buttonTitle:"Ok", buttonColor:ColorHelper.getGreenColor(1.0))
					
				}
				
				
		}
		
	}
	
	func requestForNewCooperation(_ cooperation: CooperationModel) -> Void{
		
		
		let params = ["concept":cooperation.concept!,
		              "description":cooperation.subject!,
		              "amount":"\(cooperation.account)",
			"id_group":(group!.value(forKey: "idGroup") as! String)] as [String:Any]
		
		//GET IMAGE AND SAVE OBJECT
		self.startAnimating(Constants.getSizeForLoadingView(view: self.view), type: NVActivityIndicatorType.ballBeat, color: ColorHelper.getSecondaryColor(1.0), padding: NVActivityIndicatorView.DEFAULT_PADDING)
		
		Alamofire.request(Constants.COOPERATION_ENDPOINT, method: .post, parameters: params)
			.validate()
			.response { (defaultDataResponse) in
				
				self.stopAnimating()
				
				if let cooperation = CooperationController.cooperationFromResponse(defaultDataResponse) {
					
					cooperation.groups.append(self.group!)
					CooperationController.addCooperationToDatabase(cooperation)
                    
                    self.view.endEditing(true)
					_ = SweetAlert().showAlert("Cooperación agregada", subTitle: "¡Agregaste una cooperación correctamente!", style: AlertStyle.success, buttonTitle:"Ok", buttonColor: ColorHelper.getGreenColor(1.0)) { (button) -> Void in
						
						self.navigationController?.popViewController(animated: true)
						
					}
				}else{
                    
                    self.view.endEditing(true)
					_ = SweetAlert().showAlert("Error", subTitle: "¡Algo salió mal!", style: AlertStyle.error, buttonTitle:"Ok", buttonColor:ColorHelper.getGreenColor(1.0))
					
					
				}
		}
		
	}
	
	func validationFailed(_ errors: [(Validatable, ValidationError)]){
		var stringError = ""
		
		for (field, error) in errors {
			if let field = field as? UITextField {
				field.layer.borderColor = UIColor.red.cgColor
				field.layer.borderWidth = 1.0
			}
			
			stringError = stringError + "\n\(error.errorMessage)"
			error.errorLabel?.text = error.errorMessage // works if you added labels
			error.errorLabel?.isHidden = false
			
		}
		
        
        self.view.endEditing(true)
		_ = SweetAlert().showAlert("Error", subTitle: "¡Ingresa los datos requeridos! \(stringError)", style: AlertStyle.error, buttonTitle:"Ok", buttonColor:ColorHelper.getGreenColor(1.0))
		
	}
	
	
	override func didReceiveMemoryWarning() {
		super.didReceiveMemoryWarning()
		// Dispose of any resources that can be recreated.
	}
	
	
	//KEYBOARD METHODS
	override func touchesBegan(_ touches: Set<UITouch>, with event: UIEvent?) {
		
		self.view.endEditing(true)
		
	}
	
	func textFieldShouldReturn(_ textField: UITextField) -> Bool {
		
		textField.resignFirstResponder()
		return true
		
	}
	
	
    func textFieldDidBeginEditing(_ textField: UITextField) {
        
        if textField == conceptTextField || textField == amountTextField{
            scrollView.setContentOffset(CGPoint(x: 0, y: 250), animated: true)
        }
        
    }
    
    
    func textFieldDidEndEditing(_ textField: UITextField) {
        
        scrollView.setContentOffset(CGPoint(x: 0, y: 0), animated: true)
        
    }

	/*func keyboardWillShow(notification: NSNotification) {
		
		//textfield for school is not always visible for user
		
		if !conceptTextField.isEditing{
			
			if let keyboardSize = (notification.userInfo?[UIKeyboardFrameBeginUserInfoKey] as? NSValue)?.cgRectValue {
				if self.view.frame.origin.y == 0{
					self.view.frame.origin.y -= keyboardSize.height
				}
			}
			
		}
		
	}
	
	func keyboardWillHide(notification: NSNotification) {
		if let keyboardSize = (notification.userInfo?[UIKeyboardFrameBeginUserInfoKey] as? NSValue)?.cgRectValue {
			if self.view.frame.origin.y != 0{
				self.view.frame.origin.y += keyboardSize.height
			}
		}
	}*/
	
	
}
