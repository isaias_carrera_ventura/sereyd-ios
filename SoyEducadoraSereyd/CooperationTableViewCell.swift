//
//  CooperationTableViewCell.swift
//  SoyEducadoraSereyd
//
//  Created by Isaias Carrera Ventura on 01/06/17.
//  Copyright © 2017 FullStack. All rights reserved.
//

import UIKit

class CooperationTableViewCell: UITableViewCell {

	var tapDeleteAction: ((UITableViewCell) -> Void)?
	var tapEditAction: ((UITableViewCell) -> Void)?
	
	@IBOutlet weak var conceptLabel: UILabel!
	@IBOutlet weak var amountLabel: UILabel!
    @IBOutlet weak var textViewDescription: UITextView!
	
	@IBAction func editCooperation(_ sender: Any) {
		tapEditAction?(self)
	}
	
	@IBAction func deleteCooperation(_ sender: Any) {
		tapDeleteAction?(self)
	}
}
