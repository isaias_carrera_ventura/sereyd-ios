//
//  DateUtils.swift
//  SoyEducadoraSereyd
//
//  Created by Isaias Carrera Ventura on 21/07/17.
//  Copyright © 2017 FullStack. All rights reserved.
//

import Foundation

class DateUtils {
    
    class func getStringDate() -> String{
        
        let date = Date()
        let formatter = DateFormatter()
        formatter.dateFormat = "dd/MM/yyyy"
        return formatter.string(from: date)
    
    }
    
}
