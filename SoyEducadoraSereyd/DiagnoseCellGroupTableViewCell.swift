//
//  DiagnoseCellGroupTableViewCell.swift
//  SoyEducadoraSereyd
//
//  Created by Isaias Carrera Ventura on 12/07/17.
//  Copyright © 2017 FullStack. All rights reserved.
//

import UIKit

class DiagnoseCellGroupTableViewCell: UITableViewCell {

    @IBOutlet weak var nameDiagnose: UILabel!
    @IBOutlet weak var diagnoseFinished: UILabel!
    

}
