//
//  DiagnoseViewController.swift
//  SoyEducadoraSereyd
//
//  Created by Isaias Carrera Ventura on 11/07/17.
//  Copyright © 2017 FullStack. All rights reserved.
//

import UIKit
import ActionSheetPicker_3_0
import Alamofire
import NVActivityIndicatorView

class DiagnoseViewController: UIViewController, UITableViewDelegate, UITableViewDataSource, NVActivityIndicatorViewable {
    
    @IBOutlet weak var selectGroup: UIButton!
    @IBOutlet weak var labelSelectedGroup: UILabel!
    @IBOutlet weak var tableViewDiagnose: UITableView!
    @IBOutlet weak var tableViewDiagnoseSelection: UITableView!
    
    var isGroupSelected = false
    var positionSelected = 0
    var diagnoseComplete = false
    var groups = [GroupModel]()
    var diagnoseToSelect = [DiagnoseModel]()
    var positionSelectedDiagnose = -1
    
    var groupsToDiagnose = [GroupModel]()
    var testToPass : [TestModel]?
    var diagnoseToPass : DiagnoseModel?
    var studentArrayToPass : [StudentModel]?
    
    override func viewDidLoad() {
        
        super.viewDidLoad()
        CustomBar.setNavBarProfile((self.navigationController?.navigationBar)!, navItem: navigationItem,nav: self)
        
        selectGroup.backgroundColor = .clear
        selectGroup.layer.cornerRadius = 10
        selectGroup.layer.borderWidth = 2
        selectGroup.layer.borderColor = ColorHelper.getSecondaryColor(1.0).cgColor
        
        fetchDiagnoseType()
        
    }
    
    override func viewDidAppear(_ animated: Bool) {
        
        groupsToDiagnose.removeAll()
        self.tableViewDiagnose.reloadData()
        requestForGroupDiagnostic()
        
    }
    
    func fetchDiagnoseType() -> Void{
        
        self.startAnimating(Constants.getSizeForLoadingView(view: self.view), type: NVActivityIndicatorType.ballBeat, color: ColorHelper.getSecondaryColor(1.0), padding: NVActivityIndicatorView.DEFAULT_PADDING)
        
        Alamofire.request(Constants.DIAGNOSTIC_ENDPOINT, method: .get)
            .validate()
            .response { (dataResponse) in
                
                self.stopAnimating()
                if let diagnoses = DiagnoseController.getAvailableDiagnosticFromResponse(dataResponse){
                    
                    self.diagnoseToSelect = diagnoses
                    self.tableViewDiagnoseSelection.reloadData()
                    
                }else{
                    
                    _ = SweetAlert().showAlert("Ooops!", subTitle: "¡Algo salió mal!", style: AlertStyle.warning)
                    
                }
        }
    }
    
    func requestForGroupDiagnostic() -> Void{
        
        let myGroups = GroupController.getGroupInDatabase()
        
        for group in myGroups{
            
            self.startAnimating(Constants.getSizeForLoadingView(view: self.view), type: NVActivityIndicatorType.ballBeat, color: ColorHelper.getSecondaryColor(1.0), padding: NVActivityIndicatorView.DEFAULT_PADDING)
            let url = "\(Constants.GROUP_DIAGNOSTIC_PENDING_ENDPOINT)\(group.value(forKey: "idGroup") as! String)"
            Alamofire.request(url, method: .get)
                .validate()
                .response { (dataResponse) in
                    
                    self.stopAnimating()
                    if let diagnosesForUser = DiagnoseController.getAvailableDiagnosticForUserGroup(dataResponse){
                        
                        if diagnosesForUser.count > 0{
                            
                            self.groupsToDiagnose.append(group)
                            DiagnoseController.addDiagnoseListToDatabase(diagnosesForUser,group)
                            self.tableViewDiagnose.reloadData()
                            
                        }
                    }
                    
            }
            
        }
    }
    
    
    public func tableView(_ tableView: UITableView, numberOfRowsInSection section: Int) -> Int{
        
        
        var count: Int?
        
        if tableView == self.tableViewDiagnose{
            
            count =  groupsToDiagnose.count
            
        }
        
        if tableView == self.tableViewDiagnoseSelection{
            
            count = diagnoseToSelect.count
            
        }
        
        return count!
        
    }
    
    func tableView(_ tableView: UITableView, cellForRowAt indexPath: IndexPath) -> UITableViewCell {
        
        var cell: UITableViewCell?
        
        if tableView == self.tableViewDiagnose{
            
            let cellDiagnose = tableView.dequeueReusableCell(withIdentifier: "Cell",for: indexPath) as! DiagnoseCellGroupTableViewCell
            let group = groupsToDiagnose[indexPath.row]
            let groupName = "\(group.value(forKey: "grade") as! String)\(group.value(forKey: "group") as! String)"
            
            cellDiagnose.nameDiagnose.text = "Diagnóstico: \(groupName)"
            cellDiagnose.diagnoseFinished.isHidden = !DiagnoseController.incompleteDiagnoseForGroup(group)
            
            cell = cellDiagnose
        }
        
        if tableView == self.tableViewDiagnoseSelection{
            
            //CellSelection
            let cellSelection = tableView.dequeueReusableCell(withIdentifier: "CellSelection",for: indexPath) as! DiagnoseSelectionTableViewCell
            cellSelection.nameDiagnose.text = self.diagnoseToSelect[indexPath.row].value(forKey: "name") as? String
            
            if positionSelectedDiagnose == indexPath.row{
                cellSelection.accessoryType = UITableViewCellAccessoryType.checkmark
            }else{
                cellSelection.accessoryType = UITableViewCellAccessoryType.none
            }
            
            cell = cellSelection
            
            
        }
        
        return cell!
        
    }
    
    func tableView(_ tableView: UITableView, didSelectRowAt indexPath: IndexPath) {
        
        if tableView == self.tableViewDiagnose{
            
            self.positionSelected = indexPath.row
            
            if DiagnoseController.incompleteDiagnoseByGroup(groupsToDiagnose[positionSelected]){
                
                let diagnose = DiagnoseController.getIncompleteDiagnose(groupsToDiagnose[self.positionSelected])
                let idTest = diagnose?.value(forKey: "idDiagnose") as! String
                let url = "\(Constants.TEST_ENDPOINT)\(idTest)"
                
                self.startAnimating(Constants.getSizeForLoadingView(view: self.view), type: NVActivityIndicatorType.ballBeat, color: ColorHelper.getSecondaryColor(1.0), padding: NVActivityIndicatorView.DEFAULT_PADDING)
                
                Alamofire.request(url, method: .get)
                    .validate()
                    .response(completionHandler: { (dataResponse) in
                        
                        self.stopAnimating()
                        
                        if let testList = TestController.getTestFromResponse(dataResponse){
                            
                            self.downloadPendingStudentsToOpenSegue(diagnose!,testList,self.groupsToDiagnose[self.positionSelected])
                            
                        }else{
                            
                            _ = SweetAlert().showAlert("Ooops!", subTitle: "¡Algo salió mal", style: AlertStyle.warning)
                            
                        }
                        
                    })
                

            }else{
            
                self.performSegue(withIdentifier: "diagnoseHistory", sender: self)
            
            }
            
        }
        
        if tableView == self.tableViewDiagnoseSelection{
            
            positionSelectedDiagnose = indexPath.row
            
            if let cell = tableView.cellForRow(at: indexPath){
                cell.accessoryType = .checkmark
            }
            
        }
        
    }
    
    
    func downloadPendingStudentsToOpenSegue(_ diagnoseObject: DiagnoseModel, _ testList: [TestModel],_ group: GroupModel){
        
        self.startAnimating(Constants.getSizeForLoadingView(view: self.view), type: NVActivityIndicatorType.ballBeat, color: ColorHelper.getSecondaryColor(1.0), padding: NVActivityIndicatorView.DEFAULT_PADDING)
        
        let url = "\(Constants.STUDENTS_PENDING_ENDPOINT)\(diagnoseObject.value(forKey: "idGroupDiagnose") as! String)"
        Alamofire.request(url, method: .get)
            .validate()
            .response { (dataResponse) in
                
                self.stopAnimating()
                
                if let idStudents = TestController.getStudentListForPendingTest(dataResponse){
                    
                    self.studentArrayToPass = TestController.getStudentArrayInDatabaseForPendingDiagnose(idStudents,group)
                    self.testToPass = testList
                    self.diagnoseComplete = true
                    self.diagnoseToPass = diagnoseObject
                    self.performSegue(withIdentifier: "diagnoseStudent", sender: self)

                    
                }else{
                    _ = SweetAlert().showAlert("Ooops!", subTitle: "¡Algo salió mal!", style: AlertStyle.warning)
                }
                
        }
        
    }
    
    func tableView(_ tableView: UITableView, didDeselectRowAt indexPath: IndexPath) {
        
        if tableView == self.tableViewDiagnoseSelection{
            
            positionSelectedDiagnose = -1
            
            if let cell = tableView.cellForRow(at: indexPath){
                cell.accessoryType = .none
            }
            
        }
        
    }
    
    @IBAction func selectGroupToDiagnose(_ sender: Any) {
        
        groups = GroupController.getGroupInDatabase()
        
        if groups.count > 0 {
            
            var stringName = [String]()
            
            for group in groups{
                var name = group.value(forKey: "grade") as! String
                name += group.value(forKey: "group") as! String
                stringName.append(name)
            }
            
            ActionSheetStringPicker.show(withTitle: "Aplicar al grupo", rows: stringName, initialSelection: 0, doneBlock: {
                picker, value, index in
                
                self.positionSelected = value
                self.isGroupSelected = true
                
                let text = "\(self.groups[self.positionSelected].value(forKey: "grade") as! String) \(self.groups[self.positionSelected].value(forKey: "group") as! String)"
                self.labelSelectedGroup.text = "Grupo seleccionado: " + text
                
                return
                
            }, cancel: { ActionStringCancelBlock in return }, origin: sender)
            
            
        }else{
            
            _ = SweetAlert().showAlert("Ooops!", subTitle: "No tienes grupos, crea uno y aplica el diagnóstico", style: AlertStyle.warning)
            
        }
        
    }
    
    
    @IBAction func startDiagnoseForGroup(_ sender: Any) {
        
        if self.isGroupSelected && self.positionSelectedDiagnose != -1 {
            
            let groupSelected = groups[self.positionSelected]
            
            if (StudentController.getStudentInDatabaseByGroup(groupSelected)?.count)! > 0{
                
                if !DiagnoseController.incompleteDiagnoseByGroup(groupSelected) {
                    
                    self.diagnoseComplete = false
                    
                    let idTest = self.diagnoseToSelect[self.positionSelectedDiagnose].value(forKey: "idDiagnose") as! String
                    let url = "\(Constants.TEST_ENDPOINT)\(idTest)"
                    
                    self.startAnimating(Constants.getSizeForLoadingView(view: self.view), type: NVActivityIndicatorType.ballBeat, color: ColorHelper.getSecondaryColor(1.0), padding: NVActivityIndicatorView.DEFAULT_PADDING)
                    
                    Alamofire.request(url, method: .get)
                        .validate()
                        .response(completionHandler: { (dataResponse) in
                            
                            self.stopAnimating()
                            
                            if let testList = TestController.getTestFromResponse(dataResponse){
                                
                                self.testToPass = testList
                                
                                self.startAnimating(Constants.getSizeForLoadingView(view: self.view), type: NVActivityIndicatorType.ballBeat, color: ColorHelper.getSecondaryColor(1.0), padding: NVActivityIndicatorView.DEFAULT_PADDING)
                                
                                let params = ["id_diagnostic":idTest,
                                              "id_group":groupSelected.value(forKey: "idGroup") as! String]
                                
                                
                                
                                Alamofire.request(Constants.GROUP_DIAGNOSTIC_ENDPOINT, method: .post, parameters: params)
                                    .validate()
                                    .response(completionHandler: { (dataResponse) in
                                        
                                        self.stopAnimating()
                                        
                                        if let diagnoseGroup = DiagnoseController.getDiagnoseResponseForGroup(dataResponse){
                                            
                                            self.diagnoseToPass = diagnoseGroup
                                            self.diagnoseToPass?.groups.append(groupSelected)
                                            
                                            self.performSegue(withIdentifier: "diagnoseStudent", sender: self)
                                            
                                        }else{
                                            
                                            _ = SweetAlert().showAlert("Ooops!", subTitle: "No tienes grupos, crea uno y aplica la planeación", style: AlertStyle.warning)
                                            
                                        }
                                        
                                    })
                                
                            }else{
                                
                                _ = SweetAlert().showAlert("Ooops!", subTitle: "¡Algo salió mal!", style: AlertStyle.warning)
                                
                            }
                            
                        })
                    
                }else{
                    
                    _ = SweetAlert().showAlert("Ooops!", subTitle: "Finaliza tu diagnóstico pendiente con este grupo para poder crear uno nuevo", style: AlertStyle.warning)
                    
                }
                
            }else{
                
                _ = SweetAlert().showAlert("Ooops!", subTitle: "No tienes alumnos en este grupo para aplicar la evaluación", style: AlertStyle.warning)
                
            }
            
            
        }else{
            _ = SweetAlert().showAlert("Ooops!", subTitle: "Selecciona un grupo y realiza el diagnóstico!\nSelecciona una tipo de evaluación", style: AlertStyle.warning)
        }
        
    }
    
    override func prepare(for segue: UIStoryboardSegue, sender: Any?) {
        
        
        if segue.identifier == "diagnoseStudent" && !diagnoseComplete {
            
            DiagnoseController.addDiagnoseToDatabase(self.diagnoseToPass!)
            
            let destinatioViewController = segue.destination as! StudentDiagnoseViewController
            destinatioViewController.group = groups[self.positionSelected]
            destinatioViewController.diagnose = self.diagnoseToPass!
            destinatioViewController.test = self.testToPass
            
            self.labelSelectedGroup.text = ""
            self.positionSelected = -1
            self.isGroupSelected = false
            
        }else if segue.identifier == "diagnoseStudent" && diagnoseComplete {
            
            let destinatioViewController = segue.destination as! StudentDiagnoseViewController
            destinatioViewController.group = groupsToDiagnose[self.positionSelected]
            destinatioViewController.diagnose = self.diagnoseToPass!
            destinatioViewController.test = self.testToPass
            destinatioViewController.studentArray = self.studentArrayToPass
            
            self.labelSelectedGroup.text = ""
            self.positionSelected = -1
            self.isGroupSelected = false
            self.diagnoseComplete = false
            
        }else if segue.identifier == "diagnoseHistory" {
            
            let diagnoseHistory = segue.destination as! DiagnoseHistoryViewController
            diagnoseHistory.group = groupsToDiagnose[positionSelected]
            
        }
        
    }
    
}
