//
//  Diary.swift
//  SoyEducadoraSereyd
//
//  Created by Isaias Carrera Ventura on 21/11/17.
//  Copyright © 2017 FullStack. All rights reserved.
//

import Foundation
import RealmSwift

class Diary: Object {
    
    dynamic var idDiary: Int = 0
    var title: String?
    var subject: String?
    var date: String?
    
}
