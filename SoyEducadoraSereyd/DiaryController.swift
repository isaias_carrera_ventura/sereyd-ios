//
//  DiaryController.swift
//  SoyEducadoraSereyd
//
//  Created by Isaias Carrera Ventura on 21/11/17.
//  Copyright © 2017 FullStack. All rights reserved.
//

import Foundation
import Alamofire
import RealmSwift

class DiaryController{
    
    
    class func getDiaryListForIdUser(_ dataResponse: DefaultDataResponse) -> [Diary]? {
        
        guard dataResponse.error == nil else{
            
            return nil
            
        }
        
        guard let dataResponse = dataResponse.data else{
            
            return nil
            
        }
        
        let jsonResponse = try? JSONSerialization.jsonObject(with: dataResponse, options: .allowFragments) as! [String:Any]
        
        guard (jsonResponse) != nil else{
            
            return nil
            
        }
        
        if (jsonResponse!["error"] as? Int) == 1 {
            
            return nil
            
        }
        
        
        var arrayDiary = [Diary]()
        
        let jsonArray = jsonResponse?["entries"] as! NSArray
        
        for object in jsonArray {
            
            let objectDiary = object as! [String:Any]
            
            let date = objectDiary["date_creation"] as! String
            let description = objectDiary["description"] as! String
            let idDiary = (objectDiary["id_diary"] as! NSString).intValue
            let title = objectDiary["title"] as! String
            
            let diary = Diary()
            diary.date = date
            diary.subject = description
            diary.idDiary = Int(idDiary)
            diary.title = title
            
            arrayDiary.append(diary)
            
        }
        
        return arrayDiary
    }
    
    
    class func getDiaryResponseCreation(_ dataResponse: DefaultDataResponse) -> Bool {
        
        guard dataResponse.error == nil else{
            
            return false
            
        }
        
        guard let dataResponse = dataResponse.data else{
            
            return false
            
        }
        
        let jsonResponse = try? JSONSerialization.jsonObject(with: dataResponse, options: .allowFragments) as! [String:Any]
        
        guard (jsonResponse) != nil else{
            
            return false
            
        }
        
        if (jsonResponse!["error"] as? Int) == 1 {
            
            return false
            
        }
        
        return true
    }
    
    
    class func saveListInDisk(_ list: [Diary]) -> Void{
        
        let diaryListDisk = getDiaryInDisk()
        
        for diaryServer in list{
            
            var isInDisk = false
            
            for diaryDisk in diaryListDisk{
                
                if diaryServer.idDiary == diaryDisk.value(forKey: "idDiary") as! Int{
                    
                    isInDisk = true
                    
                }
                
            }
            
            if !isInDisk {
                
                //SAVE
                saveDiaryInDatabase(diary: diaryServer)
                
            }
            
        }
        
    }
    
    class func getDiaryInDisk() -> [Diary] {
        
        let realm = try! Realm()
        let diaries = realm.objects(Diary.self)
        return Array(diaries)
        
    }
    
    class func saveDiaryInDatabase(diary: Diary) -> Void{
        
        let realm = try! Realm()
        
        try! realm.write {
            
            realm.add(diary)
            
        }
        
    }
    
}
