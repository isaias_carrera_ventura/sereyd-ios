//
//  DiaryViewController.swift
//  SoyEducadoraSereyd
//
//  Created by Isaias Carrera Ventura on 20/11/17.
//  Copyright © 2017 FullStack. All rights reserved.
//

import UIKit
import SwiftValidator
import Alamofire
import NVActivityIndicatorView

class DiaryViewController: UIViewController, UITextFieldDelegate, ValidationDelegate, NVActivityIndicatorViewable, UITableViewDelegate, UITableViewDataSource {
    
    @IBOutlet weak var subjectDiary: UITextView!
    @IBOutlet weak var titleDiary: UITextField!
    @IBOutlet weak var tableView: UITableView!
    
    var diaryEntries = [Diary]()
    let validator = Validator()
    
    override func viewDidLoad() {
        
        super.viewDidLoad()
        
        titleDiary.roundBorder()
        subjectDiary.roundBorder()
        titleDiary.delegate = self
        
        validator.registerField(titleDiary, rules: [RequiredRule(message: "Escribe el titúlo")])
        
        self.getListDiary()
        
    }
    
    func getListDiary() {
        
        let url = "\(Constants.URL_DIARY)/\(UserController.getUserInDatabase()?.value(forKey: "idUser") as! String)"
        
        Alamofire.request(url, method: .get)
            .validate()
            .response { (dataResponse) in
                
                let array = DiaryController.getDiaryListForIdUser(dataResponse)
                if array != nil {
                    
                    self.diaryEntries = array!
                    DiaryController.saveListInDisk(array!)
                    self.tableView.reloadData()
                    
                }else{
                    
                    self.diaryEntries = DiaryController.getDiaryInDisk()
                    self.tableView.reloadData()
                    
                }
                
        }
        
    }
    
    @IBAction func registerDiary(_ sender: Any) {
        
        validator.validate(self)
        
    }
    
    
    //KEYBOARD METHODS
    override func touchesBegan(_ touches: Set<UITouch>, with event: UIEvent?) {
        
        self.view.endEditing(true)
        
    }
    
    func textFieldShouldReturn(_ textField: UITextField) -> Bool {
        
        textField.resignFirstResponder()
        return true
        
    }
    
    override func didReceiveMemoryWarning() {
        super.didReceiveMemoryWarning()
        // Dispose of any resources that can be recreated.
    }
    
    func validationFailed(_ errors:[(Validatable ,ValidationError)]) {
        var stringError = ""
        
        for (field, error) in errors {
            if let field = field as? UITextField {
                field.layer.borderColor = UIColor.red.cgColor
                field.layer.borderWidth = 1.0
            }
            
            stringError = stringError + "\n\(error.errorMessage)"
            error.errorLabel?.text = error.errorMessage // works if you added labels
            error.errorLabel?.isHidden = false
            
        }
        
        
        self.view.endEditing(true)
        _ = SweetAlert().showAlert("Error", subTitle: "¡Ingresa los datos requeridos! \(stringError)", style: AlertStyle.error, buttonTitle:"Ok", buttonColor:ColorHelper.getGreenColor(1.0))
        
        
    }
    
    
    func validationSuccessful() {
        
        if self.subjectDiary.text == "" {
            
            _ = SweetAlert().showAlert("Error", subTitle: "¡Ingresa los datos requeridos!", style: AlertStyle.error, buttonTitle:"Ok", buttonColor:ColorHelper.getGreenColor(1.0))
            
        }else{
            
            let idUser = UserController.getUserInDatabase()?.value(forKey: "idUser") as! String
            let paramsToSend = ["title":titleDiary.text!,"description":subjectDiary.text,"id_user":idUser] as [String : Any]
            
            self.startAnimating(Constants.getSizeForLoadingView(view: self.view), type: NVActivityIndicatorType.ballBeat, color: ColorHelper.getSecondaryColor(1.0), padding: NVActivityIndicatorView.DEFAULT_PADDING)
            
            Alamofire.request(Constants.URL_DIARY, method: .post, parameters: paramsToSend)
                .validate()
                .response { (dataResponse) in
                    
                    self.stopAnimating()
                    
                    self.titleDiary.text = ""
                    self.subjectDiary.text = ""
                    
                    self.view.endEditing(true)
                    
                    if DiaryController.getDiaryResponseCreation(dataResponse){
                        
                        _ = SweetAlert().showAlert("Ok", subTitle: "Diario creado", style: AlertStyle.success, buttonTitle:"Ok", buttonColor:ColorHelper.getGreenColor(1.0))
                        
                        self.getListDiary()
                        
                    }else{
                        
                        _ = SweetAlert().showAlert("Ooops!", subTitle: "¡Algo salió mal!", style: AlertStyle.error, buttonTitle:"Ok", buttonColor:ColorHelper.getGreenColor(1.0))
                        
                    }
                    
            }
        }
        
    }
    
    public func tableView(_ tableView: UITableView, numberOfRowsInSection section: Int) -> Int{
        
        return self.diaryEntries.count
        
    }
    
    func tableView(_ tableView: UITableView, cellForRowAt indexPath: IndexPath) -> UITableViewCell {
        
        let cell = tableView.dequeueReusableCell(withIdentifier: "Cell", for: indexPath) as! DiaryTableViewCell
        cell.dateDiary.text = "Diario: \(self.diaryEntries[indexPath.row].value(forKey: "date") as! String)"
        return cell
        
    }
    
    var diaryToPass : Diary!
    
    func tableView(_ tableView: UITableView, didSelectRowAt indexPath: IndexPath) {
        
        diaryToPass = diaryEntries[indexPath.row]
        
        self.performSegue(withIdentifier: "detailDiaryViewController", sender: self)
        
    }
    
    override func prepare(for segue: UIStoryboardSegue, sender: Any?) {
        
        if segue.identifier == "detailDiaryViewController" {
            
            let diaryVC = segue.destination as! DiaryDetailViewController
            diaryVC.diaryToPass = self.diaryToPass
            
        }
        
    }
    
}
