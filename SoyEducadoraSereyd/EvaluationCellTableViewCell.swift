//
//  EvaluationCellTableViewCell.swift
//  SoyEducadoraSereyd
//
//  Created by Isaias Carrera Ventura on 29/08/17.
//  Copyright © 2017 FullStack. All rights reserved.
//

import UIKit

class EvaluationCellTableViewCell: UITableViewCell {

    @IBOutlet weak var nameEvaluation: UILabel!
    @IBOutlet weak var evaluationFinished: UILabel!
    
}
