//
//  EvaluationHistoryViewController.swift
//  SoyEducadoraSereyd
//
//  Created by Isaias Carrera Ventura on 06/09/17.
//  Copyright © 2017 FullStack. All rights reserved.
//
import Alamofire
import NVActivityIndicatorView
import UIKit
import MessageUI

class EvaluationHistoryViewController: UIViewController,UITableViewDelegate, UITableViewDataSource, NVActivityIndicatorViewable,MFMailComposeViewControllerDelegate{
    
    @IBOutlet weak var tableViewEvaluation: UITableView!
    var group : GroupModel?
    var evaluationList = [ResultDiagnose]()
    var evaluationInDatabase = [Evaluation]()
    
    override func viewDidLoad() {
        super.viewDidLoad()
        CustomBar.setNavBarProfile((self.navigationController?.navigationBar)!, navItem: navigationItem,nav: self)
        evaluationInDatabase = EvaluationController.getEvaluationForGroup(group!)!
        downloadForEvaluationResult(evaluationInDatabase)
        
        
    }
    
    func downloadForEvaluationResult(_ list: [Evaluation]){
        
        self.startAnimating(Constants.getSizeForLoadingView(view: self.view), type: NVActivityIndicatorType.ballBeat, color: ColorHelper.getSecondaryColor(1.0), padding: NVActivityIndicatorView.DEFAULT_PADDING)
        
        for diagnose in list{
            
            self.startAnimating(Constants.getSizeForLoadingView(view: self.view), type: NVActivityIndicatorType.ballBeat, color: ColorHelper.getSecondaryColor(1.0), padding: NVActivityIndicatorView.DEFAULT_PADDING)
            let urlDiagnose = "\(Constants.EVALUATION_COMPLETE_ENDPOINT)\(diagnose.value(forKey: "idGroupEvaluation") as! String)"
            Alamofire.request(urlDiagnose, method: .get)
                .validate()
                .response(completionHandler: { (dataResponse) in
                    
                    self.stopAnimating()
                    if let result = EvaluationController.getEvaluationResultFromResponse(dataResponse){
                        self.evaluationList.append(result)
                        self.tableViewEvaluation.reloadData()
                    }
                    
                })
        }
    }
    
    override func didReceiveMemoryWarning() {
        super.didReceiveMemoryWarning()
        // Dispose of any resources that can be recreated.
    }
    
    public func tableView(_ tableView: UITableView, numberOfRowsInSection section: Int) -> Int{
        
        return evaluationList.count
        
    }
    
    public func tableView(_ tableView: UITableView, cellForRowAt indexPath: IndexPath) -> UITableViewCell{
        
        let cell = tableView.dequeueReusableCell(withIdentifier: "Cell", for: indexPath) as! EvaluationResultTableViewCell
        cell.dateEvaluation.text = evaluationList[indexPath.row].name
        cell.name.text = evaluationList[indexPath.row].leve
        cell.percentage.text = "\(evaluationList[indexPath.row].percent!)%"
        cell.progress.progress = Float(evaluationList[indexPath.row].percent!)/100
        
        cell.tapExportAction = {(cell) in
            
            SweetAlert().showAlert("Exportar evaluación", subTitle: "Se exportará tu evaluación y se enviará vía correo electrónico.", style: AlertStyle.warning, buttonTitle:"Exportarc", buttonColor: ColorHelper.getGreenColor(1.0), otherButtonTitle:  "Cancelar", otherButtonColor: ColorHelper.getRedColor(1.0)) { (isOtherButton) -> Void in
                if isOtherButton == true {
                    
                    let url = Constants.URL_SERVER + "/group_evaluation/file/" + (self.evaluationInDatabase[indexPath.row].value(forKey: "idGroupEvaluation") as! String)
                    
                    self.startAnimating(Constants.getSizeForLoadingView(view: self.view), type: NVActivityIndicatorType.ballBeat, color: ColorHelper.getSecondaryColor(1.0), padding: NVActivityIndicatorView.DEFAULT_PADDING)
                    
                    Alamofire.request(url, method: .get, parameters: nil)
                        .validate()
                        .response(completionHandler: { (dataResponse) in
                            
                            self.stopAnimating()
                            
                            if let evalFilePath = EvaluationController.getEvaluationFilePath(dataResponse){
                                
                                let url = URL(string: Constants.URL_SERVER + "/" + evalFilePath)
                                let request = NSMutableURLRequest(url: url!)
                                
                                let task = URLSession.shared.dataTask(with: request as URLRequest){
                                    data, response, error in
                                    
                                    if error != nil{
                                        
                                        DispatchQueue.main.sync(execute: {
                                            
                                            self.stopAnimating()
                                            _ = SweetAlert().showAlert("Error", subTitle: "Algo salió mal , inténtalo de nuevo", style: AlertStyle.error)
                                            
                                        })
                                        
                                    }else{
                                        
                                        if let dataData = data{
                                            
                                            DispatchQueue.main.sync(execute: {
                                                
                                                self.stopAnimating()
                                                do{
                                                    
                                                    var docURL = (FileManager.default.urls(for: .documentDirectory, in: .userDomainMask)).last as NSURL?
                                                    
                                                    let evaluationFileName = EvaluationController.getFileNameForEvaluation(self.evaluationInDatabase[indexPath.row])
                                                    docURL = docURL?.appendingPathComponent(evaluationFileName) as NSURL?
                                                    try dataData.write(to: docURL! as URL)
                                                    
                                                    //UPDATE UI
                                                    
                                                    let mailComposeViewController = self.configuredMailComposeViewController(self.evaluationInDatabase[indexPath.row])
                                                    
                                                    if MFMailComposeViewController.canSendMail() {
                                                        
                                                        self.present(mailComposeViewController, animated: true, completion: nil)
                                                        
                                                    } else {
                                                        
                                                        self.showSendMailErrorAlert()
                                                        
                                                    }
                                                    
                                                }catch{
                                                    
                                                    _ = SweetAlert().showAlert("Error", subTitle: "Algo salió mal , inténtalo de nuevo", style: AlertStyle.error)
                                                    
                                                }
                                                
                                            })
                                            
                                        }else{
                                            
                                            DispatchQueue.main.sync(execute: {
                                                
                                                self.stopAnimating()
                                                _ = SweetAlert().showAlert("Error", subTitle: "Algo salió mal , inténtalo de nuevo", style: AlertStyle.error)
                                                
                                            })
                                            
                                        }
                                        
                                    }
                                    
                                }
                                
                                task.resume()
                                
                                
                            }else{
                                
                                _ = SweetAlert().showAlert("Ooops!", subTitle: "Algo salió mal", style: AlertStyle.warning)
                                
                            }
                            
                        })
                    
                    
                    
                }
            }
            
        }
        
        return cell
        
    }
    
    func configuredMailComposeViewController(_ evaluation: Evaluation) -> MFMailComposeViewController {
        
        let mailComposerVC = MFMailComposeViewController()
        mailComposerVC.mailComposeDelegate = self
        
        //mailComposerVC.setToRecipients(["nurdin@gmail.com"])
        mailComposerVC.setSubject("Evaluación")
        mailComposerVC.setMessageBody("PDF de Evaluación adjunto", isHTML: false)
        
        let pathFileWord = EvaluationController.getFileForEvaluation(evaluation)
        let fileDataWord = NSData(contentsOf: pathFileWord!)
        let fileNameWord = EvaluationController.getFileNameForEvaluation(evaluation)
        
        mailComposerVC.addAttachmentData(fileDataWord! as Data, mimeType: "application/vnd.openxmlformats-officedocument.wordprocessingml.document", fileName: fileNameWord)
        
        return mailComposerVC
    }
    
    func showSendMailErrorAlert() {
        
        _ = SweetAlert().showAlert("No pudimos mandar el correo", subTitle: "Tu dispositivo no puede mandar email, configura una cuenta e inténtalo de nuevo.", style: AlertStyle.warning)
        
    }
    
    func mailComposeController(_ controller: MFMailComposeViewController, didFinishWith result: MFMailComposeResult, error: Error?) {
        controller.dismiss(animated: true, completion: nil)
        
    }
    
}
