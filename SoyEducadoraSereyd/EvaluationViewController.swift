//
//  EvaluationViewController.swift
//  SoyEducadoraSereyd
//
//  Created by Isaias Carrera Ventura on 23/08/17.
//  Copyright © 2017 FullStack. All rights reserved.
//

import UIKit
import ActionSheetPicker_3_0
import Alamofire
import NVActivityIndicatorView

class EvaluationViewController: UIViewController, UITableViewDelegate, UITableViewDataSource, NVActivityIndicatorViewable {
    
    @IBOutlet weak var tableViewEvaluationSelection: UITableView!
    @IBOutlet weak var selectGroup: UIButton!
    @IBOutlet weak var labelSelectGroup: UILabel!
    @IBOutlet weak var tableViewEvaluation: UITableView!
    
    var groups = [GroupModel]()
    var evaluationAv = [EvaluationSelection]()
    var studentArrayToPass : [StudentModel]?
    var isGroupSelected = false
    var positionSelected = -1
    var positionSelectedEvaluation = -1
    
    var groupsToEvaluate = [GroupModel]()
    
    override func viewDidLoad() {
        super.viewDidLoad()
        
        CustomBar.setNavBarProfile((self.navigationController?.navigationBar)!, navItem: navigationItem,nav: self)
        selectGroup.backgroundColor = .clear
        selectGroup.layer.cornerRadius = 10
        selectGroup.layer.borderWidth = 2
        selectGroup.layer.borderColor = ColorHelper.getSecondaryColor(1.0).cgColor
        
        fetchEvaluationAvailable()
        
    }
    
    override func viewDidAppear(_ animated: Bool) {
        
        groupsToEvaluate.removeAll()
        self.tableViewEvaluation.reloadData()
        requestForGroupEvaluation()
        
    }
    
    func requestForGroupEvaluation(){
    
        let groups = GroupController.getGroupInDatabase()
        
        for group in groups{
            
            self.startAnimating(Constants.getSizeForLoadingView(view: self.view), type: NVActivityIndicatorType.ballBeat, color: ColorHelper.getSecondaryColor(1.0), padding: NVActivityIndicatorView.DEFAULT_PADDING)

            let url = "\(Constants.GROUP_EVALUATION_PENDING_ENDPOINT)\(group.value(forKey: "idGroup") as! String)"
            
            Alamofire.request(url, method: .get)
                .validate()
                .response { (dataResponse) in
                    
                    self.stopAnimating()
                    if let evaluationForUser = EvaluationController.getAvailableEvaluationForUserGroup(dataResponse){
                        
                        if evaluationForUser.count > 0{
                            
                            self.groupsToEvaluate.append(group)
                            EvaluationController.addEvaluationListToDatabase(evaluationForUser,group)
                            self.tableViewEvaluation.reloadData()
                            
                        }
                        
                    }
                    
            }
        }
    }
    
    func fetchEvaluationAvailable(){
        
        self.startAnimating(Constants.getSizeForLoadingView(view: self.view), type: NVActivityIndicatorType.ballBeat, color: ColorHelper.getSecondaryColor(1.0), padding: NVActivityIndicatorView.DEFAULT_PADDING)
        
        let url = "\(Constants.EVALUATION_USER)\(UserController.getUserInDatabase()?.value(forKey: "idUser") as! String)"
        Alamofire.request(url, method: .get)
            .validate()
            .response { (dataResponse) in
                
                self.stopAnimating()
                if let evaluations = EvaluationController.getAvailableEvaluationFromResponse(dataResponse){
                    
                    if evaluations.count > 0{
                        
                        self.evaluationAv = evaluations
                        self.tableViewEvaluationSelection.reloadData()
                    
                    }else{
                    
                        _ = SweetAlert().showAlert("Ooops!", subTitle: "No tienes evaluaciones disponibles", style: AlertStyle.warning)

                    }
                }else{
                    _ = SweetAlert().showAlert("Ooops!", subTitle: "¡Algo salió mal!", style: AlertStyle.warning)
                }
        }

        
    }
    
    override func didReceiveMemoryWarning() {
        super.didReceiveMemoryWarning()
        // Dispose of any resources that can be recreated.
    }
    
    func tableView(_ tableView: UITableView, numberOfRowsInSection section: Int) -> Int {
        
        var count : Int?
        
        if tableView == self.tableViewEvaluationSelection{
            
            count = evaluationAv.count
        }
        
        if tableView == self.tableViewEvaluation{
        
            count = groupsToEvaluate.count
        
        }
        
        return count!
    }
    
    func tableView(_ tableView: UITableView, cellForRowAt indexPath: IndexPath) -> UITableViewCell {
        
        var cell: UITableViewCell?
        if tableView == self.tableViewEvaluationSelection{
            
            //CellSelection
            let cellSelection = tableView.dequeueReusableCell(withIdentifier: "CellSelection",for: indexPath) as! EvaluationSelectionTableViewCell
            cellSelection.nameEvaluation.text = evaluationAv[indexPath.row].name!
            
            if positionSelectedEvaluation == indexPath.row{
                cellSelection.accessoryType = UITableViewCellAccessoryType.checkmark
            }else{
                cellSelection.accessoryType = UITableViewCellAccessoryType.none
            }
            
            cell = cellSelection
            
        }
        if tableView == self.tableViewEvaluation{
            
            let cellEvaluation = tableView.dequeueReusableCell(withIdentifier: "Cell",for: indexPath) as! EvaluationCellTableViewCell
            let group = groupsToEvaluate[indexPath.row]
            let groupName = "\(group.value(forKey: "grade") as! String)\(group.value(forKey: "group") as! String)"
            
            cellEvaluation.nameEvaluation.text = "Evaluación: \(groupName)"
            cellEvaluation.evaluationFinished.isHidden = !EvaluationController.incompleteEvaluationForGroup(group)
            
            cell = cellEvaluation
            
        }
        return cell!
    }
    
    func tableView(_ tableView: UITableView, didSelectRowAt indexPath: IndexPath) {
        
        
        if tableView == self.tableViewEvaluationSelection{
        
            positionSelectedEvaluation = indexPath.row
            
            if let cell = tableView.cellForRow(at: indexPath){
                cell.accessoryType = .checkmark
            }
            
        }
        
        if tableView == self.tableViewEvaluation{
            
            self.positionSelected = indexPath.row
            if EvaluationController.incompleteEvaluationForGroup(groupsToEvaluate[positionSelected]){
                
                let evaluation = EvaluationController.getIncompleteEvaluation(groupsToEvaluate[self.positionSelected])
                let idEvaluation = evaluation?.value(forKey: "idEvaluation") as! String
                let url = "\(Constants.TEST_ENDPOINT_EVALUATION)\(idEvaluation)"
                
                self.startAnimating(Constants.getSizeForLoadingView(view: self.view), type: NVActivityIndicatorType.ballBeat, color: ColorHelper.getSecondaryColor(1.0), padding: NVActivityIndicatorView.DEFAULT_PADDING)
                
                Alamofire.request(url, method: .get)
                    .validate()
                    .response(completionHandler: { (dataResponse) in
                        
                        self.stopAnimating()
                        
                        if let testList = EvaluationController.getTestFromResponse(dataResponse){
                            
                            self.downloadPendingStudentsToOpenSegue(evaluation!,testList,self.groupsToEvaluate[self.positionSelected])
                            
                        }else{
                            
                            _ = SweetAlert().showAlert("Ooops!", subTitle: "¡Algo salió mal", style: AlertStyle.warning)
                            
                        }
                        
                    })
                
            }else{
                 self.performSegue(withIdentifier: "evaluationHistory", sender: self)
            }
        }
        
    }
    
    
    func downloadPendingStudentsToOpenSegue(_ evaluation: Evaluation, _ testList: [TestModel],_ group: GroupModel){
        
        self.startAnimating(Constants.getSizeForLoadingView(view: self.view), type: NVActivityIndicatorType.ballBeat, color: ColorHelper.getSecondaryColor(1.0), padding: NVActivityIndicatorView.DEFAULT_PADDING)
        
        let url = "\(Constants.STUDENTS_PENDING_EVALUATION_ENDPOINT)\(evaluation.value(forKey: "idGroupEvaluation") as! String)"
        Alamofire.request(url, method: .get)
            .validate()
            .response { (dataResponse) in
                
                self.stopAnimating()
                
                if let idStudents = TestController.getStudentListForPendingTest(dataResponse){
                    
                    self.studentArrayToPass = TestController.getStudentArrayInDatabaseForPendingDiagnose(idStudents,group)
                    self.testToPass = testList
                    self.evaluationComplete = true
                    self.evaluationToPass = evaluation
                    self.performSegue(withIdentifier: "questionEvaluationSegue", sender: self)
                    
                    
                }else{
                    _ = SweetAlert().showAlert("Ooops!", subTitle: "Algo salió mal", style: AlertStyle.warning)
                }
                
        }
        
    }
    
    func tableView(_ tableView: UITableView, didDeselectRowAt indexPath: IndexPath) {
        
        if tableView == self.tableViewEvaluationSelection{
            
            positionSelectedEvaluation = -1
            
            if let cell = tableView.cellForRow(at: indexPath){
                cell.accessoryType = .none
            }
            
        }
        
    }
    
    
    @IBAction func selectGroupToEvaluation(_ sender: Any) {
        
        groups = GroupController.getGroupInDatabase()
        
        if groups.count > 0 {
            
            var stringName = [String]()
            
            for group in groups{
                var name = group.value(forKey: "grade") as! String
                name += group.value(forKey: "group") as! String
                stringName.append(name)
            }
            
            ActionSheetStringPicker.show(withTitle: "Aplicar al grupo", rows: stringName, initialSelection: 0, doneBlock: {
                picker, value, index in
                
                self.positionSelected = value
                self.isGroupSelected = true
                
                let text = "\(self.groups[self.positionSelected].value(forKey: "grade") as! String) \(self.groups[self.positionSelected].value(forKey: "group") as! String)"
                self.labelSelectGroup.text = "Grupo seleccionado: " + text
                
                return
                
            }, cancel: { ActionStringCancelBlock in return }, origin: sender)
            
            
        }else{
            
            _ = SweetAlert().showAlert("Ooops!", subTitle: "No tienes grupos, crea uno y aplica la evaluación", style: AlertStyle.warning)
            
        }
        
    }
    
    var testToPass : [TestModel]?
    var evaluationToPass : Evaluation?
    var evaluationComplete : Bool = false
    
    @IBAction func startEvaluationForGroup(_ sender: Any) {
        
        if self.isGroupSelected && self.positionSelectedEvaluation != -1 {
            
            let groupSelected = groups[self.positionSelected]
            if (StudentController.getStudentInDatabaseByGroup(groupSelected)?.count)! > 0{
                
                if !EvaluationController.incompleteEvaluationForGroup(groupSelected){
                    let idEvaluation = evaluationAv[self.positionSelectedEvaluation].idEvaluation!
                    let url = "\(Constants.EVALUATION)\(idEvaluation)"
                    self.evaluationComplete = false
                    
                    self.startAnimating(Constants.getSizeForLoadingView(view: self.view), type: NVActivityIndicatorType.ballBeat, color: ColorHelper.getSecondaryColor(1.0), padding: NVActivityIndicatorView.DEFAULT_PADDING)
                    
                    Alamofire.request(url, method: .get)
                        .validate()
                        .response(completionHandler: { (dataResponse) in
                            
                            self.stopAnimating()
                            
                            if let testList = EvaluationController.getTestFromResponse(dataResponse){
                                
                                self.testToPass = testList
                                
                                self.startAnimating(Constants.getSizeForLoadingView(view: self.view), type: NVActivityIndicatorType.ballBeat, color: ColorHelper.getSecondaryColor(1.0), padding: NVActivityIndicatorView.DEFAULT_PADDING)
                                
                                let params = ["id_evaluation":idEvaluation,
                                              "id_group":groupSelected.value(forKey: "idGroup") as! String]
                                
                                
                                Alamofire.request(Constants.GROUP_EVALUATION_ENDPOINT, method: .post, parameters: params)
                                    .validate()
                                    .response(completionHandler: { (dataResponse) in
                                        
                                        self.stopAnimating()
                                        
                                        if let evaluationGroup = EvaluationController.getEvaluationResponseForGroup(dataResponse){
                                            
                                            self.evaluationToPass = evaluationGroup
                                            self.evaluationToPass?.groups.append(groupSelected)
                                            self.performSegue(withIdentifier: "questionEvaluationSegue", sender: self)
                                            
                                        }else{
                                            
                                            _ = SweetAlert().showAlert("Ooops!", subTitle: "No tienes grupos, crea uno y aplica la planeación", style: AlertStyle.warning)
                                            
                                        }
                                        
                                    })
                                
                                
                                
                            }else{
                                _ = SweetAlert().showAlert("Ooops!", subTitle: "Algo salió mal", style: AlertStyle.warning)
                            }
                            
                        })
                    
                }else{
                    
                    _ = SweetAlert().showAlert("Ooops!", subTitle: "Finaliza tu evaluación pendiente con este grupo para poder crear una nuevo", style: AlertStyle.warning)
                    
                }
                
            }else{
                 _ = SweetAlert().showAlert("Ooops!", subTitle: "No tienes alumnos en este grupo para aplicar la evaluación", style: AlertStyle.warning)
            }
            
            
        }else{
            _ = SweetAlert().showAlert("Ooops!", subTitle: "Selecciona un grupo y realiza el diagnóstico!\nSelecciona una tipo de evaluación", style: AlertStyle.warning)
        }
        
    }
    
    override func prepare(for segue: UIStoryboardSegue, sender: Any?) {
        
        if segue.identifier == "questionEvaluationSegue" && !evaluationComplete{
        
            EvaluationController.addEvaluationToDatabase(self.evaluationToPass!)
            
            let destinatioViewController = segue.destination as! StudentEvaluationViewController
            destinatioViewController.group = groups[self.positionSelected]
            destinatioViewController.test = self.testToPass
            destinatioViewController.evaluation = self.evaluationToPass
            
            self.labelSelectGroup.text = ""
            self.positionSelected = -1
            self.isGroupSelected = false
        
        }else if segue.identifier == "questionEvaluationSegue" && evaluationComplete{
            
            let destinatioViewController = segue.destination as! StudentEvaluationViewController
            destinatioViewController.group = groupsToEvaluate[self.positionSelected]
            destinatioViewController.evaluation = self.evaluationToPass!
            destinatioViewController.test = self.testToPass
            destinatioViewController.studentArray = self.studentArrayToPass
            
            self.labelSelectGroup.text = ""
            self.positionSelected = -1
            self.isGroupSelected = false
            self.evaluationComplete = false
            
        }else if segue.identifier == "evaluationHistory" {
            
            let evaluationHistory = segue.destination as! EvaluationHistoryViewController
            evaluationHistory.group = groupsToEvaluate[positionSelected]
            
        }
        
    }
    
}
