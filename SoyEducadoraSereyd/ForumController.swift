//
//  ForumController.swift
//  SoyEducadoraSereyd
//
//  Created by Isaias Carrera Ventura on 19/12/17.
//  Copyright © 2017 FullStack. All rights reserved.
//

import Foundation
import Alamofire

class ForumController{
    
    class func getForumList(_ dataResponse: DefaultDataResponse) -> [ForumModel]?{
        
        
        guard dataResponse.error == nil else{
            
            return nil
            
        }
        
        guard let dataResponse = dataResponse.data else{
            
            return nil
            
        }
        
        let jsonResponse = try? JSONSerialization.jsonObject(with: dataResponse, options: .allowFragments) as! [String:Any]
        
        guard (jsonResponse) != nil else{
            
            return nil
            
        }
        
        if (jsonResponse!["error"] as? Int) == 1 {
            
            return nil
            
        }
        
        guard let questions = jsonResponse?["questions"] as? NSArray else{
            
            return nil
        }
     
        var arrayForum = [ForumModel]()
        
        for item in questions{
            
            let itemObject = item as! [String:Any]
            
            let idDiscussion = itemObject["id_discussion"] as! String
            let title = itemObject["title"] as! String
            let question = itemObject["question"] as! String
            let date = itemObject["date_creation"] as! String
            let ranking = (itemObject["ranking"] as! NSString).intValue
            let idUser = itemObject["id_user"] as! String
            let name = itemObject["name"] as! String
            let image = Constants.URL_SERVER + (itemObject["image"] as! String)
            
            arrayForum.append(ForumModel(id: idDiscussion, title: title, question: question, date: date, ranking: Int(ranking), idUser: idUser, name: name, image: image))
        }
        
        return arrayForum
    }
    
    class func getAnswerList(_ dataResponse: DefaultDataResponse) -> [Answer]?{
        
        
        guard dataResponse.error == nil else{
            
            return nil
            
        }
        
        guard let dataResponse = dataResponse.data else{
            
            return nil
            
        }
        
        let jsonResponse = try? JSONSerialization.jsonObject(with: dataResponse, options: .allowFragments) as! [String:Any]
        
        guard (jsonResponse) != nil else{
            
            return nil
            
        }
        
        if (jsonResponse!["error"] as? Int) == 1 {
            
            return nil
            
        }
        
        guard let questions = jsonResponse?["answers"] as? NSArray else{
            
            return nil
        }
        
        var arrayAnswer = [Answer]()
        
        for item in questions{
            
            let itemObject = item as! [String:Any]
            
            let idAnswer = itemObject["id_answer"] as! String
            let answer = itemObject["answer"] as! String
            let date = itemObject["date_creation"] as! String
            let name = itemObject["name"] as! String
            let image = Constants.URL_SERVER + (itemObject["image"] as! String)
            let ranking = (itemObject["ranking"] as! NSString).integerValue
            arrayAnswer.append(Answer(id: idAnswer, answer: answer, date: date, image: image, name: name,ranking: ranking))
            
        }
        
        return arrayAnswer
    }
    
    
}
