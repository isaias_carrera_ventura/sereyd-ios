//
//  ForumListViewController.swift
//  SoyEducadoraSereyd
//
//  Created by Isaias Carrera Ventura on 19/12/17.
//  Copyright © 2017 FullStack. All rights reserved.
//
import Alamofire
import UIKit
import Kingfisher
import NVActivityIndicatorView

class ForumListViewController: UIViewController, UITableViewDataSource, UITableViewDelegate, NVActivityIndicatorViewable{

    @IBOutlet weak var tableView: UITableView!
    var listForum = [ForumModel]()
    
    override func viewDidLoad() {
        
        super.viewDidLoad()
        
        CustomBar.setNavBarProfile((self.navigationController?.navigationBar)!, navItem: navigationItem,nav: self)
        
    }
    
    override func viewDidAppear(_ animated: Bool) {
        
        
        self.startAnimating(Constants.getSizeForLoadingView(view: self.view), type: NVActivityIndicatorType.ballBeat, color: ColorHelper.getSecondaryColor(1.0), padding: NVActivityIndicatorView.DEFAULT_PADDING)
        
        
        Alamofire.request(Constants.DISCUSSION_ENDPOINT, method: .get)
        .validate()
        .response { (dataResponse) in
            
            self.stopAnimating()
            
            if let arrayList = ForumController.getForumList(dataResponse){
                
                if arrayList.count > 0{
                    
                    self.listForum.removeAll()
                    self.listForum.append(contentsOf: arrayList)
                    self.tableView.reloadData()
                }else{
                    
                    _ = SweetAlert().showAlert("Ooops!", subTitle: "¡No hay resultados!", style: AlertStyle.warning, buttonTitle:"Ok", buttonColor:ColorHelper.getGreenColor(1.0))
                }
                
            }else{
                
                _ = SweetAlert().showAlert("Ooops!", subTitle: "¡Algo salió mal!", style: AlertStyle.error, buttonTitle:"Ok", buttonColor:ColorHelper.getGreenColor(1.0))
                
            }
            
        }
        
    }

    override func didReceiveMemoryWarning() {
        super.didReceiveMemoryWarning()
        // Dispose of any resources that can be recreated.
    }
    
    public func tableView(_ tableView: UITableView, numberOfRowsInSection section: Int) -> Int{
        
        return listForum.count
        
    }


    var modelSelected : ForumModel?
    
    func tableView(_ tableView: UITableView, didSelectRowAt indexPath: IndexPath) {
        
        modelSelected = self.listForum[indexPath.row]
        self.performSegue(withIdentifier: "detailForumQuestionSegue", sender: self)
        
    }
    
    
    
    public func tableView(_ tableView: UITableView, cellForRowAt indexPath: IndexPath) -> UITableViewCell{
        
        let cell = tableView.dequeueReusableCell(withIdentifier: "Cell", for: indexPath) as! ForumTableViewCell
        
        cell.titleLabel.text = self.listForum[indexPath.row].title!
        cell.questionLabel.text = self.listForum[indexPath.row].question!
        
        
        let urlImage = self.listForum[indexPath.row].image!
        let url = URL(string: urlImage)
        KingfisherManager.shared.retrieveImage(with: url!, options: nil, progressBlock: nil, completionHandler: { image, error, cacheType, imageURL in
            
            if error == nil{
                
                cell.imageProfile.image = ImageUtils.resizeImage(image: image!, newWidth: 40.0)
                ImageUtils.circularImage(cell.imageProfile, color: ColorHelper.getSecondaryColor(1.0).cgColor)
                
            }
            
        })

        
        return cell
        
    }
    
    
    
    override func viewWillAppear(_ animated: Bool) {
        
        tableView.estimatedRowHeight = 150
        tableView.rowHeight = UITableViewAutomaticDimension
        
    }

    @IBAction func doQuestion(_ sender: Any) {
        
        self.performSegue(withIdentifier: "doQuestionSegue",sender: self)
        
    }
    
    override func prepare(for segue: UIStoryboardSegue, sender: Any?) {
        
        if segue.identifier == "detailForumQuestionSegue" {
        
            let vc = segue.destination as! ForumDetailViewController
            vc.model = self.modelSelected
            
        }
        
    }
}
