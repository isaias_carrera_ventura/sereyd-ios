//
//  ForumModel.swift
//  SoyEducadoraSereyd
//
//  Created by Isaias Carrera Ventura on 19/12/17.
//  Copyright © 2017 FullStack. All rights reserved.
//

import Foundation

class ForumModel {
    
    var idDiscussion: String?
    var title: String?
    var question: String?
    var date: String?
    var ranking: Int = 0
    var idUser: String?
    var name: String?
    var image: String?
    
    init(id: String,title: String, question: String,date: String,ranking: Int,idUser:String,name: String,image: String) {
        
        self.idDiscussion = id
        self.title = title
        self.question = question
        self.date = date
        self.ranking = ranking
        self.idUser = idUser
        self.name = name
        self.image = image
        
    }
    
    
}
