//
//  ForumTableViewCell.swift
//  SoyEducadoraSereyd
//
//  Created by Isaias Carrera Ventura on 19/12/17.
//  Copyright © 2017 FullStack. All rights reserved.
//

import UIKit
import Cosmos

class ForumTableViewCell: UITableViewCell {

    @IBOutlet weak var titleLabel: UILabel!
    @IBOutlet weak var questionLabel: UITextView!
    @IBOutlet weak var imageProfile: UIImageView!

}
