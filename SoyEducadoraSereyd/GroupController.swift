//
//  GroupController.swift
//  SoyEducadoraSereyd
//
//  Created by Isaias Carrera Ventura on 24/05/17.
//  Copyright © 2017 FullStack. All rights reserved.
//

import Foundation
import RealmSwift
import Alamofire

class GroupController {
	
	class func saveGroupInDatabase(group: GroupModel) -> Void{
		
		let realm = try! Realm()
		
		try! realm.write {
			
			realm.add(group)
			
		}
		
	}
	
    class func editGroupInDatabase(groupEdit: GroupModel, school: String, grade: String, group: String,period: String,user: UserModel, percentage: Double) -> Void{
		
		let realm  = try! Realm()
		
		try! realm.write {
			
			groupEdit.setValue(school, forKey: "school")
			groupEdit.setValue(grade, forKey: "grade")
			groupEdit.setValue(group, forKey: "group")
			groupEdit.setValue(period, forKey: "period")
			groupEdit.setValue(user, forKey: "user")
            groupEdit.setValue(percentage, forKey: "percentage")
			
		}
		
	}
    
    class func editGroupInDatabase(groupEdit: GroupModel, school: String, grade: String, group: String,period: String,user: UserModel) -> Void{
        
        let realm  = try! Realm()
        
        try! realm.write {
            
            groupEdit.setValue(school, forKey: "school")
            groupEdit.setValue(grade, forKey: "grade")
            groupEdit.setValue(group, forKey: "group")
            groupEdit.setValue(period, forKey: "period")
            groupEdit.setValue(user, forKey: "user")
            
        }
        
    }
	
	class func deleteGroupInDatabase(group: GroupModel) -> Void{
		
		let realm = try! Realm()
		
		// Delete an object with a transaction
		try! realm.write {
			
			//delete student list in group
			let list = group.value(forKey: "students") as! LinkingObjects<StudentModel>
			for student in list{
				
				realm.delete(student)
				
			}
			//delete group object
			realm.delete(group)
			
		}
	
	}
	
	class func getGroupFromResponse(_ dataResponse: DefaultDataResponse) -> GroupModel? {
		
		guard dataResponse.error == nil else{
			
			return nil
			
		}
		
		guard let dataResponse = dataResponse.data else{
			
			return nil
			
		}
		
		let jsonResponse = try? JSONSerialization.jsonObject(with: dataResponse, options: .allowFragments) as! [String:Any]
		
		guard (jsonResponse) != nil else{
			
			return nil
			
		}
		
		
		if (jsonResponse!["error"] as? Int) == 1 {
			
			return nil
			
		}
		
		let groupRaw = jsonResponse!["group"] as! [String:Any]
		let id_group = groupRaw["id_group"] as! String
		let school = groupRaw["school"] as! String
		let grade = groupRaw["grade"] as! String
		let group_number = groupRaw["group"] as! String
		let period = groupRaw["period"] as! String
    
        var percentage = 0.0
        if let doubleValue = groupRaw["attendance_percent"] as? Double{
            percentage = doubleValue
        }
        
		let groupReturn = GroupModel()
		groupReturn.idGroup = id_group
		groupReturn.period = period
		groupReturn.group = group_number
		groupReturn.grade = "\(grade)"
		groupReturn.school = school
        groupReturn.percentage = percentage
		
		return groupReturn
		
	}
	
	class func getGroupInDatabase() -> [GroupModel] {
		
		let realm = try! Realm()
		let groups = realm.objects(GroupModel.self)
		return Array(groups)
		
	}

	class func getGroupInDatabaseAsResult() -> Results<GroupModel> {
		
		let realm = try! Realm()
		let groups = realm.objects(GroupModel.self)
		return groups
		
	}

	
	class func getGroupListFromResponse(_ dataResponse: DefaultDataResponse) -> [GroupModel]? {
		
		guard dataResponse.error == nil else{
			
			return nil
			
		}
		
		guard let dataResponse = dataResponse.data else{
			
			return nil
			
		}
		
		let jsonResponse = try? JSONSerialization.jsonObject(with: dataResponse, options: .allowFragments) as! [String:Any]
		
		guard (jsonResponse) != nil else{
			
			return nil
			
		}
		
		if (jsonResponse!["error"] as? Int) == 1 {
			
			return nil
			
		}
		
		guard let groupArray = jsonResponse?["data"] as? NSArray else{
			
			return nil
		}

		var groupArrayToReturn : [GroupModel] = [GroupModel]()

		
		if groupArray.count == 0 {
			
			return groupArrayToReturn
			
		}
		
		for groupObject in groupArray{
			
			let groupUnwrapped = groupObject as! [String:Any]
			let groupDict = groupUnwrapped["group"] as! NSDictionary
			
			let groupAppend = GroupModel()
			groupAppend.idGroup = (groupDict["id_group"] as! String)
			groupAppend.school = groupDict["school"] as? String
			groupAppend.grade = groupDict["grade"] as? String
			groupAppend.period = groupDict["period"] as? String
			groupAppend.group = groupDict["group_number"] as? String
            
            if let doubleValue = groupDict["attendance_percent"] as? Double{
                groupAppend.percentage = doubleValue
            }
            groupArrayToReturn.append(groupAppend)
			
		}
		
		return groupArrayToReturn
		
	}

	
	class func saveListUnrepeatedGroupInList(_ groupArray : [GroupModel],_ user: UserModel ) -> Void{
		
		let listGroupInDatabase = getGroupInDatabaseAsResult()
		
		if  groupArray.count > listGroupInDatabase.count{
			
			//List from Server bigger than List from local database
			for group in groupArray{
				
				var isGroupInDatabase = false
				
				for groupInDb in listGroupInDatabase{
					
					if(group.idGroup == groupInDb.value(forKey: "idGroup") as? String){
						isGroupInDatabase = true
					}
					
				}
				
				if !isGroupInDatabase{
					saveGroupInDatabase(group: group)
				}
				
			}
			
		}else if groupArray.count < listGroupInDatabase.count{
			
			
			for group in listGroupInDatabase{
				
				var isGroupInServer = false
				
				for groupInServer in groupArray {
					
					if group.value(forKey: "idGroup") as? String == groupInServer.idGroup{
						
						isGroupInServer = true
						
					}
					
				}
				
				if !isGroupInServer {
					
					deleteGroupInDatabase(group: group)
					
				}
				
			}
			
			
		}
		
		saveChangesInDatabaseUpdate(getGroupInDatabase(),groupArray,user)
		
		
	}
	
	class func saveChangesInDatabaseUpdate(_ elementsInDisk: [GroupModel],_ elementsFromResponse : [GroupModel],_ user: UserModel) -> Void{
		
		for group in elementsInDisk{
			
			if let groupAux = getGroupInServerById(group.value(forKey: "idGroup") as! String, elementsFromResponse){
                editGroupInDatabase(groupEdit: group, school: groupAux.school!, grade: groupAux.grade!, group: groupAux.group!, period: groupAux.period!, user: user , percentage: groupAux.percentage)
			}
		}
		
	}
	
	class func getGroupInServerById(_ idGroup: String,_ elements: [GroupModel] ) -> GroupModel?{
		
		for group in elements{
			
			if group.idGroup == idGroup{
				return group
			}
			
		}
		
		return nil
		
	}
	
	class func deleteGroupFromResponse(_ dataResponse: DefaultDataResponse) -> Bool? {
		
		
		guard dataResponse.error == nil else{
			
			return nil
			
		}
		
		guard let dataResponse = dataResponse.data else{
			
			return nil
			
		}
		
		let jsonResponse = try? JSONSerialization.jsonObject(with: dataResponse, options: .allowFragments) as! [String:Any]
		
		guard (jsonResponse) != nil else{
			
			return nil
			
		}
		
		if (jsonResponse!["error"] as? Int) == 1 {
			
			return false
			
		}
		
		return true
		
	}
	
	class func updateGroupInServer(_ dataResponse: DefaultDataResponse)-> Bool{
		
		guard dataResponse.error == nil else{
			
			return false
			
		}
		
		guard let dataResponse = dataResponse.data else{
			
			return false
			
		}
		
		let jsonResponse = try? JSONSerialization.jsonObject(with: dataResponse, options: .allowFragments) as! [String:Any]
		
		guard (jsonResponse) != nil else{
			
			return false
			
		}
		
		if (jsonResponse!["error"] as? Int) == 1 {
			
			return false
			
		}
		
		return true
	}
	
	
}
