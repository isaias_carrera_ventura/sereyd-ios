//
//  DetailGroupViewController.swift
//  SoyEducadoraSereyd
//
//  Created by Isaias Carrera Ventura on 24/05/17.
//  Copyright © 2017 FullStack. All rights reserved.
//

import UIKit
import SwiftValidator
import RealmSwift
import Alamofire
import NVActivityIndicatorView

class GroupCrudViewController: UIViewController, ValidationDelegate, UITextFieldDelegate, NVActivityIndicatorViewable {

	@IBOutlet weak var schoolTextField: UITextField!
	@IBOutlet weak var groupTextField: UITextField!
	@IBOutlet weak var gradeTextField: UITextField!
    @IBOutlet weak var scrollView: UIScrollView!
	@IBOutlet weak var periodTextField: UITextField!
	
	// ViewController.swift
	let validator = Validator()
	var user : UserModel!
	
	var bandEditGroup : Bool = false
	var editGroup: GroupModel?
	
    override func viewDidLoad() {
		
		super.viewDidLoad()
		
		user = UserController.getUserInDatabase()
		
		setView()
		
		if editGroup != nil {
		
			bandEditGroup = true
			setViewWithGroupToEdit(editGroup!)
			
		}
		
    }

	func setView(){
		
		CustomBar.setNavBarProfile((self.navigationController?.navigationBar)!, navItem: navigationItem,nav: self)
		
		schoolTextField.roundBorder()
		groupTextField.roundBorder()
		gradeTextField.roundBorder()
		periodTextField.roundBorder()
		
		//REGISTER VALIDATION
		validator.registerField(schoolTextField, rules: [RequiredRule(message: "Ingresa una escuela")])
		validator.registerField(groupTextField, rules: [RegexRule(regex: "^[a-zA-Z]$", message: "Una letra de A-Z")])
		validator.registerField(gradeTextField, rules: [RegexRule(regex: "[1-9]|10", message: "Un dígito del 1-10")])
		validator.registerField(periodTextField, rules: [RequiredRule(message: "Ingresa el periodo escolar")])
		
		/*NotificationCenter.default.addObserver(self, selector: #selector(GroupCrudViewController.keyboardWillShow), name: NSNotification.Name.UIKeyboardWillShow, object: nil)
		NotificationCenter.default.addObserver(self, selector: #selector(GroupCrudViewController.keyboardWillHide), name: NSNotification.Name.UIKeyboardWillHide, object: nil)*/
	
	}
	
	func setViewWithGroupToEdit(_ group: GroupModel){
		
		schoolTextField.text = group.value(forKey: "school") as? String
		groupTextField.text = group.value(forKey: "group") as? String
		gradeTextField.text = group.value(forKey: "grade") as? String
		periodTextField.text = group.value(forKey: "period") as? String
		
		
	}
	
    override func didReceiveMemoryWarning() {
        super.didReceiveMemoryWarning()
        // Dispose of any resources that can be recreated.
    }
	
	@IBAction func addGroup(_ sender: Any) {
		
		validator.validate(self)
		
	}
	
	
	func validationSuccessful() {
		
		
		if self.bandEditGroup {
			
			
			editGroupRequest()
			
			
		}else{
			
			createGroupRequest()
			
		}
		
		
	}
	
	func editGroupRequest() -> Void{
		
		let params = ["school":schoolTextField.text!,
		              "grade":gradeTextField.text!,
		              "group":groupTextField.text!,
		              "period":periodTextField.text!]
		
		let url = "\(Constants.GROUP_ENDPOINT)\(self.editGroup!.value(forKey: "idGroup")!)"
		
		//ANIMATE
		self.startAnimating(Constants.getSizeForLoadingView(view: self.view), type: NVActivityIndicatorType.ballBeat, color: ColorHelper.getSecondaryColor(1.0), padding: NVActivityIndicatorView.DEFAULT_PADDING)
		
		Alamofire.request(url, method: .put, parameters: params)
		.validate()
		.response { (dataResponse) in
			
			self.stopAnimating()
			
			if GroupController.updateGroupInServer(dataResponse) {
				
				//Save existent object
				self.bandEditGroup = false
				
				GroupController.editGroupInDatabase(groupEdit: self.editGroup!, school: self.schoolTextField.text!, grade: self.gradeTextField.text!, group: self.groupTextField.text!, period: self.periodTextField.text!, user: self.user)
				
                self.view.endEditing(true)
				_ = SweetAlert().showAlert("Grupo editado", subTitle: "¡Editaste un grupo correctamente!", style: AlertStyle.success, buttonTitle:"Ok", buttonColor: ColorHelper.getGreenColor(1.0)) { (button) -> Void in
				
				self.navigationController?.popViewController(animated: true)
				
				}
				
			}else{
                
                self.view.endEditing(true)
				_ = SweetAlert().showAlert("Ooops...", subTitle: "¡Algo salío mal, Inténtalo de nuevo!", style: AlertStyle.error)
				
			}
			
		}
		
		
	}
	
	func createGroupRequest() -> Void{
		let userId = UserController.getUserInDatabase()?.value(forKey: "idUser") as! String
		let school = schoolTextField.text!
		let grade = gradeTextField.text!
		let group = groupTextField.text!
		let period = periodTextField.text!
		
		let params = ["school":school,
		              "grade":grade,
		              "group":group,
		              "period":period,
		              "user":userId
		]
		
		//ANIMATE
		self.startAnimating(Constants.getSizeForLoadingView(view: self.view), type: NVActivityIndicatorType.ballBeat, color: ColorHelper.getSecondaryColor(1.0), padding: NVActivityIndicatorView.DEFAULT_PADDING)
		
		Alamofire.request(Constants.GROUP_ENDPOINT, method: .post, parameters: params)
			.validate()
			.response(completionHandler: { (dataResponse) in
				
				self.stopAnimating()
				
				if let group = GroupController.getGroupFromResponse(dataResponse){
					
					group.user = UserController.getUserInDatabase()!
					GroupController.saveGroupInDatabase(group: group)
                    
                    self.view.endEditing(true)
                    
					_ = SweetAlert().showAlert("Grupo agregado", subTitle: "¡Agregaste un grupo correctamente!", style: AlertStyle.success, buttonTitle:"Ok", buttonColor: ColorHelper.getGreenColor(1.0)) { (button) -> Void in
						
						self.navigationController?.popViewController(animated: true)
						
					}
					
				}else{
                    
                    self.view.endEditing(true)
					_ = SweetAlert().showAlert("Ooops...", subTitle: "¡Algo salío mal, Inténtalo de nuevo!", style: AlertStyle.error)
					
				}
			})

	}
	
	func validationFailed(_ errors:[(Validatable ,ValidationError)]) {
		var stringError = ""
		
		for (field, error) in errors {
			if let field = field as? UITextField {
				field.layer.borderColor = UIColor.red.cgColor
				field.layer.borderWidth = 1.0
			}
			
			stringError = stringError + "\n\(error.errorMessage)"
			error.errorLabel?.text = error.errorMessage // works if you added labels
			error.errorLabel?.isHidden = false
			
		}
        
        
        self.view.endEditing(true)
		
		_ = SweetAlert().showAlert("Error", subTitle: "¡Ingresa los datos requeridos! \(stringError)", style: AlertStyle.error, buttonTitle:"Ok", buttonColor:ColorHelper.getGreenColor(1.0))
		

	}
	
	//KEYBOARD METHODS
	override func touchesBegan(_ touches: Set<UITouch>, with event: UIEvent?) {
		
		self.view.endEditing(true)
		
	}
	
	func textFieldShouldReturn(_ textField: UITextField) -> Bool {
		
		textField.resignFirstResponder()
		return true
		
	}
	
    func textFieldDidEndEditing(_ textField: UITextField) {
        
        scrollView.setContentOffset(CGPoint(x: 0, y: 0), animated: true)
        
    }
    
    func textFieldDidBeginEditing(_ textField: UITextField) {
        scrollView.setContentOffset(CGPoint(x: 0, y: 250), animated: true)
                
    }
	
}
