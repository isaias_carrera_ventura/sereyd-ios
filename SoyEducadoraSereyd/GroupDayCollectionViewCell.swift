//
//  GroupDayCollectionViewCell.swift
//  SoyEducadoraSereyd
//
//  Created by Isaias Carrera Ventura on 21/07/17.
//  Copyright © 2017 FullStack. All rights reserved.
//

import UIKit

class GroupDayCollectionViewCell: UICollectionViewCell {
    
    @IBOutlet weak var nameGroup: UILabel!
    
}
