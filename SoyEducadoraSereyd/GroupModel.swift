//
//  GroupModel.swift
//  SoyEducadoraSereyd
//
//  Created by Isaias Carrera Ventura on 24/05/17.
//  Copyright © 2017 FullStack. All rights reserved.
//

import Foundation
import RealmSwift

class GroupModel:Object {
	
	var school: String?
	var grade: String?
	var group: String?
	var period: String?
	var idGroup: String?
    dynamic var percentage = 0.0
    
	dynamic var user: UserModel?
	
	let students = LinkingObjects(fromType: StudentModel.self, property: "groups")
	let cooperations = LinkingObjects(fromType: CooperationModel.self, property: "groups")
	let homeworks = LinkingObjects(fromType: HomeWorkModel.self, property: "groups")
    let diagnoses = LinkingObjects(fromType: DiagnoseModel.self, property: "groups")
    let evaluations = LinkingObjects(fromType: Evaluation.self, property: "groups")
	
}
