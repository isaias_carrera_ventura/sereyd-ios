//
//  GroupViewController.swift
//  SoyEducadoraSereyd
//
//  Created by Isaias Carrera Ventura on 23/05/17.
//  Copyright © 2017 FullStack. All rights reserved.
//

import UIKit
import RealmSwift
import NVActivityIndicatorView
import Alamofire
import UserNotifications

class GroupViewController: UIViewController, UITableViewDelegate, UITableViewDataSource,NVActivityIndicatorViewable  {
	 
	@IBOutlet weak var tableView: UITableView!
	
	var selectedGroupToEdit : GroupModel?
	var didSelectGroupToEdit : Bool! = false
	var items: [GroupModel]! = [GroupModel]()
	var groupDetailSelected : GroupModel?
	
    override func viewDidLoad() {
		
		super.viewDidLoad()
		setupView()
		
	}
	
	override func viewDidAppear(_ animated: Bool) {
		
		getGroupFromServer()
		
	}
	
	func getGroupFromServer() -> Void{
		 
		//GET IMAGE AND SAVE OBJECT
		self.startAnimating(Constants.getSizeForLoadingView(view: self.view), type: NVActivityIndicatorType.ballBeat, color: ColorHelper.getSecondaryColor(1.0), padding: NVActivityIndicatorView.DEFAULT_PADDING)

        //let url = "\(Constants.GROUP_BYUSER_ENDPOINT)\(UserController.getUserInDatabase()!.value(forKey: "idUser")!)"
        
		Alamofire.request("\(Constants.GROUP_BYUSER_ENDPOINT)\(UserController.getUserInDatabase()!.value(forKey: "idUser")!)", method: .get)
		.validate()
		.response { (dataDefaultResponse) in
			 
			self.stopAnimating()
			if let groupList = GroupController.getGroupListFromResponse(dataDefaultResponse) {
                
                if groupList.count > 0{
                    
                    GroupController.saveListUnrepeatedGroupInList(groupList,UserController.getUserInDatabase()!)
                    self.items = GroupController.getGroupInDatabase()
                    self.tableView.reloadData()
                    
                }else{
                    
                    _ = SweetAlert().showAlert("Sin grupos", subTitle: "Agrega un grupo y adminístralo", style: AlertStyle.warning)
                    
                }
				
            }else{
                
                 _ = SweetAlert().showAlert("¡Algo salió mal!", subTitle: "Inténtalo de nuevo más tarde", style: AlertStyle.error)
                
            }
			
		}
		
	}
	
	func setupView() -> Void{
		
		CustomBar.setNavBarProfile((self.navigationController?.navigationBar)!, navItem: navigationItem,nav: self)
		navigationItem.rightBarButtonItem = UIBarButtonItem(barButtonSystemItem: .add, target: self, action: #selector(GroupViewController.addGroup(_:)))
		items = GroupController.getGroupInDatabase()
	}
	
	func addGroup(_ button: UIBarButtonItem)
	{
		
		self.performSegue(withIdentifier: "groupCrudFormSegue", sender: self)
	
	}


    override func didReceiveMemoryWarning() {
        super.didReceiveMemoryWarning()
        // Dispose of any resources that can be recreated.
    }
	
	public func tableView(_ tableView: UITableView, numberOfRowsInSection section: Int) -> Int
	{
		
		return items.count
		
	}
	
	public func tableView(_ tableView: UITableView, cellForRowAt indexPath: IndexPath) -> UITableViewCell
	{
		
		let cell = tableView.dequeueReusableCell(withIdentifier: "Cell", for: indexPath) as! GroupTableViewCell
		
		let groupObject = self.items[indexPath.row]
        
        let percentage = round(groupObject.value(forKey: "percentage") as! Double)
        
        cell.labelPercentageAttendance.text = "\(percentage) %"
        cell.progressPercentageAttendance.progress = Float(groupObject.value(forKey: "percentage") as! Double)/100
		cell.nameGroupLabel.text = "\(String(describing: self.items[indexPath.row].value(forKey: "grade") as! String)) \(self.items[indexPath.row].value(forKey: "group") as! String) / \(self.items[indexPath.row].value(forKey: "school") as! String)"
		
		let account = StudentController.getAccountForStudentsInGroup(group: self.items[indexPath.row])
        
        //REMINDERS
        //FETCH REMINDERS
        let center = UNUserNotificationCenter.current()
        var notificationsArray = [UNNotificationRequest]()
        notificationsArray = [UNNotificationRequest]()
        let idGroupInDatabase = self.items[indexPath.row].value(forKey: "idGroup") as! String
         
        center.getPendingNotificationRequests { (notifications) in
            
            for item in notifications{
                
                let reminder = item.identifier.components(separatedBy: "_")
                let idGroup = reminder[0]
                
                if idGroup == idGroupInDatabase {
                    notificationsArray.append(item)
                }
                
            }
            
            DispatchQueue.main.async {
                
              
                if notificationsArray.count > 0 {
                    
                    let trigger = notificationsArray[indexPath.row].trigger as! UNCalendarNotificationTrigger
                    
                    let dateFormatter = DateFormatter()
                    dateFormatter.dateStyle = .short
                    dateFormatter.dateFormat = "dd-MM-YYYY HH:MM:SS"
                    dateFormatter.timeStyle = .medium
                    
                    var dateAux = trigger.dateComponents
                    dateAux.year = -16
                    cell.alertLabel.text = "\(dateFormatter.string(from: dateAux.date!))"
                    
                }
                
            }
            
        }
		
		cell.accountStudentsLabel.text = "\(account.total)"
		cell.manIndicatorLabel.text = "\(account.men)"
		cell.womanIndicatorLabel.text = "\(account.women)"
		
		cell.tapDeleteAction = { (cell) in
			
			_ = SweetAlert().showAlert("¿Eliminar este grupo?", subTitle: "", style: AlertStyle.warning, buttonTitle:"Cancelar", buttonColor:ColorHelper.getGreenColor(1.0), otherButtonTitle:  "Eliminar",otherButtonColor: ColorHelper.getRedColor(1.0)) { (isOtherButton) -> Void in
				
				if isOtherButton != true {
				
					self.startAnimating(Constants.getSizeForLoadingView(view: self.view), type: NVActivityIndicatorType.ballBeat, color: ColorHelper.getSecondaryColor(1.0), padding: NVActivityIndicatorView.DEFAULT_PADDING)
					
					let url = "\(Constants.GROUP_ENDPOINT)\(groupObject.value(forKey: "idGroup") as! String)"
                    
					Alamofire.request(url, method: .delete)
					.validate()
					.response(completionHandler: { (defaultDataResponse) in
						
						self.stopAnimating()
						
						if let bandGroupDeleted = GroupController.deleteGroupFromResponse(defaultDataResponse) {
							
							if bandGroupDeleted {
								
								GroupController.deleteGroupInDatabase(group: groupObject)
								self.items = GroupController.getGroupInDatabase()
								self.tableView.reloadData()
								_ = SweetAlert().showAlert("Grupo eliminado!", subTitle: "Has eliminado correctamente al grupo", style: AlertStyle.success)
								
							}else{
								
								_ = SweetAlert().showAlert("¡Algo salió mal!", subTitle: "Inténtalo de nuevo más tarde", style: AlertStyle.error)
								
							}
							
						}else{
							
							_ = SweetAlert().showAlert("¡Algo salió mal!", subTitle: "Inténtalo de nuevo más tarde", style: AlertStyle.error)
							
						}
						
						
					})
					
					
				}
			}
			
			
		}
		
		cell.tapEditAction = { (cell) in
		
			self.selectedGroupToEdit = self.items[indexPath.row]
			self.didSelectGroupToEdit = true
			self.performSegue(withIdentifier: "groupCrudFormSegue", sender: self)
			
		}
		
		
		return cell
		
	}
	
	
	func tableView(_ tableView: UITableView, didSelectRowAt indexPath: IndexPath) {
		
		groupDetailSelected = items[indexPath.row]
		self.performSegue(withIdentifier: "detailGroup", sender: self)
		
	}
	
	override func prepare(for segue: UIStoryboardSegue, sender: Any?) {
		
		//CONTROL BUTTON EDIT FOR PASSING OBJECT TO VIEW CONTROLLER
		if segue.identifier == "groupCrudFormSegue" && self.didSelectGroupToEdit == true {
			
			let crudViewController = segue.destination as! GroupCrudViewController
			crudViewController.editGroup = self.selectedGroupToEdit
			self.selectedGroupToEdit = nil
			self.didSelectGroupToEdit = false
			
		}else if segue.identifier == "detailGroup"{
	
			let crudViewController = segue.destination as! GroupCustomTabViewController
			crudViewController.group = self.groupDetailSelected

		}
	}
	
}
