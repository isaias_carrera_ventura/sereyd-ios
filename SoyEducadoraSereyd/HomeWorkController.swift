//
//  HomeWorkController.swift
//  SoyEducadoraSereyd
//
//  Created by Isaias Carrera Ventura on 02/06/17.
//  Copyright © 2017 FullStack. All rights reserved.
//

import Foundation
import RealmSwift
import Alamofire

class HomeWorkController{
	
	class func addHomeWorkToDatabase(_ homework: HomeWorkModel) -> Void{
		
		let realm = try! Realm()
		
		try! realm.write {
			
			realm.add(homework)
			
		}
		
	}
	
	class func deleteHomeWorkInDatabase(homework: HomeWorkModel) -> Void{
		
		let realm = try! Realm()
		
		// Delete an object with a transaction
		try! realm.write {
			realm.delete(homework)
		}
		 
	}
	
	class func editHomeWorkInDatabase(homework: HomeWorkModel,name: String,description: String,date: String) -> Void{
		
		let realm = try! Realm()
		
		// Delete an object with a transaction
		try! realm.write {
			
			homework.setValue(name, forKey: "name")
			homework.setValue(description, forKey: "descriptionHomeWork")
			homework.setValue(date, forKey: "date")
			
		}
		
	} 
	
	class func getHomeworksForGroup(_ group: GroupModel) -> [HomeWorkModel]?{
		
		
		var items: LinkingObjects<HomeWorkModel>!
		items = group.value(forKey: "homeworks") as? LinkingObjects<HomeWorkModel>
		
		var arrayHomework = [HomeWorkModel]()
		
		for homework in items{
			
			arrayHomework.append(homework)
			
		}
		
		return arrayHomework

		
	}
	
	class func getHomeworkListFromResponse(_ dataResponse: DefaultDataResponse) -> [HomeWorkModel]?{
		
		guard dataResponse.error == nil else{
			
			return nil
			
		}
		
		guard let dataResponse = dataResponse.data else{
			
			return nil
			
		}
		
		let jsonResponse = try? JSONSerialization.jsonObject(with: dataResponse, options: .allowFragments) as! [String:Any]
		
		guard (jsonResponse) != nil else{
			
			return nil
			
		}
		
		if (jsonResponse!["error"] as? Int) == 1 {
			
			return nil
			
		}
		
		guard let groupObject = jsonResponse?["data"] as? [String:Any] else{
			
			return nil
		}
		
		guard let homeworkArrayRaw = groupObject["homeworks"] as? NSArray else{
			
			
			return nil
			
		}
		
		
		var homeworks = [HomeWorkModel]()
		
		if homeworkArrayRaw.count == 0 {
			
			return homeworks
			
		}
		
		for homeworkRaw in homeworkArrayRaw{
			
			let homeworkObject = homeworkRaw as! [String:Any]
			let id_homework = homeworkObject["id_homework"] as! String
			let name = homeworkObject["name"] as! String
			let description = homeworkObject["description"] as! String
			let deadline = homeworkObject["deadline"] as! String
			
			let homework = HomeWorkModel()
			homework.date = deadline
			homework.descriptionHomeWork = description
			homework.name = name
			homework.idHomework = id_homework
			
			homeworks.append(homework)
			
		}
		
		return homeworks
		
	}
	
	class func saveListUnrepeatedHomeworkInList(_ homeworkArray : [HomeWorkModel],_ groupTo: GroupModel ) -> Void{
		
		let listHomeworkInDatabase : [HomeWorkModel]! = getHomeworksForGroup(groupTo)
		
		if  homeworkArray.count > listHomeworkInDatabase.count{
			
			//List from Server bigger than List from local database
			for homework in homeworkArray{
				
				var isHomeworkInDatabase = false
				 
				for homeworkInDb in listHomeworkInDatabase{
					
					if(homework.idHomework == homeworkInDb.value(forKey: "idHomework") as? String){
						isHomeworkInDatabase = true
					}
					
				}
				
				if !isHomeworkInDatabase{
					
					homework.groups.append(groupTo)
					addHomeWorkToDatabase(homework)
					
				}
				
			}
			
		}else if homeworkArray.count < listHomeworkInDatabase.count{
			
			
			for homework in listHomeworkInDatabase{
				
				var isHomeworkInServer = false
				
				for homeworkInServer in homeworkArray {
					
					if homework.value(forKey: "idHomework") as? String == homeworkInServer.idHomework{
						
						isHomeworkInServer = true
						
					}
					
				}
				
				if !isHomeworkInServer {
					
					deleteHomeWorkInDatabase(homework: homework)
					
				}
			}
		}
		
		
		saveChangesInDatabaseUpdate(getHomeworksForGroup(groupTo)!,homeworkArray)
		
	}
	
	class func saveChangesInDatabaseUpdate(_ homeworkInDB:[HomeWorkModel],_ homeInServer:[HomeWorkModel]) -> Void{
		
		for homework in homeworkInDB{
			
			if let homeAux = getHomeworkInServerById(homework.value(forKey: "idHomework") as! String,homeInServer){
				editHomeWorkInDatabase(homework: homework, name: homeAux.name!, description: homeAux.descriptionHomeWork!, date: homeAux.date!)
			}
		}
	}
	
	class func getHomeworkInServerById(_ idHomework: String,_ elements: [HomeWorkModel] ) -> HomeWorkModel?{
		
		for homework in elements{
			
			if homework.idHomework == idHomework{
				return homework
			}
			
		}
		
		return nil
		
	}
	 
	
	class func getHomeworkFromResponse(_ dataResponse: DefaultDataResponse) -> HomeWorkModel?{
		
	
		
		guard dataResponse.error == nil else{
			
			return nil
			
		}
		
		guard let dataResponse = dataResponse.data else{
			
			return nil
			
		}
		
		let jsonResponse = try? JSONSerialization.jsonObject(with: dataResponse, options: .allowFragments) as! [String:Any]
		
		guard (jsonResponse) != nil else{
			
			return nil
			
		}
		
		if (jsonResponse!["error"] as? Int) == 1 {
			
			return nil
			
		}
		
		guard let homeworkObject = jsonResponse?["homework"] as? [String:Any] else{
			
			return nil
		}
		
		
		let name = homeworkObject["name"] as! String
		let description = homeworkObject["description"] as! String
		let deadline = homeworkObject["deadline"] as! String
		let idHomework = homeworkObject["id_homework"] as! String
		
		let homeworkModel = HomeWorkModel()
		homeworkModel.date = deadline
		homeworkModel.idHomework = idHomework
		homeworkModel.name = name
		homeworkModel.descriptionHomeWork = description
		
		return homeworkModel
		
		
	}
	
	class func getResponseFromDeleteHomework(_ dataResponse: DefaultDataResponse) -> Bool{
		
		guard dataResponse.error == nil else{
			
			return false
			
		}
		
		guard let dataResponse = dataResponse.data else{
			
			return false
			
		}
		
		let jsonResponse = try? JSONSerialization.jsonObject(with: dataResponse, options: .allowFragments) as! [String:Any]
		
		guard (jsonResponse) != nil else{
			
			return false
			
		}
		
		if (jsonResponse!["error"] as? Int) == 1 {
			
			return false
			
		}
		
		return true
		
	}
	
	class func editResponseFromServer(_ dataResponse: DefaultDataResponse) -> Bool{
	
		guard dataResponse.error == nil else{
			
			return false
			
		}
		
		guard let dataResponse = dataResponse.data else{
			
			return false
			
		}
		
		let jsonResponse = try? JSONSerialization.jsonObject(with: dataResponse, options: .allowFragments) as! [String:Any]
		
		guard (jsonResponse) != nil else{
			
			return false
			
		}
		
		if (jsonResponse!["error"] as? Int) == 1 {
			
			return false
			
		}
		
		return true
		
	}
	
}
