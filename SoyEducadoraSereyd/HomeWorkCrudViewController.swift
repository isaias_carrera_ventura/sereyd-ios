//
//  HomeWorkCrudViewController.swift
//  SoyEducadoraSereyd
//
//  Created by Isaias Carrera Ventura on 02/06/17.
//  Copyright © 2017 FullStack. All rights reserved.
//

import UIKit
import SwiftValidator
import Alamofire
import NVActivityIndicatorView

class HomeWorkCrudViewController: UIViewController, ValidationDelegate, UITextFieldDelegate,NVActivityIndicatorViewable {
    
    
    @IBOutlet weak var scrollView: UIScrollView!
    @IBOutlet weak var nameTextField: UITextField!
    @IBOutlet weak var descriptionTextField: UITextField!
    @IBOutlet weak var dateTextField: UITextField!
    
    let validator = Validator()
    let datePicker = UIDatePicker()
    var group: GroupModel?
    var homework: HomeWorkModel?
    var bandEditHomework : Bool = false
    
    override func viewDidLoad() {
        
        super.viewDidLoad()
        setupView()
        
        if homework != nil {
            
            self.setupViewWithHomework(homework!)
            bandEditHomework = true
            
        }
        
    }
    
    func setupViewWithHomework(_ homework: HomeWorkModel) -> Void{
        
        nameTextField.text = homework.value(forKey: "name") as? String
        descriptionTextField.text = homework.value(forKey: "descriptionHomeWork") as? String
        dateTextField.text = homework.value(forKey: "date") as? String
        
    }
    
    func setupView() {
        
        CustomBar.setNavBarProfile((self.navigationController?.navigationBar)!, navItem: navigationItem,nav: self)
        nameTextField.roundBorder()
        descriptionTextField.roundBorder()
        dateTextField.roundBorder()
        
        createDatePicker()
        
        //VIEW IN EDITING MODE
        /*NotificationCenter.default.addObserver(self, selector: #selector(GroupCrudViewController.keyboardWillShow), name: NSNotification.Name.UIKeyboardWillShow, object: nil)
         NotificationCenter.default.addObserver(self, selector: #selector(GroupCrudViewController.keyboardWillHide), name: NSNotification.Name.UIKeyboardWillHide, object: nil)*/
        
        //REGISTER VALIDATION
        
        validator.registerField(nameTextField, rules: [RequiredRule(message: "Ingresa el nombre")])
        validator.registerField(descriptionTextField, rules: [RequiredRule(message: "Ingresa la descripción")])
        
        
    }
    
    
    func createDatePicker(){
        
        
        datePicker.datePickerMode = .date
        
        let minDate = Date()
        
        datePicker.minimumDate = minDate
        
        let toolbar  = UIToolbar()
        
        toolbar.sizeToFit()
        
        let doneButton = UIBarButtonItem(barButtonSystemItem: .done, target: nil, action: #selector(donePressedForDatePicker))
        toolbar.setItems([doneButton], animated: false)
        
        dateTextField.inputAccessoryView = toolbar
        
        dateTextField.inputView = datePicker
        
    }
    
    func donePressedForDatePicker(){
        
        let dateFormatter = DateFormatter()
        dateFormatter.dateFormat = "YYYY-MM-dd"
        
        dateTextField.text = "\(dateFormatter.string(from: datePicker.date))"
        self.view.endEditing(true)
        
    }
    
    func validationSuccessful() {
        
        
        if self.bandEditHomework == true{
            
            self.bandEditHomework = false
            self.requestUpdateHomework()
            
        }else{
            
            let homework = HomeWorkModel()
            homework.date = dateTextField.text!
            homework.descriptionHomeWork = descriptionTextField.text!
            homework.name = nameTextField.text!
            homework.groups.append(group!)
            
            self.requestAddHomework(homework)
            
        }
    }
    
    func requestUpdateHomework() -> Void{
        
        let params = ["name":nameTextField.text!,
                      "description":self.descriptionTextField.text!,
                      "deadline":self.dateTextField.text!]
        
        let url = "\(Constants.HOMEWORK_ENDPOINT)\(self.homework?.value(forKey: "idHomework") as! String)"
        
        self.startAnimating(Constants.getSizeForLoadingView(view: self.view), type: NVActivityIndicatorType.ballBeat, color: ColorHelper.getSecondaryColor(1.0), padding: NVActivityIndicatorView.DEFAULT_PADDING)
        
        Alamofire.request(url, method: .put, parameters: params)
            .validate()
            .response { (defaultDataResponse) in
                
                self.stopAnimating()
                
                if HomeWorkController.editResponseFromServer(defaultDataResponse) {
                    HomeWorkController.editHomeWorkInDatabase(homework: self.homework!, name: self.nameTextField.text!, description: self.descriptionTextField.text!, date: self.dateTextField.text!)
                    
                    self.view.endEditing(true)
                    _ = SweetAlert().showAlert("Tarea editada", subTitle: "¡Editaste una tarea correctamente!", style: AlertStyle.success, buttonTitle:"Ok", buttonColor: ColorHelper.getGreenColor(1.0)) { (button) -> Void in
                        
                        self.navigationController?.popViewController(animated: true)
                        
                    }
                    
                }else{
                    
                    self.view.endEditing(true)
                    _ = SweetAlert().showAlert("Error", subTitle: "¡Algo salió mal!", style: AlertStyle.error, buttonTitle:"Ok", buttonColor:ColorHelper.getGreenColor(1.0))
                    
                    
                }
                
        }
        
    }
    
    func requestAddHomework(_ homework: HomeWorkModel) -> Void{
        
        let params = [
            "name":homework.name!,
            "description":homework.descriptionHomeWork!,
            "deadline":homework.date!,
            "id_group":group!.value(forKey: "idGroup")!]
        
        
        self.startAnimating(Constants.getSizeForLoadingView(view: self.view), type: NVActivityIndicatorType.ballBeat, color: ColorHelper.getSecondaryColor(1.0), padding: NVActivityIndicatorView.DEFAULT_PADDING)
        
        Alamofire.request(Constants.HOMEWORK_ENDPOINT, method: .post, parameters: params)
            .validate()
            .response { (dataResponse) in
                
                self.stopAnimating()
                
                if let homeworkToSave = HomeWorkController.getHomeworkFromResponse(dataResponse){
                    
                    homeworkToSave.groups.append(self.group!)
                    HomeWorkController.addHomeWorkToDatabase(homeworkToSave)
                    
                    self.view.endEditing(true)
                    _ = SweetAlert().showAlert("Tarea agregada", subTitle: "¡Agregaste una tarea correctamente!", style: AlertStyle.success, buttonTitle:"Ok", buttonColor: ColorHelper.getGreenColor(1.0)) { (button) -> Void in
                        
                        self .navigationController?.popViewController(animated: true)
                        
                    }
                    
                }else{
                    
                    self.view.endEditing(true)
                    _ = SweetAlert().showAlert("Error", subTitle: "¡Algo salió mal!", style: AlertStyle.error, buttonTitle:"Ok", buttonColor:ColorHelper.getGreenColor(1.0))
                    
                    
                }
                
        }
    }
    
    func validationFailed(_ errors:[(Validatable ,ValidationError)]) {
        
        var stringError = ""
        
        for (field, error) in errors {
            if let field = field as? UITextField {
                field.layer.borderColor = UIColor.red.cgColor
                field.layer.borderWidth = 1.0
            }
            
            stringError = stringError + "\n\(error.errorMessage)"
            error.errorLabel?.text = error.errorMessage // works if you added labels
            error.errorLabel?.isHidden = false
            
        }
        
        self.view.endEditing(true)
        _ = SweetAlert().showAlert("Error", subTitle: "¡Ingresa los datos requeridos! \(stringError)", style: AlertStyle.error, buttonTitle:"Ok", buttonColor:ColorHelper.getGreenColor(1.0))
        
    }
    
    @IBAction func addHomeworkAction(_ sender: Any) {
        validator.validate(self)
    }
    
    //KEYBOARD METHODS
    override func touchesBegan(_ touches: Set<UITouch>, with event: UIEvent?) {
        
        self.view.endEditing(true)
        
    }
    
    func textFieldShouldReturn(_ textField: UITextField) -> Bool {
        
        textField.resignFirstResponder()
        return true
        
    }
    
    func textFieldDidEndEditing(_ textField: UITextField) {
        
        scrollView.setContentOffset(CGPoint(x: 0, y: 0), animated: true)
        
    }
    
    func textFieldDidBeginEditing(_ textField: UITextField) {
        
        
        scrollView.setContentOffset(CGPoint(x: 0, y: 250), animated: true)
        
    }
    
    
    /*func keyboardWillShow(notification: NSNotification) {
     
     //textfield for school is not always visible for user
     
     if !nameTextField.isEditing{
     
     if let keyboardSize = (notification.userInfo?[UIKeyboardFrameBeginUserInfoKey] as? NSValue)?.cgRectValue {
     if self.view.frame.origin.y == 0{
					self.view.frame.origin.y -= keyboardSize.height
     }
     }
     
     }
     
     }
     
     func keyboardWillHide(notification: NSNotification) {
     if let keyboardSize = (notification.userInfo?[UIKeyboardFrameBeginUserInfoKey] as? NSValue)?.cgRectValue {
     if self.view.frame.origin.y != 0{
     self.view.frame.origin.y += keyboardSize.height
     }
     }
     }
     */
    
    
}
