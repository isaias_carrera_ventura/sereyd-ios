//
//  ViewController.swift
//  SoyEducadoraSereyd
//
//  Created by Isaias Carrera Ventura on 18/05/17.
//  Copyright © 2017 FullStack. All rights reserved.
//

import UIKit
import BWWalkthrough
import NVActivityIndicatorView
import AlamofireImage
import Alamofire

class InitViewController: UIViewController, BWWalkthroughViewControllerDelegate, NVActivityIndicatorViewable{
	
    @IBOutlet weak var labelTerms: UILabel!
	
    override func viewDidLoad() {
		
		super.viewDidLoad()
        let gestureRecognizer = UITapGestureRecognizer(target: self, action:#selector(InitViewController.handleTap))
        labelTerms.isUserInteractionEnabled = true
        labelTerms.addGestureRecognizer(gestureRecognizer)
		
	}
    
    func handleTap(gestureRecognizer: UIGestureRecognizer) {
        openURL(Constants.TERMS_URL)
    }
    
    func openURL(_ urlString: String){
        let url = URL(string: urlString)!
        if #available(iOS 10.0, *) {
            UIApplication.shared.open(url, options: [:], completionHandler: nil)
        } else {
            UIApplication.shared.openURL(url)
        }
    }

	
	@IBAction func loginFacebook(_ sender: Any) {
		
		let fbLoginManager : FBSDKLoginManager = FBSDKLoginManager()
		fbLoginManager.logIn(withReadPermissions: ["email"], from: self) { (result, error) in
			
			if (error == nil){
				
				let fbloginresult : FBSDKLoginManagerLoginResult = result!
				
				if (fbloginresult.grantedPermissions) != nil{
					if(fbloginresult.grantedPermissions.contains("email"))
					{
						self.getFBUserData()
						fbLoginManager.logOut()
					}
				}else{
					
					_ = SweetAlert().showAlert("Error", subTitle: "¡Cancelaste el inicio de sesión!", style: AlertStyle.error, buttonTitle:"Ok", buttonColor:ColorHelper.getGreenColor(1.0))
					
				}
				
				
			}
		}
		
	}
	
	func getFBUserData(){
		if((FBSDKAccessToken.current()) != nil){
			FBSDKGraphRequest(graphPath: "me", parameters: ["fields": "id, name, first_name, last_name, picture.type(large), email"]).start(completionHandler: { (connection, result, error) -> Void in
				if (error == nil){
					
					if let dictionaryUserData = result as? NSDictionary{
						
						self.loginWithFacebook(dictionaryUserData)
						
						
					}
					
				}
			})
		}
	}
	
	
	func loginWithFacebook(_ dictionaryUserData: NSDictionary) -> Void{
		
		let user = UserController.getJsonFromFacebookResponse(dictionaryUserData)
		
		//GET IMAGE AND SAVE OBJECT
		self.startAnimating(Constants.getSizeForLoadingView(view: self.view), type: NVActivityIndicatorType.ballBeat, color: ColorHelper.getSecondaryColor(1.0), padding: NVActivityIndicatorView.DEFAULT_PADDING)
		
		let params = ["id_facebook":user.idUserFacebook!]
        
		Alamofire.request(Constants.LOGIN_FACEBOOK_ENDPOINT, method: .post, parameters: params)
			.validate()
			.responseString(completionHandler: { (dataResponse) in
				
				self.stopAnimating()
				
				guard dataResponse.error == nil else{
					
					self.userRegisterAndSign(user)
					return
					
				}
				
				guard let dataResponseSign = dataResponse.data else{
					
					return
					
				}
				
				let jsonResponseUser = try? JSONSerialization.jsonObject(with: dataResponseSign, options: .allowFragments)
				
				guard (jsonResponseUser as? [String:Any]) != nil else{
					
					return
					
				}
				
				let user = UserController.getJsonFromResponse(jsonResponseUser as! NSDictionary)
				self.downloadForUserContent(user)
				
				
			})
		
	}
		
    
    func downloadForUserContent(_ user: UserModel)-> Void{
        
        UserController.saveUserInDatabase(user: user)
        
        self.startAnimating(Constants.getSizeForLoadingView(view: self.view), type: NVActivityIndicatorType.ballBeat, color: ColorHelper.getSecondaryColor(1.0), padding: NVActivityIndicatorView.DEFAULT_PADDING)
        
        Alamofire.request("\(Constants.GROUP_BYUSER_ENDPOINT)\(UserController.getUserInDatabase()!.value(forKey: "idUser")!)", method: .get)
            .validate()
            .response { (dataDefaultResponse) in
                
                self.stopAnimating()
                
                if let groupList = GroupController.getGroupListFromResponse(dataDefaultResponse) {
                    
                    GroupController.saveListUnrepeatedGroupInList(groupList,UserController.getUserInDatabase()!)
                    let groups = GroupController.getGroupInDatabase()
                    for group in groups{
                        
                        if let studentList = StudentController.getListStudentForGroup(dataDefaultResponse,group){
                            StudentController.saveListUnrepeatedStudentInList(studentList, group)
                        }
                        
                    }
                    
                    
                    self.dismiss(animated: true, completion: nil)
                    self.performSegue(withIdentifier: "mainMenuSegue", sender: self)
                    
                    
                    
                }
                
        }
        
        
    }
    
	func userRegisterAndSign(_ user: UserModel) -> Void{
		
		let params = ["name":user.name,
		              "email":user.email ?? "",
		              "id_facebook":user.idUserFacebook!,
		              "image":user.imageUrl] as [String:Any]
		
        //GET IMAGE AND SAVE OBJECT
		self.startAnimating(Constants.getSizeForLoadingView(view: self.view), type: NVActivityIndicatorType.ballBeat, color: ColorHelper.getSecondaryColor(1.0), padding: NVActivityIndicatorView.DEFAULT_PADDING)
		
		Alamofire.request(Constants.USER_ENDPOINT, method: .post, parameters: params)
			.validate()
			.responseString { (dataResponse) in
				
				self.stopAnimating()
				guard dataResponse.error == nil else{
					
					let stringError = AlamofireHandlerError.getErrorMessage(dataResponse)
					_ = SweetAlert().showAlert("Error", subTitle: "\(stringError)", style: AlertStyle.error, buttonTitle:"Ok", buttonColor:ColorHelper.getGreenColor(1.0))
					
					return
					
				}
				
				guard let dataResponseSign = dataResponse.data else{
					
					return
					
				}
				
				let jsonResponseUser = try? JSONSerialization.jsonObject(with: dataResponseSign, options: .allowFragments)
				
				guard (jsonResponseUser as? [String:Any]) != nil else{
					
					return
					
				}
				
				
				let user = UserController.getJsonFromResponse(jsonResponseUser as! NSDictionary)
				
				_ = SweetAlert().showAlert("Inicio de sesión exitoso", subTitle: "¡Ya puedes iniciar sesión!", style: AlertStyle.success, buttonTitle:"Ok", buttonColor: ColorHelper.getGreenColor(1.0)) { (button) -> Void in
					
					UserController.saveUserInDatabase(user: user)
					self.dismiss(animated: true, completion: nil)
					self.performSegue(withIdentifier: "mainMenuSegue", sender: self)
					
					
				}
				
				
		}
		
	}
	
	
	override func viewDidAppear(_ animated: Bool) {
		
		
		let userDefaults = UserDefaults.standard
		
		if !userDefaults.bool(forKey: "walkthroughPresented") {
			
			showWalkthrough()
			userDefaults.set(true, forKey: "walkthroughPresented")
			userDefaults.synchronize()
			
		}
		
		if UserController.getUserInDatabase() != nil {
			
			self.performSegue(withIdentifier: "mainMenuSegue", sender: self)
			
		}
		
		
		
	}
	
	override func didReceiveMemoryWarning() {
		super.didReceiveMemoryWarning()
		// Dispose of any resources that can be recreated.
	}
	
	@IBAction func signupButtonClicked(_ sender: Any) {
		
		self.performSegue(withIdentifier: "signUp", sender: self)
		
	}
	
	@IBAction func loginButtonClicked(_ sender: Any) {
		
		self.performSegue(withIdentifier: "loginSereydAccount", sender: self)
		
	}
	
	func showWalkthrough(){
		
		// Get view controllers and build the walkthrough
		let stb = UIStoryboard(name: "Walkthrough", bundle: nil)
		let walkthrough = stb.instantiateViewController(withIdentifier: "walk") as! BWWalkthroughViewController
		let page_zero = stb.instantiateViewController(withIdentifier: "walk0")
		let page_one = stb.instantiateViewController(withIdentifier: "walk1")
		let page_two = stb.instantiateViewController(withIdentifier: "walk2")
		let page_three = stb.instantiateViewController(withIdentifier: "walk3")
		
		// Attach the pages to the master
		walkthrough.delegate = self
		walkthrough.add(viewController:page_zero)
		walkthrough.add(viewController:page_one)
		walkthrough.add(viewController:page_two)
		walkthrough.add(viewController:page_three)
		
		self.present(walkthrough, animated: true, completion: nil)
	}
	
	func walkthroughCloseButtonPressed() {
		self.dismiss(animated: true, completion: nil)
	}
	
    @IBAction func unwindToVC1(segue:UIStoryboardSegue) { }
    //@IBAction override func unwind(for unwindSegue: UIStoryboardSegue, towardsViewController subsequentVC: UIViewController) {}
	
	
}

