//
//  MainMenuViewController.swift
//  SoyEducadoraSereyd
//
//  Created by Isaias Carrera Ventura on 20/05/17.
//  Copyright © 2017 FullStack. All rights reserved.
//

import UIKit
import Kingfisher
import Alamofire
import NVActivityIndicatorView

class MainMenuViewController: UIViewController, NVActivityIndicatorViewable {

	@IBOutlet weak var imageProfileUser: UIImageView!
	@IBOutlet weak var nameLabel: UILabel!
	
    override func viewDidLoad() {
		
		super.viewDidLoad()
		CustomBar.setNavBarProfile((self.navigationController?.navigationBar)!, navItem: navigationItem,nav: self)		   
    }
    
    override func viewDidAppear(_ animated: Bool) {
        
        
        if let user = UserController.getUserInDatabase(){
            
            nameLabel.text = user.name
            let urlImage = user.value(forKey: "imageUrl") as! String
            let url = URL(string: urlImage)
            KingfisherManager.shared.retrieveImage(with: url!, options: nil, progressBlock: nil, completionHandler: { image, error, cacheType, imageURL in
                
                if error == nil{
                
                    self.imageProfileUser.image = ImageUtils.resizeImage(image: image!, newWidth: 90.0)
                    ImageUtils.circularImage(self.imageProfileUser, color: ColorHelper.getThirdColor(1.0).cgColor)
                    
                }
                
            })
        }

        
    }

    override func didReceiveMemoryWarning() {
        super.didReceiveMemoryWarning()
        // Dispose of any resources that can be recreated.
    }
    
	@IBAction func openShopWebView(_ sender: Any) {
	
		self.performSegue(withIdentifier: "shopSereydView", sender: self)

		
	}
    
    @IBAction func openPlanningMenu(_ sender: Any) {
     
        self.performSegue(withIdentifier: "showPlanningViewController", sender: self)
    
    }

	@IBAction func openGroupMain(_ sender: Any) {
	
        self.performSegue(withIdentifier: "groupSegue", sender: self)
	
    }
    
    @IBAction func settingsView(_ sender: Any) {
        self.performSegue(withIdentifier: "settingsView",sender: self)
    }
    
    @IBAction func forumView(_ sender: Any){
        self.performSegue(withIdentifier: "forumListSegue",sender: self)
    }
     
    
    var itemsToSegue = [ResourceContainerModel]()
    
    @IBAction func openResources(_ sender: Any) {
        
        //GET IMAGE AND SAVE OBJECT
        self.startAnimating(Constants.getSizeForLoadingView(view: self.view), type: NVActivityIndicatorType.ballBeat, color: ColorHelper.getSecondaryColor(1.0), padding: NVActivityIndicatorView.DEFAULT_PADDING)
        
        Alamofire.request(Constants.RESOURCE_ENDPOINT, method: .get)
            .validate()
            .response { (dataResponse) in
                
                self.stopAnimating()
                
                if let resourceListFromServer = ResourceController.getResourceListContainerFromResponse(dataResponse){
                    
                    self.itemsToSegue = resourceListFromServer
                    self.performSegue(withIdentifier: "resourcesSegue", sender: self)
                    
                }else{
                    
                    self.performSegue(withIdentifier: "myResourceDatabaseDetailSegue", sender: self)
                }
                
        }
        
    }
    
    @IBAction func editProfile(_ sender: Any) {
        
        self.performSegue(withIdentifier: "openEditProfile", sender: self)
    }
    
    @IBAction func startDay(_ sender: Any) {
    
        
        if GroupController.getGroupInDatabase().count > 0 {
            
            self.performSegue(withIdentifier: "startDaySegue",sender: self)
            
        }else{
            
            _ = SweetAlert().showAlert("Oops!", subTitle: "No tienes grupos", style: AlertStyle.warning)
            
        }
    
    }
    
    override func prepare(for segue: UIStoryboardSegue, sender: Any?) {
        
        if segue.identifier == "resourcesSegue"{
        
            let viewController = segue.destination as! ResourcesListViewController
            viewController.items = self.itemsToSegue
        
        }
        
    }

}
