//
//  PaymentInformation.swift
//  SoyEducadoraSereyd
//
//  Created by Isaias Carrera Ventura on 01/01/18.
//  Copyright © 2018 FullStack. All rights reserved.
//

import Foundation

class PaymentInformation {
    
    /*
     private String description, initDate, finishDate;
     private int code;
     
     private String cardNumber, holderName, chargeDate, descriptionCard;
     private Double amount;
     */
    
    var description,initDate,finishDate: String?
    var code: Int = 0
    
    var cardNumber,holderName,chargeDate,descriptionCard: String?
    var amount: Double = 0.0
    var cancelled: Int = 0
    
    init(code: Int) {
        
        self.code = code
        
        self.description = nil
        self.initDate = nil
        self.finishDate = nil
        
        self.cardNumber = nil
        self.holderName = nil
        self.chargeDate = nil
        self.descriptionCard = nil
        
    }
    
}
