//
//  PaymentObjectSereyd.swift
//  SoyEducadoraSereyd
//
//  Created by Isaias Carrera Ventura on 27/09/17.
//  Copyright © 2017 FullStack. All rights reserved.
//

import Foundation
class PaymentObjectSereyd {
    
    var error:Int!
    var codeError:Int!
    var description: String!
    var annual: Bool?
    var resourceCount: Int!
    var planningCount: Int!
    
    init(error: Int,codeError: Int,description: String, annual: Bool?,resourceCount: Int,planningCount: Int) {
        
        self.error = error
        self.codeError = codeError
        self.description = description
        self.annual = annual
        self.resourceCount = resourceCount
        self.planningCount = planningCount
        
    }
    
}
