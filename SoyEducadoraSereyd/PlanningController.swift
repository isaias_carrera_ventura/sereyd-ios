//
//  PlanningController.swift
//  SoyEducadoraSereyd
//
//  Created by Isaias Carrera Ventura on 13/07/17.
//  Copyright © 2017 FullStack. All rights reserved.
//

import Foundation
import RealmSwift
import Alamofire

class PlanningController {
    
    class func saveListToDatabase(_ planningListServer : [Planning]){
        
        let planningInDatabase = getPlanningInDatabase()
        
        for planningServer in planningListServer{
            
            var isPlanningInDatabase = false
            
            for planningDB in planningInDatabase {
                
                if planningServer.idPlanning == (planningDB.value(forKey: "idPlanning") as! String){
                    isPlanningInDatabase = true
                }
                
            }
            
            if !isPlanningInDatabase {
            
                savePlanningInDatabase(planning: planningServer)
                
            }
        }
        
    }
    
    class func savePlanningInDatabase(planning: Planning) -> Void{
        
        let realm = try! Realm()
        
        try! realm.write {
            
            realm.add(planning)
            
        }
        
    }
    
    class func isPlanningInDatabase(_ planning: Planning) -> Bool{
        
        let planningInDatabase = getPlanningInDatabase()
        
        for planningInDB in planningInDatabase{
            
            if planning.value(forKey: "idPlanning") as! String == planningInDB.value(forKey: "idPlanning") as! String {
                return true
            }
        }
        
        return false
        
    }
    
    class func getPlanningInDatabase() -> [Planning]{
        
        let realm = try! Realm()
        let planning = realm.objects(Planning.self)
        return Array(planning)
        
        
    }
    
    class func getListPlanningFromResponse(_ dataResponse: DefaultDataResponse) -> [Planning]? {
        
        guard dataResponse.error == nil else{
            return nil
            
        }
        
        guard let dataResponse = dataResponse.data else{
            
            return nil
            
        }
        
        let jsonResponse = try? JSONSerialization.jsonObject(with: dataResponse, options: .allowFragments) as! [String:Any]
        
        guard (jsonResponse) != nil else{
            
            return nil
            
        }
        
        if (jsonResponse!["error"] as? Int) == 1 {
            
            return nil
            
        }
        
        guard let planningArrayRaw = jsonResponse?["planifications"] as? NSArray else{
            
            return nil
            
        }
        
        var planningArray = [Planning]()
        
        for planning in planningArrayRaw {
            
            let objectPlanning = planning as! [String:Any]
            let planningToAdd = Planning()
            planningToAdd.idPlanning = objectPlanning["id_planification"] as? String
            planningToAdd.descriptionPlanning = objectPlanning["description"] as? String
            planningToAdd.name = objectPlanning["name"] as? String
            planningToAdd.field = objectPlanning["formative_field"] as? String
            planningToAdd.premium = Int((objectPlanning["premium"] as? NSString)!.intValue)
            planningToAdd.imagePlanning = "\(Constants.URL_SERVER)\(String(describing: objectPlanning["image"] as! String))"
            planningToAdd.urlFile = "\(Constants.URL_SERVER)\(String(describing: objectPlanning["url"] as! String))"
            planningToAdd.urlWord = "\(Constants.URL_SERVER)\(String(describing: objectPlanning["url_word"] as! String))"
            planningToAdd.preview = "\(Constants.URL_SERVER)\(String(describing: objectPlanning["preview"] as! String))"
            planningArray.append(planningToAdd)
        }
        
        return planningArray
    }
    
    class func getNumberPlanningAllowed(_ dataResponse: DefaultDataResponse) -> Int{
        
        guard dataResponse.error == nil else{
            
            return 0
            
        }
        
        guard let dataResponse = dataResponse.data else{
            
            return 0
            
        }
        
        let jsonResponse = try? JSONSerialization.jsonObject(with: dataResponse, options: .allowFragments) as! [String:Any]
        
        guard (jsonResponse) != nil else{
            
            return 0
            
        }
        
        if (jsonResponse!["error"] as? Int) == 1 {
            
            return 0
            
        }

        let count = jsonResponse!["planification_count"] as! Int
        return count
    }
    
    class func getStatePaymentRequest(_ dataResponse: DefaultDataResponse) -> Bool{
        
        guard dataResponse.error == nil else{
            
            return false
            
        }
        
        guard let dataResponse = dataResponse.data else{
            
            return false
            
        }
        
        let jsonResponse = try? JSONSerialization.jsonObject(with: dataResponse, options: .allowFragments) as! [String:Any]
        
        guard (jsonResponse) != nil else{
            
            return false
            
        }
        
        if (jsonResponse!["error"] as? Int) == 1 {
            
            return false
            
        }

        return true
        
    }
    
    class func asignPlanningToDatabase(_ dataResponse: DefaultDataResponse, _ planning: Planning) -> Bool{
    
        guard dataResponse.error == nil else{
            
            return false
            
        }
        
        guard let dataResponse = dataResponse.data else{
            
            return false
            
        }
        
        let jsonResponse = try? JSONSerialization.jsonObject(with: dataResponse, options: .allowFragments) as! [String:Any]
        
        guard (jsonResponse) != nil else{
            
            return false
            
        }
        
        if (jsonResponse!["error"] as? Int) == 1 {
            
            return false
            
        }
        
        savePlanningInDatabase(planning: planning)
        
        return true

        
    }
    
    class func isPlanningFileInDisk(_ planning: Planning) -> Bool{
    
        do{
        
            //Getting a list of the docs directory
            let docURL = (FileManager.default.urls(for: .documentDirectory, in: .userDomainMask).last) as NSURL?
            let contents = try (FileManager.default.contentsOfDirectory(at: docURL! as URL, includingPropertiesForKeys: nil, options: FileManager.DirectoryEnumerationOptions.skipsHiddenFiles))
            
            for content in contents{
                
                let stringFileName = content.absoluteString
                let namePlanning = getFileNameForPlanning(planning)
                if stringFileName.contains(namePlanning){
                    return true
                }
                
            }
            
        }catch{
            return false
        }
        
        return false
        
    }
    
    class func isPlanningFileWordInDisk(_ planning: Planning) -> Bool{
        
        do{
            
            //Getting a list of the docs directory
            let docURL = (FileManager.default.urls(for: .documentDirectory, in: .userDomainMask).last) as NSURL?
            let contents = try (FileManager.default.contentsOfDirectory(at: docURL! as URL, includingPropertiesForKeys: nil, options: FileManager.DirectoryEnumerationOptions.skipsHiddenFiles))
            
            for content in contents{
                
                let stringFileName = content.absoluteString
                let namePlanning = getFileNameForPlanningWord(planning)
                if stringFileName.contains(namePlanning){
                    return true
                }
                
            }
            
        }catch{
            return false
        }
        
        return false
        
    }
    
    class func getFileNameForPlanning(_ planning: Planning) -> String{
        let nameAcc = "\(planning.value(forKey: "idPlanning") as! String)_\((planning.value(forKey: "name") as! String).replacingOccurrences(of: " ", with: "")).pdf"
        return nameAcc.folding(options: .diacriticInsensitive, locale: .current)
    }
    
    class func getFileNameForPlanningWord(_ planning: Planning) -> String{
        let nameAcc = "\(planning.value(forKey: "idPlanning") as! String)_\((planning.value(forKey: "name") as! String).replacingOccurrences(of: " ", with: "")).docx"
        return nameAcc.folding(options: .diacriticInsensitive, locale: .current)
    }
    
    class func getFileForPlanning(_ planning: Planning) -> URL?{
        
        do{
            let docURL = (FileManager.default.urls(for: .documentDirectory, in: .userDomainMask).last) as NSURL?
            let contents = try (FileManager.default.contentsOfDirectory(at: docURL! as URL, includingPropertiesForKeys: nil, options: FileManager.DirectoryEnumerationOptions.skipsHiddenFiles))
            
            for content in contents{
                
                let stringFileName = content.absoluteString
                let namePlanning = getFileNameForPlanning(planning)
                if stringFileName.contains(namePlanning){
                    return content
                }
                
            }
            
            return nil
 
        }catch{
            return nil
        }
        
    }
    
    class func getFileWordForPlanning(_ planning: Planning) -> URL?{
        
        do{
            let docURL = (FileManager.default.urls(for: .documentDirectory, in: .userDomainMask).last) as NSURL?
            let contents = try (FileManager.default.contentsOfDirectory(at: docURL! as URL, includingPropertiesForKeys: nil, options: FileManager.DirectoryEnumerationOptions.skipsHiddenFiles))
            
            for content in contents{
                
                let stringFileName = content.absoluteString
                let namePlanning = getFileNameForPlanningWord(planning)
                if stringFileName.contains(namePlanning){
                    return content
                }
                
            }
            
            return nil
            
        }catch{
            return nil
        }
        
    }
    
    class func removeFileForPlanning() -> Void{
        
        let fileManager = FileManager.default
        let plannings = getPlanningInDatabase()
        
        for planning in plannings{
            
            if isPlanningFileInDisk(planning){
                
                let pathFile = getFileForPlanning(planning)
                let pathFileWord = getFileWordForPlanning(planning)
                do{
                    
                    try fileManager.removeItem(at: pathFile!)
                    try fileManager.removeItem(at: pathFileWord!)
                    
                }catch _ as NSError {
                    
                    
                     
                }
                
                
            }
            
        }
        
        
    }
    class func getPlanningRequestMistake(_ dataResponse:DefaultDataResponse) ->Int{
        
        
        guard let dataResponse = dataResponse.data else{
            
            return 0
            
        }
        
        let jsonResponse = try? JSONSerialization.jsonObject(with: dataResponse, options: .allowFragments) as! [String:Any]
        
        guard (jsonResponse) != nil else{
            
            return 0
            
        }
        
        let count = jsonResponse!["error"] as! Int
        return count
        
        
    }
    class func getPlanningMissing(_ listPlanningFromServer : inout [Planning]) -> [Planning] {
        
        var list = [Planning]()
        
        let listInDatabase = getPlanningInDatabase()
        
        for planning in listInDatabase{
            
            for (i,planningInServer) in listPlanningFromServer.enumerated(){
                
                if planningInServer.value(forKey: "idPlanning") as! String == planning.value(forKey: "idPlanning") as! String{
                    
                    listPlanningFromServer.remove(at: i)
                    
                }
                
            }
            
        }
        
        list.append(contentsOf: listInDatabase)
        list.append(contentsOf: listPlanningFromServer)
        
        return list
        
    }
    
}
