//
//  PlanningMenuViewController.swift
//  SoyEducadoraSereyd
//
//  Created by Isaias Carrera Ventura on 06/07/17.
//  Copyright © 2017 FullStack. All rights reserved.
//

import UIKit
import CarbonKit

class PlanningMenuViewController: UIViewController, CarbonTabSwipeNavigationDelegate {
    
    override func viewDidLoad() {
        super.viewDidLoad()
        
        CustomBar.setNavBarProfile((self.navigationController?.navigationBar)!, navItem: navigationItem,nav: self)
        let items = ["Planeaciones","Diagnóstico","Evaluación"]
        let carbonTabSwipeNavigation = CarbonTabSwipeNavigation(items: items, delegate: self)
        carbonTabSwipeNavigation.setNormalColor(ColorHelper.getWhiteColor(1.0))
        carbonTabSwipeNavigation.setSelectedColor(ColorHelper.getSecondaryColor(1.0))
        carbonTabSwipeNavigation.setIndicatorColor(ColorHelper.getSecondaryColor(1.0))
        carbonTabSwipeNavigation.carbonSegmentedControl?.backgroundColor = ColorHelper.getPrimaryColor(1.0)
        carbonTabSwipeNavigation.carbonTabSwipeScrollView.backgroundColor = ColorHelper.getPrimaryColor(1.0)
        carbonTabSwipeNavigation.insert(intoRootViewController: self)
        
        let width = self.view.bounds.width/3
        carbonTabSwipeNavigation.carbonSegmentedControl?.setWidth(width, forSegmentAt: 0)
        carbonTabSwipeNavigation.carbonSegmentedControl?.setWidth(width, forSegmentAt: 1)
        carbonTabSwipeNavigation.carbonSegmentedControl?.setWidth(width, forSegmentAt: 2)
        
    }
    
    func carbonTabSwipeNavigation(_ carbonTabSwipeNavigation: CarbonTabSwipeNavigation, viewControllerAt index: UInt) -> UIViewController {
        
        switch index {
        case 0:
            
            let viewControllerGroupDetail = self.storyboard?.instantiateViewController(withIdentifier: "planningViewController") as? PlanningViewController
            return viewControllerGroupDetail!
            
        case 1:
            
            let viewControllerCooperationDetail = self.storyboard?.instantiateViewController(withIdentifier: "diagnoseViewController") as? DiagnoseViewController
            return viewControllerCooperationDetail!
            
        case 2:
            let viewControllerCooperationDetail = self.storyboard?.instantiateViewController(withIdentifier: "evaluationViewController") as? EvaluationViewController
            return viewControllerCooperationDetail!
            
        default:
            
            let viewControllerCooperationDetail = self.storyboard?.instantiateViewController(withIdentifier: "planningViewController") as? PlanningViewController
            return viewControllerCooperationDetail!
            
        }
        
    }
    
}
