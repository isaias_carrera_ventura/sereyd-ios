//
//  PlanningViewController.swift
//  SoyEducadoraSereyd
//
//  Created by Isaias Carrera Ventura on 06/07/17.
//  Copyright © 2017 FullStack. All rights reserved.
//

import UIKit
import Alamofire
import NVActivityIndicatorView
import Kingfisher

class PlanningViewController: UIViewController,UICollectionViewDelegate,UICollectionViewDataSource,UICollectionViewDelegateFlowLayout,NVActivityIndicatorViewable {
    
    @IBOutlet weak var collectionViewPlanning: UICollectionView!
    
    fileprivate let reuseIdentifier = "CellPlanning"
    var planningList: [Planning] = [Planning]()
    var planningFiltered : [Planning] = [Planning]()
    
    var searchActive : Bool = false
    let searchController = UISearchController(searchResultsController: nil)

    override func viewDidLoad() {
        
        // Setup the Search Controller
        searchController.searchResultsUpdater = self
        searchController.dimsBackgroundDuringPresentation = false
        _ = searchController.hidesNavigationBarDuringPresentation = false
        _ = searchController.searchBar.placeholder = "Buscar"
        _ = searchController.searchBar.barTintColor = ColorHelper.getPrimaryColor(1.0)
        searchController.searchBar.becomeFirstResponder()
        definesPresentationContext = true
        self.view.addSubview(searchController.searchBar)
        
    }
    
    func filterContentForSearchText(searchText: String, scope: String = "All") {
        
        planningFiltered = planningList.filter { planning in
            
            return ((planning.value(forKey: "name") as! String).lowercased().contains(searchText.lowercased())) || (planning.value(forKey: "field") as! String).lowercased().contains(searchText.lowercased())
        }
        
        collectionViewPlanning.reloadData()
    }
    
    override func viewDidAppear(_ animated: Bool) {
        
        
        planningList = PlanningController.getPlanningInDatabase()
        collectionViewPlanning.reloadData()
        requestForUserPlanning()
        
    }
    
    
    func requestForUserPlanning(){
        
        self.startAnimating(Constants.getSizeForLoadingView(view: self.view), type: NVActivityIndicatorType.ballBeat, color: ColorHelper.getSecondaryColor(1.0), padding: NVActivityIndicatorView.DEFAULT_PADDING)
        
        let url = "\(Constants.PLANIFICATION_USER_ENDPOINT)\(UserController.getUserInDatabase()?.value(forKey: "idUser") as! String)"
        Alamofire.request(url, method: .get)
            .validate()
            .response { (dataResponse) in
                
                self.stopAnimating()
                
                if let planningListResponse = PlanningController.getListPlanningFromResponse(dataResponse){
                    
                    PlanningController.saveListToDatabase(planningListResponse)
                    self.planningList = PlanningController.getPlanningInDatabase()
                    self.collectionViewPlanning.reloadData()
                    
                }
                
                self.requestForPlanning()
                
        }
        
    }
    
    func requestForPlanning(){
        
        self.startAnimating(Constants.getSizeForLoadingView(view: self.view), type: NVActivityIndicatorType.ballBeat, color: ColorHelper.getSecondaryColor(1.0), padding: NVActivityIndicatorView.DEFAULT_PADDING)
       
        Alamofire.request(Constants.PLANIFICATION_ENDPOINT, method: .get)
            .validate()
            .response { (dataResponse) in
                
                self.stopAnimating()
                
                if var planningListResponse = PlanningController.getListPlanningFromResponse(dataResponse){
                    
                    self.planningList = PlanningController.getPlanningMissing(&planningListResponse)
                    self.planningFiltered = self.planningList
                    self.collectionViewPlanning.reloadData()
                    
                }else{
                    
                    _ = SweetAlert().showAlert("¡Algo salió mal!", subTitle: "Inténtalo de nuevo más tarde", style: AlertStyle.error)
                    
                }
                
        }
    }
    
    func collectionView(_ collectionView: UICollectionView, numberOfItemsInSection section: Int) -> Int{
        
        if searchController.isActive && searchController.searchBar.text != ""{

            return planningFiltered.count
            
        }else{
            
            return self.planningList.count
        
        }
        
        //return planningList.count
    }
    
    
    func collectionView(_ collectionView: UICollectionView, cellForItemAt indexPath: IndexPath) -> UICollectionViewCell
    {
        
        //CellBook
        let cell = collectionView.dequeueReusableCell(withReuseIdentifier: reuseIdentifier, for: indexPath) as! PlanningCollectionViewCell
        
        let plann : Planning
        
        if searchController.isActive && searchController.searchBar.text != ""{
        
            plann = planningFiltered[indexPath.row]
            
        }else{
            
            plann = planningList[indexPath.row]
            
        }
        
        cell.namePlanning.text = plann.value(forKey: "name") as? String
        cell.textDescription.text = plann.value(forKey: "descriptionPlanning") as! String
        cell.textDescription.setContentOffset(CGPoint.zero, animated: false)
        cell.textDescription.textContainer.maximumNumberOfLines = 3
        cell.fieldPlanning.text = plann.value(forKey: "field") as? String
        let url = URL(string: plann.value(forKey: "imagePlanning") as! String)
        cell.imageView.kf.setImage(with: url)
        cell.imageIndicatorViewDownload.isHidden = PlanningController.isPlanningInDatabase(plann) ? false : true
        
        cell.imageIndicatorPremium.isHidden = (plann.value(forKey: "premium") as! Int) == Constants.FREE ? false : true
        
        return cell
        
    }
    
    func numberOfSections(in collectionView: UICollectionView) -> Int
    {
        return 1
    }
    
    fileprivate let itemsPerRow: CGFloat = 2
    fileprivate let sectionInsets = UIEdgeInsets(top: 50.0, left: 5.0, bottom: 50.0, right: 20.0)
    
    func collectionView(_ collectionView: UICollectionView, layout collectionViewLayout: UICollectionViewLayout, sizeForItemAt indexPath: IndexPath) -> CGSize {
        
        let paddingSpace = sectionInsets.left * (itemsPerRow + 1)
        let availableWidth = view.frame.width - paddingSpace
        let widthPerItem = availableWidth / itemsPerRow
        return CGSize(width: widthPerItem, height: 230)
        
    }
    
    func collectionView(_ collectionView: UICollectionView, didSelectItemAt indexPath: IndexPath) {
        
        let plan : Planning
        if searchController.isActive && searchController.searchBar.text != ""{
        
            plan = planningFiltered[indexPath.row]
            
        }else{
            plan = planningList[indexPath.row]
        }
        
    
        self.planningSelected = plan    //planningList[indexPath.row]
        self.performSegue(withIdentifier: "showPlanningDetail", sender: self)
        
    }
    
    var planningSelected : Planning!
    
    override func prepare(for segue: UIStoryboardSegue, sender: Any?) {
        
        if segue.identifier == "showPlanningDetail" {
            
            let destinatioViewController = segue.destination as! PlanningDetailViewController
            destinatioViewController.planning = self.planningSelected
        }
        
    }
    
    func collectionView(_ collectionView: UICollectionView, layout collectionViewLayout: UICollectionViewLayout, insetForSectionAt section: Int) -> UIEdgeInsets {
        return UIEdgeInsetsMake(0.0, 0.0, 0.0, 0.0)
    }
    
    func collectionView(_ collectionView: UICollectionView, layout collectionViewLayout: UICollectionViewLayout, minimumLineSpacingForSectionAt section: Int) -> CGFloat {
        return 0.0
    }
    
    func collectionView(_ collectionView: UICollectionView, layout collectionViewLayout: UICollectionViewLayout, minimumInteritemSpacingForSectionAt section: Int) -> CGFloat {
        return 0.0
    }
    

}

extension PlanningViewController: UISearchResultsUpdating {
    
    func updateSearchResults(for searchController: UISearchController) {
        filterContentForSearchText(searchText: searchController.searchBar.text!)
    }
    
}
