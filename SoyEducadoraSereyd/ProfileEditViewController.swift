//
//  ProfileEditViewController.swift
//  SoyEducadoraSereyd
//
//  Created by Isaias Carrera Ventura on 10/08/17.
//  Copyright © 2017 FullStack. All rights reserved.
//

import UIKit
import Kingfisher
import SwiftValidator
import RealmSwift
import Alamofire
import NVActivityIndicatorView
import AVFoundation
import Photos
import CoreData

class ProfileEditViewController: UIViewController, ValidationDelegate, UITextFieldDelegate, NVActivityIndicatorViewable ,UINavigationControllerDelegate, UIImagePickerControllerDelegate  {
    
    
    @IBOutlet weak var scrollView: UIScrollView!
    @IBOutlet weak var imageView: UIImageView!
    @IBOutlet weak var textFieldName: UITextField!
    @IBOutlet weak var textFieldEmail: UITextField!
    
    // ViewController.swift
    let validator = Validator()
    
    override func viewDidLoad() {
        
        super.viewDidLoad()
        
        setupView()
        let urlImage = UserController.getUserInDatabase()?.value(forKey: "imageUrl") as! String
        
        let url = URL(string: urlImage)
        
        KingfisherManager.shared.retrieveImage(with: url!, options: nil, progressBlock: nil, completionHandler: { image, error, cacheType, imageURL in
            
            if error == nil {
                self.imageView.image = ImageUtils.resizeImage(image: image!, newWidth: 90.0)
                ImageUtils.circularImage(self.imageView, color: ColorHelper.getSecondaryColor(1.0).cgColor)
            }
            
        })
        
    }
    
    func setupView(){
        
        CustomBar.setNavBarProfile((self.navigationController?.navigationBar)!, navItem: navigationItem,nav: self)
        textFieldName.roundBorder()
        textFieldEmail.roundBorder()
        
        validator.registerField(textFieldName, rules: [FullNameRule(message: "Escribe tu nombre completo")])
        validator.registerField(textFieldEmail, rules: [EmailRule(message: "Escribe un email válido")])
        
        textFieldName.text = UserController.getUserInDatabase()?.value(forKey: "name") as? String
        textFieldEmail.text = UserController.getUserInDatabase()?.value(forKey: "email") as? String
        
    }
    
    //KEYBOARD METHODS
    override func touchesBegan(_ touches: Set<UITouch>, with event: UIEvent?) {
        
        self.view.endEditing(true)
        
    }
    
    func textFieldShouldReturn(_ textField: UITextField) -> Bool {
        
        textField.resignFirstResponder()
        return true
        
    }
    
    func textFieldDidEndEditing(_ textField: UITextField) {
        
        scrollView.setContentOffset(CGPoint(x: 0, y: 0), animated: true)
        
    }
    
    func textFieldDidBeginEditing(_ textField: UITextField) {
        
        scrollView.setContentOffset(CGPoint(x: 0, y: 150), animated: true)
        
    }
    
    func validationSuccessful() {
        
        var paramsToSend = [String:Any]()
        paramsToSend["name"] = textFieldName.text!
        
        if textFieldEmail.text! != UserController.getUserInDatabase()?.value(forKey: "email") as! String{
            paramsToSend["email"] = textFieldEmail.text!
        }else{
            paramsToSend["email"] = ""
        }
        
        let url = "\(Constants.USER_ENDPOINT)\(UserController.getUserInDatabase()?.value(forKey: "idUser") as! String)"
        
        //GET IMAGE AND SAVE OBJECT
        self.startAnimating(Constants.getSizeForLoadingView(view: self.view), type: NVActivityIndicatorType.ballBeat, color: ColorHelper.getSecondaryColor(1.0), padding: NVActivityIndicatorView.DEFAULT_PADDING)
        
        Alamofire.request(url, method: .put, parameters: paramsToSend)
            .validate()
            .response { (dataResponse) in
                
                self.stopAnimating()
                
                self.view.endEditing(true)
                
                if UserController.getResponseFromUpdateUser(dataResponse){
                    
                    UserController.updateUserInDatabase(UserController.getUserInDatabase()!, self.textFieldEmail.text!, self.textFieldName.text!)
                    
                    _ = SweetAlert().showAlert("Aviso", subTitle: "Perfil editado", style: AlertStyle.success, buttonTitle:"Ok", buttonColor:ColorHelper.getGreenColor(1.0))
                    
                }else if let stringErrorUpdate = AlamofireHandlerError.getErrorUpdateMessage(dataResponse){
                    
                    _ = SweetAlert().showAlert("Error", subTitle: "\(stringErrorUpdate)", style: AlertStyle.warning, buttonTitle:"Ok", buttonColor:ColorHelper.getGreenColor(1.0))
                    
                }else{
                    
                    _ = SweetAlert().showAlert("Ooops!", subTitle: "¡Algo salió mal!", style: AlertStyle.error, buttonTitle:"Ok", buttonColor:ColorHelper.getGreenColor(1.0))
                    
                }
                
        }
        
        
    }
    
    func validationFailed(_ errors:[(Validatable ,ValidationError)]) {
        var stringError = ""
        
        for (field, error) in errors {
            if let field = field as? UITextField {
                field.layer.borderColor = UIColor.red.cgColor
                field.layer.borderWidth = 1.0
            }
            
            stringError = stringError + "\n\(error.errorMessage)"
            error.errorLabel?.text = error.errorMessage // works if you added labels
            error.errorLabel?.isHidden = false
            
        }
        
        
        self.view.endEditing(true)
        _ = SweetAlert().showAlert("Error", subTitle: "¡Ingresa los datos requeridos! \(stringError)", style: AlertStyle.error, buttonTitle:"Ok", buttonColor:ColorHelper.getGreenColor(1.0))
        
        
    }
    
    override func didReceiveMemoryWarning() {
        super.didReceiveMemoryWarning()
        // Dispose of any resources that can be recreated.
    }
    
    
    @IBAction func changeImage(_ sender: Any) {
        
        let alertController = UIAlertController(
            title: "Edita tu foto de perfil",
            message: "Selecciona un método para tomar la foto",
            preferredStyle: .actionSheet)
        
        let galleryPreference = UIAlertAction(title: "Galería", style: .default) { (action) in
            
            self.galleryActionConfiguration()
            
        }
        
        alertController.addAction(galleryPreference)
        
        let cameraPreference = UIAlertAction(title: "Cámara", style: .default) { (action) in
            
            self.cameraActionConfiguration()
            
        }
        
        alertController.addAction(cameraPreference)
        
        let cancelAction = UIAlertAction(title: "Cancelar", style: .cancel) { (action) in
            
            alertController.dismiss(animated: true, completion: nil)
            
        }
        
        alertController.addAction(cancelAction)
        
        self.present(alertController, animated: true, completion: nil)
        
    }
    
    @IBAction func saveAction(_ sender: Any) {
        
        validator.validate(self)
        
    }
    
    func cameraActionConfiguration() -> Void{
        
        if UIImagePickerController.isSourceTypeAvailable(UIImagePickerControllerSourceType.camera){
            
            AVCaptureDevice.requestAccess(forMediaType: AVMediaTypeVideo, completionHandler: { (Bool) in
                
                if Bool{
                    
                    self.selectCameraImage()
                    
                }else{
                    
                    DispatchQueue.main.async {
                        
                        _ = SweetAlert().showAlert("Acceso a cámara", subTitle: "Permite el acceso a la cámara para poder tomar tu foto.", style: AlertStyle.error)
                        
                        
                    }
                }
                
            })
            
        }else{
            
            
            _ = SweetAlert().showAlert("Ooops", subTitle: "Tu dispositivo no cuenta con una cámara", style: AlertStyle.error)
            
        }
        
        
    }
    
    func galleryActionConfiguration() -> Void{
        
        if UIImagePickerController.isSourceTypeAvailable(UIImagePickerControllerSourceType.photoLibrary) && (UIImagePickerController.availableMediaTypes(for: UIImagePickerControllerSourceType.photoLibrary) != nil){
            
            PHPhotoLibrary.requestAuthorization({ (PHAuthorizationStatus) in
                
                if PHPhotoLibrary.authorizationStatus() == .authorized{
                    
                    self.selectGalleryImage()
                    
                }else{
                    
                    DispatchQueue.main.async {
                        
                        _ = SweetAlert().showAlert("Acceso a galería", subTitle: "Permite el acceso a tus fotos para poder guardar una de perfil.", style: AlertStyle.warning)
                        
                    }
                    
                }
            })
        }
        else{
            
            _ = SweetAlert().showAlert("Ooops", subTitle: "Tu dispositivo no puede abrir la galería.", style: AlertStyle.error)
            
        }
        
    }
    
    func selectGalleryImage(){
        
        let imagePicker = UIImagePickerController()
        imagePicker.delegate = self
        imagePicker.mediaTypes = ["public.image"]
        imagePicker.sourceType = UIImagePickerControllerSourceType.photoLibrary;
        imagePicker.modalPresentationStyle = .popover
        
        DispatchQueue.global().async {
            DispatchQueue.main.async {
                self.present(imagePicker, animated: true, completion: nil)
            }
        }
        
    }
    
    func selectCameraImage(){
        
        let imagePicker = UIImagePickerController()
        imagePicker.delegate = self
        imagePicker.sourceType = UIImagePickerControllerSourceType.camera;
        imagePicker.mediaTypes = ["public.image"]
        imagePicker.modalPresentationStyle = .popover
        
        DispatchQueue.global().async {
            DispatchQueue.main.async {
                
                self.present(imagePicker, animated: true, completion: nil)
                
            }
        }
        
    }
    
    func imagePickerController(_ picker: UIImagePickerController, didFinishPickingMediaWithInfo info: [String : Any]) {
        
        if let pickedImage = info[UIImagePickerControllerOriginalImage] as? UIImage {
            
            let imageURLENDPOINT = "\(Constants.USER_IMAGE_ENDPOINT)\(UserController.getUserInDatabase()!.value(forKey: "idUser") as! String)"
            let imgData = UIImageJPEGRepresentation(pickedImage, 0.2)!
            
            //GET IMAGE AND SAVE OBJECT
            self.startAnimating(Constants.getSizeForLoadingView(view: self.view), type: NVActivityIndicatorType.ballBeat, color: ColorHelper.getSecondaryColor(1.0), padding: NVActivityIndicatorView.DEFAULT_PADDING)
            
            Alamofire.upload(multipartFormData: { multipartFormData in
                multipartFormData.append(imgData, withName: "profile_picture",fileName: "profile_picture.jpg", mimeType: "image/jpg")
            },to: imageURLENDPOINT)
            { (result) in
                switch result {
                case .success(let upload, _, _):
                    
                    upload.responseJSON { response in
                        
                        self.stopAnimating()
                        
                        guard response.error == nil else{
                            
                            _ = SweetAlert().showAlert("Ooops", subTitle: "¡Algo salió mal!", style: AlertStyle.error)
                            return
                            
                        }
                        
                        guard let dataResponse = response.data else{
                            
                            _ = SweetAlert().showAlert("Ooops", subTitle: "¡Algo salió mal!", style: AlertStyle.error)
                            return
                            
                        }
                        
                        let jsonResponseChangeImage = try! JSONSerialization.jsonObject(with: dataResponse, options: .allowFragments)
                        
                        guard let dictionaryResponse = jsonResponseChangeImage as? [String:Any] else{
                            
                            _ = SweetAlert().showAlert("Ooops", subTitle: "¡Algo salió mal!", style: AlertStyle.error)
                            return
                            
                        }
                        
                        guard dictionaryResponse["error"] as! Int == 0 else{
                            _ = SweetAlert().showAlert("Ooops", subTitle: "¡Algo salió mal!", style: AlertStyle.error)
                            
                            return
                            
                        }
                        
                        
                        let urlString = dictionaryResponse["path"] as! String
                        let urlImage = "\(Constants.URL_SERVER)\(urlString)"
                        
                        UserController.updateImageUrl(UserController.getUserInDatabase()!, urlImage)
                        let url = URL(string: urlImage)
                        KingfisherManager.shared.retrieveImage(with: url!, options: [.forceRefresh], progressBlock: nil, completionHandler: { image, error, cacheType, imageURL in
                            
                            self.imageView.image = ImageUtils.resizeImage(image: image!, newWidth: 90.0)
                            ImageUtils.circularImage(self.imageView, color: ColorHelper.getPrimaryColor(1.0).cgColor)
                            
                        })
                        
                        _ = SweetAlert().showAlert("Aviso", subTitle: "Imagen cambiada con éxito", style: AlertStyle.success)
                        
                    }
                    
                case .failure( _ ):
                    self.stopAnimating()
                    
                    _ = SweetAlert().showAlert("Ooops", subTitle: "¡Algo salió mal!", style: AlertStyle.error)
                }
            }
            
        }
        
        self.dismiss(animated: true, completion: nil)
        
        
    }
    
    func imagePickerControllerDidCancel(_ picker: UIImagePickerController) {
        dismiss(animated: true, completion: nil)
    }
    
    
}
