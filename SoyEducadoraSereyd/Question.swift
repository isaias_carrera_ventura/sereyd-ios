//
//  Question.swift
//  SoyEducadoraSereyd
//
//  Created by Isaias Carrera Ventura on 23/08/17.
//  Copyright © 2017 FullStack. All rights reserved.
//

import Foundation

class Question{
    
    var idQuestion: String!
    var result: Int!
    
    init(idQuestion: String,result:Int) {
        
        self.idQuestion = idQuestion
        self.result = result
        
    }
    
}
