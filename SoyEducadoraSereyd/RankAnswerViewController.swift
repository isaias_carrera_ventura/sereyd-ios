//
//  RankAnswerViewController.swift
//  SoyEducadoraSereyd
//
//  Created by Isaias Carrera Ventura on 27/12/17.
//  Copyright © 2017 FullStack. All rights reserved.
//

import UIKit
import Cosmos
import Alamofire
import NVActivityIndicatorView


class RankAnswerViewController: UIViewController, NVActivityIndicatorViewable {
    
    var answer: Answer!
    
    @IBOutlet weak var rankingAnswer: CosmosView!
    @IBOutlet weak var labelDate: UILabel!
    @IBOutlet weak var imageView: UIImageView!
    @IBOutlet weak var textViewAnswer: UITextView!
    @IBOutlet weak var rankingSendAnswer: CosmosView!
    
    var flag = false
    
    override func viewDidLoad() {
        
        super.viewDidLoad()
        
        CustomBar.setNavBarProfile((self.navigationController?.navigationBar)!, navItem: navigationItem,nav: self)
        
        let url = URL(string: self.answer.image!)
        imageView.kf.setImage(with: url)
        ImageUtils.circularImage(imageView, color: ColorHelper.getSecondaryColor(1.0).cgColor)
        
        labelDate.text = answer.date
        textViewAnswer.text = answer.answer
        rankingAnswer.rating = Double(answer.ranking!)
        
        rankingSendAnswer.didTouchCosmos = { value in
            
            self.flag = true
            
        }
        
    }
    
    override func didReceiveMemoryWarning() {
        super.didReceiveMemoryWarning()
        // Dispose of any resources that can be recreated.
    }
    
    
    
    @IBAction func sendRanking(_ sender: Any) {
        
        if self.flag {
            
            let params : Parameters = ["ranking":self.rankingSendAnswer.rating,"id_user":UserController.getUserInDatabase()?.value(forKey: "idUser") as! String,"id_answer":self.answer.idAnswer!]
            
            
            self.startAnimating(Constants.getSizeForLoadingView(view: self.view), type: NVActivityIndicatorType.ballBeat, color: ColorHelper.getSecondaryColor(1.0), padding: NVActivityIndicatorView.DEFAULT_PADDING)
            
            Alamofire.request(Constants.URL_ANSWER_RANKING, method: .post, parameters: params)
                .validate()
                .response { (dataResponse) in
                    
                    self.stopAnimating()
                    
                    if dataResponse.response?.statusCode == 200{
                        
                        _ = SweetAlert().showAlert("Calificación enviada", subTitle: "Agradecemos tu participación.", style: .success, buttonTitle: "Ok", action: { (flag) in
                            
                            self.navigationController?.popViewController(animated: true)
                            
                        })
                    }else if  dataResponse.response?.statusCode == 401 {
                        
                        _ = SweetAlert().showAlert("Ooops!", subTitle: "¡Ya calificaste esta respuesta previamente!", style: AlertStyle.error, buttonTitle:"Ok", buttonColor:ColorHelper.getGreenColor(1.0))
                        
                    }
                    else{
                        
                        
                        _ = SweetAlert().showAlert("Ooops!", subTitle: "¡Algo salió mal!", style: AlertStyle.error, buttonTitle:"Ok", buttonColor:ColorHelper.getGreenColor(1.0))
                        
                    }
                    
            }
            
        }else{
            
            
            _ = SweetAlert().showAlert("Ooops!", subTitle: "¡Califica la respuesta!", style: AlertStyle.error, buttonTitle:"Ok", buttonColor:ColorHelper.getGreenColor(1.0))
            
        }
        
        
    }
    
}
