//
//  ResourceContainerModel.swift
//  SoyEducadoraSereyd
//
//  Created by Isaias Carrera Ventura on 04/08/17.
//  Copyright © 2017 FullStack. All rights reserved.
//

import Foundation

class ResourceContainerModel{
    
    var name: String?
    var idResource : String?
    
    init(name: String,idResource: String) {
        self.name = name
        self.idResource = idResource
    }
    
    
}
