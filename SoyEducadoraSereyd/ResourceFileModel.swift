//
//  ResourceFileModel.swift
//  SoyEducadoraSereyd
//
//  Created by Isaias Carrera Ventura on 04/08/17.
//  Copyright © 2017 FullStack. All rights reserved.
//

import Foundation
import RealmSwift

class ResourceFileModel: Object{
    
    var idResouceFile : String?
    var name: String?
    var url: String?
    
}
