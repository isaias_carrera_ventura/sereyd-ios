//
//  ResourceItemCellTableViewCell.swift
//  SoyEducadoraSereyd
//
//  Created by Isaias Carrera Ventura on 01/08/17.
//  Copyright © 2017 FullStack. All rights reserved.
//

import UIKit
import SwiftyButton

class ResourceItemCellTableViewCell: UITableViewCell {

    @IBOutlet weak var buttonResource: FlatButton!
    var tapAction: ((UITableViewCell) -> Void)?
    
    @IBAction func tapCellResource(_ sender: Any) {
        
        tapAction?(self)
        
    }

}
