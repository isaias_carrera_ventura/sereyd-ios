//
//  ResourcesListViewController.swift
//  SoyEducadoraSereyd
//
//  Created by Isaias Carrera Ventura on 01/08/17.
//  Copyright © 2017 FullStack. All rights reserved.
//

import UIKit
import Alamofire
import NVActivityIndicatorView

class ResourcesListViewController: UIViewController, UITableViewDelegate,UITableViewDataSource, NVActivityIndicatorViewable {

    @IBOutlet weak var tableView: UITableView!
    
    var items = [ResourceContainerModel]()
    
    override func viewDidLoad() {
        super.viewDidLoad()
        
        CustomBar.setNavBarProfile((self.navigationController?.navigationBar)!, navItem: navigationItem,nav: self)
        getResourcesForCurrentUser()
        
    }
    
    public func getResourcesForCurrentUser() {
        
        self.startAnimating(Constants.getSizeForLoadingView(view: self.view), type: NVActivityIndicatorType.ballBeat, color: ColorHelper.getSecondaryColor(1.0), padding: NVActivityIndicatorView.DEFAULT_PADDING)
        
        let url = "\(Constants.RESOURCE_USER_ENPOINT)\(UserController.getUserInDatabase()!.value(forKey: "idUser") as! String)"
        
        Alamofire.request(url, method: .get)
        .validate()
        .response { (dataResponse) in
            
            self.stopAnimating()
            
            if let resourcesListForUser = ResourceController.getResourceFileFromResponse(dataResponse){
                
                ResourceController.saveListResourceToDatabase(resourcesListForUser)
                
            }
        }
    
    }
    
    public func tableView(_ tableView: UITableView, numberOfRowsInSection section: Int) -> Int
    {
        
        return items.count
        
    }
    
    var stringId: String?
    
    func tableView(_ tableView: UITableView, cellForRowAt indexPath: IndexPath) -> UITableViewCell {
        
        let cell = tableView.dequeueReusableCell(withIdentifier: "Cell", for: indexPath) as! ResourceItemCellTableViewCell
        cell.buttonResource.color = ColorHelper.getRandomColorForResource()
        cell.buttonResource.setTitle((items[indexPath.row].name!), for: .normal)
        cell.tapAction = { (cell) in
            
            self.stringId = self.items[indexPath.row].idResource!
            self.performSegue(withIdentifier: "resourceDetailSegue", sender: self)
            
        }
        
        return cell
        
    }

    
    override func prepare(for segue: UIStoryboardSegue, sender: Any?) {
        
        if segue.identifier == "resourceDetailSegue" {
            
            let viewController = segue.destination as! ResourceDetailViewController
            viewController.stringIdResourceContainer = self.stringId
            
        }
        
    }

}
