//
//  StartDayViewController.swift
//  SoyEducadoraSereyd
//
//  Created by Isaias Carrera Ventura on 20/07/17.
//  Copyright © 2017 FullStack. All rights reserved.
//

import UIKit

class StartDayViewController: UIViewController, UICollectionViewDataSource,UICollectionViewDelegateFlowLayout {
    
    @IBOutlet weak var collectionViewGroup: UICollectionView!
    var groupArray = [GroupModel]()
    var groupSelected : GroupModel?
    
    override func viewDidLoad() {
        super.viewDidLoad()
        CustomBar.setNavBarProfile((self.navigationController?.navigationBar)!, navItem: navigationItem,nav: self)
        collectionViewGroup.showsVerticalScrollIndicator = true
        collectionViewGroup.indicatorStyle = UIScrollViewIndicatorStyle.black
        groupArray = GroupController.getGroupInDatabase()
    }
    
    func collectionView(_ collectionView: UICollectionView, numberOfItemsInSection section: Int) -> Int {
        return groupArray.count
    }
    
    func collectionView(_ collectionView: UICollectionView, cellForItemAt indexPath: IndexPath) -> UICollectionViewCell {
        
        let cell = collectionView.dequeueReusableCell(withReuseIdentifier: "groupCell", for: indexPath) as! GroupDayCollectionViewCell
        let name = "\(groupArray[indexPath.row].value(forKey: "grade") as! String) \(groupArray[indexPath.row].value(forKey: "group") as! String)"
        cell.nameGroup.text = name
        return cell
        
    }
    
    func collectionView(_ collectionView: UICollectionView, didSelectItemAt indexPath: IndexPath) {
        
        groupSelected = groupArray[indexPath.row]
        if (StudentController.getStudentInDatabaseByGroup(groupSelected!)?.count)! > 0{
            self.performSegue(withIdentifier: "listStartDay", sender: self)
            
            
        }else{
            _ = SweetAlert().showAlert("Oops!", subTitle: "No tienes alumnos en este grupo", style: AlertStyle.warning)
        }
        
        
    }
    
    private func collectionView(collectionView: UICollectionView, layout collectionViewLayout: UICollectionViewLayout, sizeForItemAtIndexPath indexPath: IndexPath) -> CGSize {
        let itemsPerRow:CGFloat = 4
        let hardCodedPadding:CGFloat = 5
        let itemWidth = (collectionView.bounds.width / itemsPerRow) - hardCodedPadding
        let itemHeight = collectionView.bounds.height - (2 * hardCodedPadding)
        return CGSize(width: itemWidth, height: itemHeight)
    }
    
    override func prepare(for segue: UIStoryboardSegue, sender: Any?) {
        
        if segue.identifier == "listStartDay" {
            let destinatioViewController = segue.destination as! ListDayStartViewController
            destinatioViewController.group = self.groupSelected
        }
        
    }
    
}
