//
//  StudentController.swift
//  SoyEducadoraSereyd
//
//  Created by Isaias Carrera Ventura on 30/05/17.
//  Copyright © 2017 FullStack. All rights reserved.
//

import Foundation
import RealmSwift
import Alamofire

class StudentController{

	
	class func addStudentToDatabase(_ student: StudentModel) -> Void{
		
		let realm = try! Realm()
		
		try! realm.write {
			
			realm.add(student)
			
		}
		
	}
    
    class func getStudentById(_ idStudent: String) -> StudentModel? {
     
        let realm = try! Realm()
        
        let students = realm.objects(StudentModel.self).filter("idStudent = '\(idStudent)'")
        
        if students.count > 0{
            return students[0]
        }
        
        return nil
        
    }
    
    class func getInformationByStudent(_ student: StudentModel) -> InformationStudentModel? {
        
        let realm = try! Realm()
        let resultInformation = realm.objects(InformationStudentModel.self) // retrieves all Dogs from the default Realm
        let arrayInformation = Array(resultInformation)
        
        var informationFoundForStudent: InformationStudentModel?
        
        let idStudent = student.value(forKey: "idStudent") as! String
        for informationItem in arrayInformation{
            
            if informationItem.student?.value(forKey: "idStudent") as! String == idStudent{
                
                informationFoundForStudent = informationItem
                return informationFoundForStudent
                
            }
            
        }
        
        return nil
        
    }
    
    class func updateInformationForStudent( _ infoFromServer : InformationStudentModel, _ infoInDatabase: InformationStudentModel) -> Void{
        
        let realm = try! Realm()
        
        
        try! realm.write {
            
            infoInDatabase.setValue(infoFromServer.value(forKey: "attendanceAccount") as! Int, forKey: "attendanceAccount")
            infoInDatabase.setValue(infoFromServer.value(forKey: "descriptionBehavior") as! String, forKey: "descriptionBehavior")
            infoInDatabase.setValue(infoFromServer.value(forKey: "averageBehavior") as! Double, forKey: "averageBehavior")
            infoInDatabase.setValue(infoFromServer.value(forKey: "language") as! Double, forKey: "language")
            infoInDatabase.setValue(infoFromServer.value(forKey: "math") as! Double, forKey: "math")
            infoInDatabase.setValue(infoFromServer.value(forKey: "world") as! Double, forKey: "world")
            infoInDatabase.setValue(infoFromServer.value(forKey: "health") as! Double, forKey: "health")
            infoInDatabase.setValue(infoFromServer.value(forKey: "social") as! Double, forKey: "social")
            infoInDatabase.setValue(infoFromServer.value(forKey: "art") as! Double, forKey: "art")
            infoInDatabase.setValue(infoFromServer.value(forKey: "diagnostic") as! Double, forKey: "diagnostic")
            infoInDatabase.setValue(infoFromServer.value(forKey: "schoolPerformance") as! Double, forKey: "schoolPerformance")
            
        }
        
    }
    
    class func addInformationStudentToDatabase(_ information: InformationStudentModel) -> Void{
        
        let realm = try! Realm()
        
        try! realm.write {
            
            realm.add(information)
            
        }
        
    }
    
    class func getListStudentForGroup(_ dataResponse: DefaultDataResponse, _ group : GroupModel) -> [StudentModel]? {
    
        guard dataResponse.error == nil else{
            
            return nil
            
        }
        
        guard let dataResponse = dataResponse.data else{
            
            return nil
            
        }
        
        let jsonResponse = try? JSONSerialization.jsonObject(with: dataResponse, options: .allowFragments) as! [String:Any]
        
        guard (jsonResponse) != nil else{
            
            return nil
            
        }
        
        if (jsonResponse!["error"] as? Int) == 1 {
            
            return nil
            
        }
        
        guard let dataObject = jsonResponse?["data"] as? NSArray else{
            
            return nil
        }
        
        var studentArrayToReturn = [StudentModel]()
        
        for object in dataObject {
            
            let objectRaw = object as! [String:Any]
            let studentArray = objectRaw["students"] as! NSArray
            let idGroup = group.value(forKey: "idGroup") as! String
            
            for student in studentArray{
                
                let studentJson = student as! [String:Any]
                let studentIdGroup = studentJson["id_group"] as! String
                
                if idGroup == studentIdGroup{
                    
                    let name = studentJson["name"] as? String
                    let lastname = studentJson["lastname"] as? String
                    let sex = studentJson["gender"] as? String
                    let emergencyContanct = studentJson["emergency_contact"] as? String
                    let birthdate = studentJson["birthday"] as? String
                    let emailTutor = studentJson["email"] as? String
                    let idStudent = studentJson["id_student"] as? String
                    let imageUrl = "\(Constants.URL_SERVER)\(String(describing: studentJson["image"] as! String))"
                    
                    let student = StudentModel()
                    student.name = name
                    student.lastname = lastname
                    student.sex = Int(sex!)!
                    student.emergencyContanct = emergencyContanct
                    student.birthdate = birthdate
                    student.emailTutor = emailTutor
                    student.idStudent = idStudent
                    student.imageUrl = imageUrl

                    studentArrayToReturn.append(student)
                    
                }
                
                
            }
            
            
        }
        
        return studentArrayToReturn
        
    }
	
	class func deleteStudentInDatabase(student: StudentModel) -> Void{
		
		let realm = try! Realm()
		
		// Delete an object with a transaction
		try! realm.write {
			realm.delete(student)
		}
		
	}
	
	class func editStudentInDatabase(student: StudentModel,name: String,lastname: String,sex: Int,emergencyContact: String, birthday: String,email: String?) -> Void{
		
		let realm  = try! Realm()
		
		try! realm.write {
			
			student.setValue(name, forKey: "name")
			student.setValue(lastname, forKey: "lastname")
			student.setValue(sex, forKey: "sex")
			student.setValue(emergencyContact, forKey: "emergencyContanct")
			student.setValue(birthday, forKey: "birthdate")
			
			//save optional email
			if email != nil {
				student.setValue(email, forKey: "emailTutor")
			}
			
		}
		
	}
	
	class func getAccountForStudentsInGroup(group: GroupModel) -> (men: Int,women: Int, total: Int){
		
		let studentList = group.value(forKey: "students") as? LinkingObjects<StudentModel>
		
		let male = studentList?.filter("sex == \(Constants.MALE)")
		let female = studentList?.filter("sex == \(Constants.FEMALE)")
		let account = studentList?.count
		
		return(male!.count,female!.count,account!)
		
	}
	
	
	class func getStudentByGroupFromResponse(_ dataResponse: DefaultDataResponse) -> StudentModel? {
		
		guard dataResponse.error == nil else{
			
			return nil
			
		}
		
		guard let dataResponse = dataResponse.data else{
			
			return nil
			
		}
		
		let jsonResponse = try? JSONSerialization.jsonObject(with: dataResponse, options: .allowFragments) as! [String:Any]
		
		guard (jsonResponse) != nil else{
			
			return nil
			
		}
		
		if (jsonResponse!["error"] as? Int) == 1 {
			
			return nil
			
		}
		
		let studentJson = jsonResponse!["student"] as! [String:Any]
		
		let name = studentJson["name"] as? String
		let lastname = studentJson["lastname"] as? String
		let sex = studentJson["gender"] as? String
		let emergencyContanct = studentJson["emergency_contact"] as? String
		let birthdate = studentJson["birthday"] as? String
		let emailTutor = studentJson["email"] as? String
		let idStudent = studentJson["id_student"] as? String
        let imageUrl = "\(Constants.URL_SERVER)\(String(describing: studentJson["image"] as! String))"
		
		let student = StudentModel()
		student.name = name
		student.lastname = lastname
		student.sex = Int(sex!)!
		student.emergencyContanct = emergencyContanct
		student.birthdate = birthdate
		student.emailTutor = emailTutor
		student.idStudent = idStudent
        student.imageUrl = imageUrl
		
		return student
		
	}
	
	class func getResponseFromDeleteStudent(_ dataResponse: DefaultDataResponse) -> Bool {
		
		guard dataResponse.error == nil else{
			
			return false
			
		}
		
		guard let dataResponse = dataResponse.data else{
			
			return false
			
		}
		
		let jsonResponse = try? JSONSerialization.jsonObject(with: dataResponse, options: .allowFragments) as! [String:Any]
		
		guard (jsonResponse) != nil else{
			
			return false
			
		}
		
		if (jsonResponse!["error"] as? Int) != 0 {
			
			return false
			
		}
		
		return true

		
	}
	
	
	class func getStudentListFromResponse(_ dataResponse: DefaultDataResponse) -> [StudentModel]? {
	
		
		guard dataResponse.error == nil else{
			
			return nil
			
		}
		
		guard let dataResponse = dataResponse.data else{
			
			return nil
			
		}
		
		let jsonResponse = try? JSONSerialization.jsonObject(with: dataResponse, options: .allowFragments) as! [String:Any]
		
		guard (jsonResponse) != nil else{
			
			return nil
			
		}
		
		if (jsonResponse!["error"] as? Int) == 1 {
			
			return nil
			
		}
		 
		guard let groupObject = jsonResponse?["data"] as? [String:Any] else{
			
			return nil
		}
		
		guard let studentsArrayRaw = groupObject["students"] as? NSArray else{
			
			return nil
			
		}
		
		 
		var studentArray = [StudentModel]()
		
		if studentsArrayRaw.count == 0 {
			
			return studentArray
			
		}
		
		
		for studentInArray in studentsArrayRaw{
			
			let studentObject = studentInArray as! [String:Any]
			
			let student = StudentModel()
			student.birthdate = studentObject["birthday"] as? String
			student.emailTutor = studentObject["email"] as? String
			student.emergencyContanct = studentObject["emergency_contact"] as? String
			student.sex = Int((studentObject["gender"]  as! NSString).intValue)
			student.lastname = studentObject["lastname"] as? String
			student.name = studentObject["name"] as? String
			student.idStudent = studentObject["id_student"] as? String
			student.imageUrl = "\(Constants.URL_SERVER)\(String(describing: studentObject["image"] as! String))"
			studentArray.append(student)
			
		}
		
		return studentArray
		
		
	}
	
	class func saveListUnrepeatedStudentInList(_ studentArray : [StudentModel], _ group: GroupModel ) -> Void{
		
		let listStudentInDatabase = getStudentInDatabaseByGroup(group)
		
		if  studentArray.count > listStudentInDatabase!.count {
			
			for student in studentArray{
				
				var isStudentInDatabase = false
				
				for studentInDatabase in listStudentInDatabase!{
					
					if(studentInDatabase.value(forKey: "idStudent") as! String == student.idStudent!){
						isStudentInDatabase = true
			 		}
					
				}
				
				if !isStudentInDatabase{
					student.groups.append(group)
					addStudentToDatabase(student)
				}
				
			}
			
		}else if  studentArray.count < listStudentInDatabase!.count{
			
			for student in listStudentInDatabase!{
				
				var isStudentInServer = false
				
				for studentInServer in studentArray {
					
					if student.value(forKey: "idStudent") as? String == studentInServer.idStudent{
						isStudentInServer = true
					}
				}
				
				if !isStudentInServer {
					
					deleteStudentInDatabase(student: student)
					
				}
				
			}
			
			
		}
		
		saveChangesInDatabaseUpdate(getStudentInDatabaseByGroup(group)!,studentArray)

		
		
	}
	
	class func saveChangesInDatabaseUpdate(_ studentInDb:[StudentModel] , _ studentInServer : [StudentModel]){
		
		for student in studentInDb{
			
			if let studentAux = getStudentInServerById(student.value(forKey: "idStudent") as! String, studentInServer){
				editStudentInDatabase(student: student, name: studentAux.name!, lastname: studentAux.lastname!, sex: studentAux.sex, emergencyContact: studentAux.emergencyContanct!, birthday: studentAux.birthdate!, email: studentAux.emailTutor)
			}
		}
		
	}
	
	class func editResponseFromServer(_ dataResponse: DefaultDataResponse) -> Bool{
		
		guard dataResponse.error == nil else{
			
			return false
			
		}
		
		guard let dataResponse = dataResponse.data else{
			
			return false
			
		}
		
		let jsonResponse = try? JSONSerialization.jsonObject(with: dataResponse, options: .allowFragments) as! [String:Any]
		
		guard (jsonResponse) != nil else{
			
			return false
			
		}
		
		if (jsonResponse!["error"] as? Int) == 1 {
			
			return false
			
		}
		
		return true
	}
    
    
    class func getInfoStudentFromResponse(_ dataResponse: DefaultDataResponse) -> InformationStudentModel? {
        
        guard dataResponse.error == nil else{
            
            return nil
            
        }
        
        guard let dataResponse = dataResponse.data else{
            
            return nil
            
        }
        
        let jsonResponse = try? JSONSerialization.jsonObject(with: dataResponse, options: .allowFragments) as! [String:Any]
        
        guard (jsonResponse) != nil else{
            
            return nil
            
        }
        
        if (jsonResponse!["error"] as? Int) == 1 {
            
            return nil
            
        }
        let attendanceObj = jsonResponse?["attendance"] as! [String:Any]
        let attendanceCount = (attendanceObj["count"] as! NSString).integerValue
        let behaviorObj = jsonResponse?["behavior"] as! [String:Any]
        let behavior = behaviorObj["description"] as! String
        var behaviorD : Double = 0.0
        
        if let bStr = behaviorObj["average"] as? String, let behaviorAverage = Double(bStr){
            behaviorD = behaviorAverage
        }
        
        let language = jsonResponse?["language"] as! Double
        let math = jsonResponse?["math"] as! Double
        let world = jsonResponse?["world"] as! Double
        let health = jsonResponse?["health"] as! Double
        let social = jsonResponse?["social"] as! Double
        let art = jsonResponse?["art"] as! Double
        
        let diagnostic = jsonResponse?["diagnostic"] as! Double
        let performance = jsonResponse?["school_performance"] as! Double
        
        
        let studentInformation = InformationStudentModel()
        studentInformation.attendanceAccount = attendanceCount
        studentInformation.descriptionBehavior = behavior
        studentInformation.averageBehavior = behaviorD
        studentInformation.language = language
        studentInformation.math = math
        studentInformation.world = world
        studentInformation.health = health
        studentInformation.social = social
        studentInformation.art = art
        studentInformation.diagnostic = diagnostic
        studentInformation.schoolPerformance = performance
        
        return studentInformation
        
        
    }
	
	class func getStudentInServerById(_ idStudent: String,_ elements: [StudentModel] ) -> StudentModel?{
		
		for student in elements{
			
			if student.idStudent == idStudent{
				return student
			}
		}
		
		return nil
		
	}
	
	class func getStudentInDatabaseByGroup(_ group: GroupModel) -> [StudentModel]?{
		
		var items: LinkingObjects<StudentModel>!
		items = group.value(forKey: "students") as? LinkingObjects<StudentModel>
		
		var arrayStudent = [StudentModel]()
		
		for student in items{
		
			arrayStudent.append(student)
			
		}
		
		return arrayStudent
		
	}

}
