//
//  StudentDiagnoseViewController.swift
//  SoyEducadoraSereyd
//
//  Created by Isaias Carrera Ventura on 11/07/17.
//  Copyright © 2017 FullStack. All rights reserved.
//

import UIKit
import SwiftyJSON
import Alamofire
import NVActivityIndicatorView

class StudentDiagnoseViewController: UIViewController, UITableViewDelegate, UITableViewDataSource, NVActivityIndicatorViewable {
    
    var group : GroupModel?
    var diagnose: DiagnoseModel?
    var currentItemIndex = 0
    var studentArray : [StudentModel]?
    var test : [TestModel]?
    
    @IBOutlet weak var labelNameStudent: UILabel!
    @IBOutlet weak var tableView: UITableView!
    
    override func viewDidLoad() {
        
        super.viewDidLoad()
        CustomBar.setNavBarProfile((self.navigationController?.navigationBar)!, navItem: navigationItem,nav: self)
        
        self.navigationItem.hidesBackButton = true
        
        let newBackButton = UIBarButtonItem(title: "Back", style: .plain, target: self, action: #selector(StudentDiagnoseViewController.back(sender:)))
        
        self.navigationItem.leftBarButtonItem = newBackButton
        
        tableView.setContentOffset(.zero, animated: false)
        
        if studentArray == nil {
            
            studentArray = StudentController.getStudentInDatabaseByGroup(group!)
            
        }
        
        
        if currentItemIndex < studentArray!.count {
            
            loadViewForStudent(studentArray![currentItemIndex])
            
        }else{
            
            self.startAnimating(Constants.getSizeForLoadingView(view: self.view), type: NVActivityIndicatorType.ballBeat, color: ColorHelper.getSecondaryColor(1.0), padding: NVActivityIndicatorView.DEFAULT_PADDING)
            
            let url = "\(Constants.TEST_COMPLETE_DIAGNOSE_ENDPOINT)\(diagnose?.value(forKey: "idGroupDiagnose") as! String)"
            
            Alamofire.request(url, method: .put).validate()
            .response(completionHandler: { (dataResponse) in
                
                self.stopAnimating()
                
                let boolCompleteDiagnose = TestController.getResponseForCompletingDiagnose(dataResponse)
                if boolCompleteDiagnose{
                    
                    DiagnoseController.editDiagnoseStateInDatabase(diagnose: self.diagnose!, state: Constants.COMPLETE)
                    
                    _ = SweetAlert().showAlert("Aviso", subTitle: "¡Diagnóstico completado!", style: AlertStyle.success, buttonTitle:"Ok", buttonColor:ColorHelper.getGreenColor(1.0)) { (isOtherButton) -> Void in
                        
                        
                        self.navigationController?.popViewController(animated: true)
                        
                    }

                    
                }else{
                    
                    _ = SweetAlert().showAlert("Ooops!", subTitle: "Algo salió mal", style: AlertStyle.warning)
                    
                }
                
            })
            
        }
        
    }
    
    
    func loadViewForStudent(_ student: StudentModel){
        
        let name = "\(student.value(forKey: "name") as! String) \(student.value(forKey: "lastname") as! String)"
        labelNameStudent.text = name
        
        for testObject in test!{
            
            testObject.result = -1
            
        }
        
        tableView.reloadData()
        
    }
    
    public func tableView(_ tableView: UITableView, numberOfRowsInSection section: Int) -> Int{
        return test!.count
    }
    
    public func tableView(_ tableView: UITableView, cellForRowAt indexPath: IndexPath) -> UITableViewCell{
        
        let cell = tableView.dequeueReusableCell(withIdentifier: "Cell", for: indexPath) as! QuestionTableViewCell
        
        cell.segmentedControl.selectedSegmentIndex = test![indexPath.row].result
        cell.segmentedControl.tag = indexPath.row
        cell.questionLabel.text = test![indexPath.row].questionString
        cell.typeQuestion.text = test![indexPath.row].nameTest
        cell.segmentedControl.addTarget(self, action: #selector(StudentDiagnoseViewController.updateRank(_:)), for: UIControlEvents.valueChanged)
        
        return cell
        
    }
    
    func updateRank(_ sender: UISegmentedControl){        test?[sender.tag].result = sender.selectedSegmentIndex
    }
    
    
    @IBAction func diagnoseStudent(_ sender: Any) {
        
        var isDiagnoseComplete : Bool = true
        
        for testObject in test!{
            
            if testObject.result == -1 {
                isDiagnoseComplete = false
            }
            
        }
        
        if isDiagnoseComplete{
            
            let idStudent = studentArray![currentItemIndex].value(forKey: "idStudent") as! String
            let idDiagnoseGroup = diagnose?.value(forKey: "idGroupDiagnose") as! String
            
            var questionArray = [Question]()
            
            var jsonQuestionList = [JSON]()
            
            for testObject in test!{
                
                let question = Question(idQuestion: testObject.idQuestion!, result: testObject.result)
                let jsonQuestion : JSON = ["id_question":question.idQuestion,"answer":question.result, "id_test": testObject.idTest!]
                
                jsonQuestionList.append(jsonQuestion)
                questionArray.append(question)
                
            }
            
            let jsonToSend : JSON = ["id_group_diagnostic": idDiagnoseGroup ,"id_student":idStudent , "answers" : jsonQuestionList]
            
            let url = Constants.QUESTION_USER_ENPOINT
            let urlRaw = URL(string: url)
            var request = URLRequest(url: urlRaw!)
            request.httpMethod = "POST"
            request.setValue("application/json", forHTTPHeaderField: "Content-Type")
            
            let jsonRaw = jsonToSend.rawString()!.parseJSONString
            request.httpBody = try! JSONSerialization.data(withJSONObject: jsonRaw!)
            
            self.startAnimating(Constants.getSizeForLoadingView(view: self.view), type: NVActivityIndicatorType.ballBeat, color: ColorHelper.getSecondaryColor(1.0), padding: NVActivityIndicatorView.DEFAULT_PADDING)
            
            Alamofire.request(request).validate()
                .responseJSON { (dataResponse) in
                    
                    self.stopAnimating()
                    
                    let isDiagnoseCorrect = TestController.getResponseForRangedDiagnose(dataResponse)
                    
                    if isDiagnoseCorrect{
                        
                        /*let result = ResultModel()
                        result.result = 10
                        let student : StudentModel! = self.studentArray![self.currentItemIndex]
                        result.student = student
                        result.diagnoses.append(self.diagnose!)
                        ResultController.addResultToDatabase(result)*/
                        
                        self.currentItemIndex += 1
                        
                        _ = SweetAlert().showAlert("Diagnóstico", subTitle: "Alumno calificado", style: AlertStyle.success, buttonTitle:"Ok", buttonColor:ColorHelper.getGreenColor(1.0)) { (isOtherButton) -> Void in
                            
                            self.viewDidLoad()
                            self.viewWillAppear(true)
                            
                        }
                        
                    }else{
                        
                        _ = SweetAlert().showAlert("Ooops!", subTitle: "Algo salió mal", style: AlertStyle.warning)
                        
                    }
                    
            }
            
        }else{
            
            _ = SweetAlert().showAlert("Ooops!", subTitle: "Finaliza tu diagnóstico correctamente!", style: AlertStyle.warning)
            
        }
        
        
    }
    
    func back(sender: UIBarButtonItem){
        
        SweetAlert().showAlert("No has terminado", subTitle: "¿Deseas acabarlo después?", style: AlertStyle.warning, buttonTitle:"Continuar", buttonColor:ColorHelper.UIColorFromHex(0xD0D0D0) , otherButtonTitle:  "Más tarde", otherButtonColor: ColorHelper.getGreenColor(1.0)) { (isOtherButton) -> Void in
            if isOtherButton == true {
                
            }else{
                
                //CLOSE
                _ = self.navigationController?.popViewController(animated: true)
                
            }
            
        }
    }
    
}
