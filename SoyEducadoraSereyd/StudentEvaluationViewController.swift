//
//  StudentEvaluationViewController.swift
//  SoyEducadoraSereyd
//
//  Created by Isaias Carrera Ventura on 23/08/17.
//  Copyright © 2017 FullStack. All rights reserved.
//

import UIKit
import SwiftyJSON
import Alamofire
import NVActivityIndicatorView

class StudentEvaluationViewController: UIViewController, UITableViewDelegate, UITableViewDataSource,NVActivityIndicatorViewable {

    @IBOutlet weak var tableView: UITableView!
    @IBOutlet weak var imageView: UIImageView!
    @IBOutlet weak var nameStudentLabel: UILabel!
    
    var currentItemIndex = 0
    var studentArray : [StudentModel]?
    var group : GroupModel?
    var test : [TestModel]?
    var evaluation : Evaluation?
    
    override func viewDidLoad() {
        super.viewDidLoad()
        
        CustomBar.setNavBarProfile((self.navigationController?.navigationBar)!, navItem: navigationItem,nav: self)
        
        self.navigationItem.hidesBackButton = true
        let newBackButton = UIBarButtonItem(title: "Back", style: .plain, target: self, action: #selector(StudentDiagnoseViewController.back(sender:)))
        
        self.navigationItem.leftBarButtonItem = newBackButton
        
        tableView.setContentOffset(.zero, animated: false)
        if studentArray == nil {
            
            studentArray = StudentController.getStudentInDatabaseByGroup(group!)
            
        }
        
        
        if currentItemIndex < studentArray!.count {
            
            loadViewForStudent(studentArray![currentItemIndex])
            
        }else{
            
            self.startAnimating(Constants.getSizeForLoadingView(view: self.view), type: NVActivityIndicatorType.ballBeat, color: ColorHelper.getSecondaryColor(1.0), padding: NVActivityIndicatorView.DEFAULT_PADDING)
            
            let url = "\(Constants.TEST_COMPLETE_EVALUATION_ENDPOINT)\(evaluation?.value(forKey: "idGroupEvaluation") as! String)"
            Alamofire.request(url, method: .put).validate()
                .response(completionHandler: { (dataResponse) in
                    
                    self.stopAnimating()
                    
                    let boolCompleteDiagnose = TestController.getResponseForCompletingDiagnose(dataResponse)
                    if boolCompleteDiagnose{
                        
                        EvaluationController.editEvaluationStateInDatabase(evaluation: self.evaluation!, state: Constants.COMPLETE)
                        
                        _ = SweetAlert().showAlert("Aviso", subTitle: "¡Evaluación completada!", style: AlertStyle.success, buttonTitle:"Ok", buttonColor:ColorHelper.getGreenColor(1.0)) { (isOtherButton) -> Void in
                            
                            
                            self.navigationController?.popViewController(animated: true)
                            
                        }
                        
                        
                    }else{
                        
                        _ = SweetAlert().showAlert("Ooops!", subTitle: "Algo salió mal", style: AlertStyle.warning)
                        
                    }
                    
                })
            
        }

    }
    
    func loadViewForStudent(_ student: StudentModel){
        
        let name = "\(student.value(forKey: "name") as! String) \(student.value(forKey: "lastname") as! String)"
        nameStudentLabel.text = name
        
        for testObject in test!{
            
            testObject.result = -1
            
        }
        
        tableView.reloadData()
        
    }
    
    public func tableView(_ tableView: UITableView, numberOfRowsInSection section: Int) -> Int{
        return test!.count
    }

    public func tableView(_ tableView: UITableView, cellForRowAt indexPath: IndexPath) -> UITableViewCell{
        
        let cell = tableView.dequeueReusableCell(withIdentifier: "Cell", for: indexPath) as! EvaluationQuestionTableViewCell
        
        cell.segmentedControl.selectedSegmentIndex = test![indexPath.row].result
        cell.segmentedControl.tag = indexPath.row
        cell.questionLabel.text = test![indexPath.row].questionString
        //cell.typeQuestion.text = test![indexPath.row].nameTest
        cell.segmentedControl.addTarget(self, action: #selector(StudentDiagnoseViewController.updateRank(_:)), for: UIControlEvents.valueChanged)
        
        return cell
        
    }
    
    func updateRank(_ sender: UISegmentedControl){
        test?[sender.tag].result = sender.selectedSegmentIndex
    }

    override func didReceiveMemoryWarning() {
        super.didReceiveMemoryWarning()
        // Dispose of any resources that can be recreated.
    }
    
    func back(sender: UIBarButtonItem){
        
        SweetAlert().showAlert("No has terminado", subTitle: "¿Deseas acabarlo después?", style: AlertStyle.warning, buttonTitle:"Continuar", buttonColor:ColorHelper.UIColorFromHex(0xD0D0D0) , otherButtonTitle:  "Más tarde", otherButtonColor: ColorHelper.getGreenColor(1.0)) { (isOtherButton) -> Void in
            if isOtherButton == true {
                
                
            }else{
                
                //CLOSE
                _ = self.navigationController?.popViewController(animated: true)
                
            }
            
        }
    }

    @IBAction func save(_ sender: Any) {
        
        var isDiagnoseComplete : Bool = true
        
        for testObject in test!{
            
            if testObject.result == -1 {
                isDiagnoseComplete = false
            }
            
        }

        if isDiagnoseComplete{
            
            let idStudent = studentArray![currentItemIndex].value(forKey: "idStudent") as! String
            let idDiagnoseGroup = evaluation?.value(forKey: "idGroupEvaluation") as! String
            
            var questionArray = [Question]()
            
            var jsonQuestionList = [JSON]()
            
            for testObject in test!{
                
                let question = Question(idQuestion: testObject.idQuestion!, result: testObject.result)
                let jsonQuestion : JSON = ["question_evaluation":question.idQuestion,"answer":question.result]
                
                jsonQuestionList.append(jsonQuestion)
                questionArray.append(question)
                
            }
            
            let jsonToSend : JSON = ["id_group_evaluation": idDiagnoseGroup ,"id_student":idStudent , "answers" : jsonQuestionList]
            let url = Constants.QUESTION_EVALUATION_USER_ENPOINT
            let urlRaw = URL(string: url)
            var request = URLRequest(url: urlRaw!)
            request.httpMethod = "POST"
            request.setValue("application/json", forHTTPHeaderField: "Content-Type")
            
            let jsonRaw = jsonToSend.rawString()!.parseJSONString
            request.httpBody = try! JSONSerialization.data(withJSONObject: jsonRaw!)
            
            self.startAnimating(Constants.getSizeForLoadingView(view: self.view), type: NVActivityIndicatorType.ballBeat, color: ColorHelper.getSecondaryColor(1.0), padding: NVActivityIndicatorView.DEFAULT_PADDING)
            
            Alamofire.request(request).validate()
                .responseJSON { (dataResponse) in
                    
                    self.stopAnimating()
                    
                    let isDiagnoseCorrect = TestController.getResponseForRangedDiagnose(dataResponse)
                    
                    if isDiagnoseCorrect{
                        
                        /*let result = ResultModel()
                        result.result = 10
                        let student : StudentModel! = self.studentArray![self.currentItemIndex]
                        result.student = student
                        result.diagnoses.append(self.diagnose!)
                        ResultController.addResultToDatabase(result)*/
                        
                        self.currentItemIndex += 1
                        
                        _ = SweetAlert().showAlert("Evaluación", subTitle: "Alumno calificado", style: AlertStyle.success, buttonTitle:"Ok", buttonColor:ColorHelper.getGreenColor(1.0)) { (isOtherButton) -> Void in
                            
                            self.viewDidLoad()
                            self.viewWillAppear(true)
                            
                        }
                        
                    }else{
                        
                        _ = SweetAlert().showAlert("Ooops!", subTitle: "Algo salió mal", style: AlertStyle.warning)
                        
                    }
                    
            }

            
        }else{
            _ = SweetAlert().showAlert("Ooops!", subTitle: "Finaliza tu evaluación correctamente!", style: AlertStyle.warning)

        }
        
    }

}
