//
//  StudentListTableViewCell.swift
//  SoyEducadoraSereyd
//
//  Created by Isaias Carrera Ventura on 21/07/17.
//  Copyright © 2017 FullStack. All rights reserved.
//

import UIKit
import DLRadioButton

class StudentListTableViewCell: UITableViewCell {

    @IBOutlet weak var studentName: UILabel!
    @IBOutlet weak var switchControlAssist: UISwitch!
    
    //@IBOutlet weak var segmentControlAssist: UISegmentedControl!
    
    @IBOutlet weak var imageStudent: UIImageView!
    @IBOutlet weak var segmentedControlConduct: UISegmentedControl!
}
