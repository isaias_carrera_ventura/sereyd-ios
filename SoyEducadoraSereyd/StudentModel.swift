//
//  Student.swift
//  SoyEducadoraSereyd
//
//  Created by Isaias Carrera Ventura on 29/05/17.
//  Copyright © 2017 FullStack. All rights reserved.
//

import Foundation
import RealmSwift

class StudentModel:Object {
	
	var name: String?
	var lastname: String?
	dynamic var sex: Int = 0
	var emergencyContanct: String?
	var birthdate: String?
	var emailTutor: String?
	var idStudent: String?
    var imageUrl: String?
	let groups = List<GroupModel>()
	
}
