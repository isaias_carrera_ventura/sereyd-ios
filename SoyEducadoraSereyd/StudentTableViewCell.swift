//
//  StudentTableViewCell.swift
//  SoyEducadoraSereyd
//
//  Created by Isaias Carrera Ventura on 30/05/17.
//  Copyright © 2017 FullStack. All rights reserved.
//

import UIKit

class StudentTableViewCell: UITableViewCell {

	var tapDeleteAction: ((UITableViewCell) -> Void)?
	var tapEditAction: ((UITableViewCell) -> Void)?

	@IBOutlet weak var nameStudentLabel: UILabel!
	
	@IBAction func editActionButton(_ sender: Any) {
		tapEditAction?(self)
	}
	
	@IBAction func deleteActionButton(_ sender: Any) {
		tapDeleteAction?(self)
	}

}
