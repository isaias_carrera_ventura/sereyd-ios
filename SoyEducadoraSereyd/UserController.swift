//
//  UserController.swift
//  SoyEducadoraSereyd
//
//  Created by Isaias Carrera Ventura on 23/05/17.
//  Copyright © 2017 FullStack. All rights reserved.
//

import Foundation
import RealmSwift
import Alamofire

class UserController{
	
	class func saveUserInDatabase(user: UserModel) -> Void{
		
		let realm = try! Realm()
		
		try! realm.write {
			
			realm.add(user)
			
		}
		
	}
	
	class func getJsonFromFacebookResponse(_ dictionaryUserData : NSDictionary) -> UserModel{
		
		var email: String = ""
		
		if let emailTemp = dictionaryUserData["email"] as? String{
			email = emailTemp
		}
		
		let id = dictionaryUserData["id"] as! String
		let name = "\(dictionaryUserData["first_name"] as! String) \(dictionaryUserData["last_name"] as! String)"
		let imageUrl = "https://graph.facebook.com/\(dictionaryUserData["id"] as! String)/picture?type=large"
		
		let user = UserModel()
		user.idUserFacebook = id 
		user.name = name
		user.email = email
		user.imageUrl = imageUrl
		
		return user
		
		
	}
    
    class func updateUserInDatabase(_ user : UserModel, _ email: String, _ name: String) {
        
        let realm  = try! Realm()
        
        try! realm.write {
            
            user.setValue(email, forKey: "email")
            user.setValue(name, forKey: "name")
            
        }
        
    }
    
    class func updateImageUrl(_ user: UserModel, _ imageUrl: String) {
        
        let realm  = try! Realm()
        
        try! realm.write {
            
            user.setValue(imageUrl, forKey: "imageUrl")
            
        }

    }
	
	class func getJsonFromResponse(_ dictionaryUser: NSDictionary) -> UserModel{
		
		let userObject = dictionaryUser["user"] as? [String:Any]
		
		var email = ""
		
		if let emailTemp = userObject?["email"] as? String{
			email = emailTemp
		}
		
		var image : String?
		
		if let imageUrl = userObject?["image"] as? String{
			image = "\(Constants.URL_SERVER)" + imageUrl
		}
		
		let name = userObject?["name"]
		
		let id_user = userObject?["id_user"]
		let user = UserModel()
		user.email = email
		user.idUser = id_user as! String
		user.imageUrl = image
		user.name = name as! String
		
		return user
		
	}

	class func getUserInDatabase() -> UserModel? {
		
		let realm = try! Realm()
		let user = realm.objects(UserModel.self)
		
		if user.count > 0 {
			return user[0]
		}
		else
		{
			return nil
		}
		
	}
    
    class func deleteSessionUser() -> Void{
        
        PlanningController.removeFileForPlanning()
        ResourceController.removeFileForResource()
        let realm = try! Realm()
        // Swift
        try! realm.write {
            realm.deleteAll()
        }
    }
    
    class func getResponseFromUpdateUser(_ dataResponse: DefaultDataResponse) -> Bool {
        
        guard dataResponse.error == nil else{
            
            return false
            
        }
        
        guard let dataResponse = dataResponse.data else{
            
            return false
            
        }
        
        let jsonResponse = try? JSONSerialization.jsonObject(with: dataResponse, options: .allowFragments) as! [String:Any]
        
        guard (jsonResponse) != nil else{
            
            return false
            
        }
        
        if (jsonResponse!["error"] as? Int) != 0 {
            
            return false
            
        }
        
        return true
        
        
    }
    
	
	
}
