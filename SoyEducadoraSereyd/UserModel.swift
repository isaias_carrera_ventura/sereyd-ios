//
//  UserModel.swift
//  SoyEducadoraSereyd
//
//  Created by Isaias Carrera Ventura on 23/05/17.
//  Copyright © 2017 FullStack. All rights reserved.
//

import Foundation
import RealmSwift

class UserModel: Object{
	
	dynamic var name, imageUrl : String!
	dynamic var email : String?
	dynamic var idUser: String!
	dynamic var idUserFacebook : String?
	
}
