//
//  WebViewPDFController.swift
//  SoyEducadoraSereyd
//
//  Created by Isaias Carrera Ventura on 13/07/17.
//  Copyright © 2017 FullStack. All rights reserved.
//

import Foundation
import UIKit

class WebViewController: UIViewController {
    
    // We're going to show a PDF with the help of this web view.
    let webView = UIWebView()
    
    override func viewDidLoad() {
        super.viewDidLoad()
        // Make sure the web view is shown fullscreen.
        webView.frame = view.frame
        view.addSubview(webView)
    }
}
